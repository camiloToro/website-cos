(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-blog-entradas-premio-oro-premio-oro-module"],{

/***/ "./src/app/pages/blog/entradas/premio-oro/premio-oro-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/blog/entradas/premio-oro/premio-oro-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: PremioOroRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PremioOroRoutingModule", function() { return PremioOroRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _premio_oro_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./premio-oro.component */ "./src/app/pages/blog/entradas/premio-oro/premio-oro.component.ts");





const routes = [
    {
        path: '', component: _premio_oro_component__WEBPACK_IMPORTED_MODULE_2__["PremioOroComponent"]
    }
];
class PremioOroRoutingModule {
}
PremioOroRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: PremioOroRoutingModule });
PremioOroRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function PremioOroRoutingModule_Factory(t) { return new (t || PremioOroRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](PremioOroRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PremioOroRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/pages/blog/entradas/premio-oro/premio-oro.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/blog/entradas/premio-oro/premio-oro.component.ts ***!
  \************************************************************************/
/*! exports provided: PremioOroComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PremioOroComponent", function() { return PremioOroComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/servicios/blog.service */ "./src/app/servicios/blog.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





const _c0 = function () { return ["/blog"]; };
class PremioOroComponent {
    constructor(meta, title, _blogService) {
        this.meta = meta;
        this.title = title;
        this._blogService = _blogService;
        this.entrada = [];
    }
    ngOnInit() {
        // usar el array del servicio blog
        this.entrada = this._blogService.entrada;
        // Para ñadir el título de la página
        this.title.setTitle('Colombian Outsourcing Solution - COS');
        // Añadir el tag de la info de la página
        this.meta.addTag({
            name: 'page.info',
            content: 'Premio Oro en el 6° Premio Nacional a la Excelencia de la Industria en las Interacciones con Clientes.'
        });
    }
    ngOnDestroy() {
        // Quitar el tag "page.info" antes de ir a otra página, para que rastree correctamente la información
        this.meta.removeTag('name=\'page.info\'');
    }
}
PremioOroComponent.ɵfac = function PremioOroComponent_Factory(t) { return new (t || PremioOroComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__["BlogService"])); };
PremioOroComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PremioOroComponent, selectors: [["app-premio-oro"]], decls: 67, vars: 7, consts: [["id", "landing", 1, "bg"], ["alt", "contact-center-BPO-COS-blog", 3, "src"], [1, "contenido"], [1, "text"], [1, "mt-5", "follow"], ["href", "https://www.facebook.com/Outsourcingcos/", "target", "blank"], [1, "fab", "fa-facebook-square", "ml-5"], ["href", "https://www.instagram.com/cosoutsourcing/", "target", "blank"], [1, "fab", "fa-instagram-square", "ml-3"], ["href", "https://www.linkedin.com/company/colombianoutsourcingcos/about/", "target", "blank"], [1, "fab", "fa-linkedin", "ml-3"], ["href", "https://www.youtube.com/channel/UCL89AlspoSj_exe6qidqHew", "target", "blank"], [1, "fab", "fa-youtube", "ml-3"], [1, "container"], [1, "fab", "fa-facebook-square", "ml-3"], [1, "tag-blog"], [3, "routerLink"], [1, "fas", "fa-tag"], [1, "comment"], [1, "form-group"], [1, "form-group", "nombre"], ["type", "text", "placeholder", "Ejem: Carlos, Maria, etc ...", 1, "form-control"], [1, "form-group", "correo"], ["type", "text", "placeholder", "Ejem: email@email.com ...", 1, "form-control"], [1, "form-group", "web"], ["type", "text", "placeholder", "Ejem: www.mysitioweb.com ...", 1, "form-control"], [1, "form-group", "comentario"], ["name", "", "id", "", "cols", "30", "rows", "10", "placeholder", "D\u00E9janos tu comentario", 1, "form-control"], [1, "btn", "btn-primary", "btn-block", "mt-10"]], template: function PremioOroComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "s\u00EDguenos en ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "i", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "s\u00EDguenos en ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "i", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "a", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "i", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " Noticias ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, " Deja tu comentario ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, " Tu Correo no ser\u00E1 publicado. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Los campos marcados con (*) con obligatorios");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Nombre *");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "input", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "correo *");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "input", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Web");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "input", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Comentario *");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "textarea", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "button", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, " Enviar Comentario ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.entrada[0].img, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[6].title);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.entrada[6].title, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[6].fecha);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[6].text);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](6, _c0));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"]], styles: ["*[_ngcontent-%COMP%] {\n  font-family: \"open-sans\", sans-serif;\n}\n\ni[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Free\";\n}\n\n[_ngcontent-%COMP%]::-moz-placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n[_ngcontent-%COMP%]::-ms-input-placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n[_ngcontent-%COMP%]::placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n.bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n  position: absolute;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n  position: relative;\n  padding: 16rem 12rem 0;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  font-weight: 800;\n  color: #fff;\n  font-size: 3rem;\n  width: 800px;\n  margin-bottom: 12rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: white;\n  width: 445px;\n  margin-bottom: 2rem;\n  font-size: 1.2rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: white;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  font-size: 1.3rem;\n}\n\n.container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  margin-top: 5rem;\n  font-weight: bold;\n  color: #353535;\n}\n\n.container[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin-bottom: 2rem;\n  font-weight: bold;\n}\n\n.container[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n  font-size: 1.5rem;\n  color: #C01A24;\n}\n\n.container[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]:hover {\n  color: #610d12;\n}\n\n.container[_ngcontent-%COMP%]   .tag-blog[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 0.8rem;\n}\n\n.container[_ngcontent-%COMP%]   .tag-blog[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #353535;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%] {\n  margin-bottom: 0.5rem;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .nombre[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin: 2rem 0 0 0;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .correo[_ngcontent-%COMP%]   label[_ngcontent-%COMP%], .container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .comentario[_ngcontent-%COMP%]   label[_ngcontent-%COMP%], .container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .web[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin: 0.5rem 0 0 0;\n}\n\n.container[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  margin-top: 3rem;\n  height: 3rem;\n}\n\n@media (min-width: 800px) and (max-width: 1024px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -200px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    margin-bottom: 7rem;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -456px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n    padding: 10rem 11rem 0;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2rem;\n    width: 460px;\n  }\n\n  .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n    margin-top: 11rem;\n  }\n}\n\n@media (min-width: 356px) and (max-width: 425px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 944px;\n    height: auto;\n    left: -519px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n    padding: 10rem 1rem 0;\n    text-align: center;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2rem;\n    width: 393px;\n    margin-bottom: 5rem;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 393px;\n  }\n\n  .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n    margin-top: 10rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYmxvZy9lbnRyYWRhcy9wcmVtaW8tb3JvL0Q6XFxwcm95ZWN0b3NcXHdlYnNpdGUtY29zL3NyY1xcYXBwXFxwYWdlc1xcYmxvZ1xcZW50cmFkYXNcXHByZW1pby1vcm9cXHByZW1pby1vcm8uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Jsb2cvZW50cmFkYXMvcHJlbWlvLW9yby9wcmVtaW8tb3JvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0NBQUE7QUNDSjs7QURFQTtFQUNJLGtDQUFBO0FDQ0o7O0FEQ0E7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQ0VKOztBREpBO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUNFSjs7QURKQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FDRUo7O0FERUk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDQ1I7O0FEQ0k7RUFDSSxrQkFBQTtFQUNBLHNCQUFBO0FDQ1I7O0FEQVE7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0FDRVo7O0FEQVE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNFWjs7QURBUTtFQUNJLGlCQUFBO0FDRVo7O0FERFk7RUFDSSxvQ0FBQTtBQ0doQjs7QUREWTtFQUNJLFlBQUE7QUNHaEI7O0FERmdCO0VBQ0ksaUJBQUE7QUNJcEI7O0FER0k7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQ0FSOztBREVJO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtBQ0FSOztBREdRO0VBQ0ksb0NBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUNEWjs7QURFWTtFQUNJLGNBQUE7QUNBaEI7O0FES1E7RUFDSSxpQkFBQTtBQ0haOztBREtRO0VBQ0ksY0FBQTtBQ0haOztBRE1JO0VBQ0kscUJBQUE7QUNKUjs7QURNWTtFQUNJLGtCQUFBO0FDSmhCOztBRFFZO0VBQ0ksb0JBQUE7QUNOaEI7O0FEZ0JJO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0FDZFI7O0FEa0JBO0VBRVE7SUFDSSxhQUFBO0lBQ0EsWUFBQTtFQ2hCVjtFRG9CVTtJQUNJLG1CQUFBO0VDbEJkO0FBQ0Y7O0FEd0JBO0VBRVE7SUFDSSxhQUFBO0lBQ0EsWUFBQTtFQ3ZCVjtFRHlCTTtJQUNJLHNCQUFBO0VDdkJWO0VEd0JVO0lBQ0ksZUFBQTtJQUNBLFlBQUE7RUN0QmQ7O0VEMkJNO0lBQ0ksaUJBQUE7RUN4QlY7QUFDRjs7QUQ0QkE7RUFFUTtJQUNJLFlBQUE7SUFDQSxZQUFBO0lBQ0EsWUFBQTtFQzNCVjtFRDZCTTtJQUNJLHFCQUFBO0lBQ0Esa0JBQUE7RUMzQlY7RUQ0QlU7SUFDSSxlQUFBO0lBQ0EsWUFBQTtJQUNBLG1CQUFBO0VDMUJkO0VENEJVO0lBQ0ksZUFBQTtJQUNBLFlBQUE7RUMxQmQ7O0VEZ0NNO0lBQ0ksaUJBQUE7RUM3QlY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Jsb2cvZW50cmFkYXMvcHJlbWlvLW9yby9wcmVtaW8tb3JvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKntcclxuICAgIGZvbnQtZmFtaWx5OiAnb3Blbi1zYW5zJywgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuaXtcclxuICAgIGZvbnQtZmFtaWx5OiAnRm9udCBBd2Vzb21lIDUgRnJlZSc7XHJcbn1cclxuOjpwbGFjZWhvbGRlcntcclxuICAgIGNvbG9yOiAjYzRjNGM0O1xyXG4gICAgcGFkZGluZzogLjVyZW07XHJcbn1cclxuLy8gQmFja2dyb3VuZyBjbGFzc1xyXG4uYmd7XHJcbiAgICBpbWd7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTsgICBcclxuICAgIH1cclxuICAgIC5jb250ZW5pZG97XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHBhZGRpbmc6IDE2cmVtIDEycmVtIDA7XHJcbiAgICAgICAgaDF7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDNyZW07XHJcbiAgICAgICAgICAgIHdpZHRoOiA4MDBweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTJyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHB7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgd2lkdGg6IDQ0NXB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEuMnJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmZvbGxvd3tcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICAgICAgICAgIGl7XHJcbiAgICAgICAgICAgICAgICBmb250LWZhbWlseTogJ0ZvbnQgQXdlc29tZSA1IEJyYW5kcyc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYXtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgICY6aG92ZXJ7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxLjNyZW07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuLmNvbnRhaW5lcntcclxuICAgIGgye1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDVyZW07XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICMzNTM1MzU7XHJcbiAgICB9XHJcbiAgICBsYWJlbHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgfVxyXG4gICAgLmZvbGxvd3tcclxuICAgICAgICBpe1xyXG4gICAgICAgICAgICBmb250LWZhbWlseTogJ0ZvbnQgQXdlc29tZSA1IEJyYW5kcyc7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgICAgICAgICBjb2xvcjogI0MwMUEyNDtcclxuICAgICAgICAgICAgJjpob3ZlcntcclxuICAgICAgICAgICAgICAgIGNvbG9yOnJnYig5NywgMTMsIDE4KSA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAudGFnLWJsb2d7XHJcbiAgICAgICAgaXtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAuOHJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgYXtcclxuICAgICAgICAgICAgY29sb3I6ICMzNTM1MzU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmZvcm0tZ3JvdXB7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogLjVyZW07XHJcbiAgICAgICAgLm5vbWJyZXtcclxuICAgICAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46MnJlbSAwIDAgMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuY29ycmVve1xyXG4gICAgICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjouNXJlbSAwIDAgMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAud2Vie1xyXG4gICAgICAgICAgICBAZXh0ZW5kIC5jb3JyZW87XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb21lbnRhcmlve1xyXG4gICAgICAgICAgICBAZXh0ZW5kIC5jb3JyZW87XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgYnV0dG9ue1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDNyZW07XHJcbiAgICAgICAgaGVpZ2h0OiAzcmVtO1xyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDo4MDBweClhbmQobWF4LXdpZHRoOjEwMjRweCl7XHJcbiAgICAuYmd7XHJcbiAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICB3aWR0aDogMTIyNHB4O1xyXG4gICAgICAgICAgICBsZWZ0OiAtMjAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb250ZW5pZG97XHJcblxyXG4gICAgICAgICAgICBoMXtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDdyZW07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDo1NjBweClhbmQobWF4LXdpZHRoOjc2OHB4KXtcclxuICAgIC5iZ3tcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMjI0cHg7XHJcbiAgICAgICAgICAgIGxlZnQ6IC00NTZweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNvbnRlbmlkb3tcclxuICAgICAgICAgICAgcGFkZGluZzogMTByZW0gMTFyZW0gMDtcclxuICAgICAgICAgICAgaDF7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDJyZW07XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogNDYwcHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY29udGFpbmVye1xyXG4gICAgICAgIGgye1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMXJlbTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiAzNTZweClhbmQobWF4LXdpZHRoOjQyNXB4KXtcclxuICAgIC5iZ3tcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIHdpZHRoOiA5NDRweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICBsZWZ0OiAtNTE5cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb250ZW5pZG97XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcmVtIDFyZW0gMDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBoMXtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMnJlbTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOjM5M3B4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTo1cmVtIDsgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAzOTNweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jb250YWluZXJ7XHJcbiAgICAgICAgXHJcbiAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcmVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSIsIioge1xuICBmb250LWZhbWlseTogXCJvcGVuLXNhbnNcIiwgc2Fucy1zZXJpZjtcbn1cblxuaSB7XG4gIGZvbnQtZmFtaWx5OiBcIkZvbnQgQXdlc29tZSA1IEZyZWVcIjtcbn1cblxuOjpwbGFjZWhvbGRlciB7XG4gIGNvbG9yOiAjYzRjNGM0O1xuICBwYWRkaW5nOiAwLjVyZW07XG59XG5cbi5iZyBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4uYmcgLmNvbnRlbmlkbyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgcGFkZGluZzogMTZyZW0gMTJyZW0gMDtcbn1cbi5iZyAuY29udGVuaWRvIGgxIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogM3JlbTtcbiAgd2lkdGg6IDgwMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxMnJlbTtcbn1cbi5iZyAuY29udGVuaWRvIHAge1xuICBjb2xvcjogd2hpdGU7XG4gIHdpZHRoOiA0NDVweDtcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbiAgZm9udC1zaXplOiAxLjJyZW07XG59XG4uYmcgLmNvbnRlbmlkbyAuZm9sbG93IHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG59XG4uYmcgLmNvbnRlbmlkbyAuZm9sbG93IGkge1xuICBmb250LWZhbWlseTogXCJGb250IEF3ZXNvbWUgNSBCcmFuZHNcIjtcbn1cbi5iZyAuY29udGVuaWRvIC5mb2xsb3cgYSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5iZyAuY29udGVuaWRvIC5mb2xsb3cgYTpob3ZlciB7XG4gIGZvbnQtc2l6ZTogMS4zcmVtO1xufVxuXG4uY29udGFpbmVyIGgyIHtcbiAgbWFyZ2luLXRvcDogNXJlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMzUzNTM1O1xufVxuLmNvbnRhaW5lciBsYWJlbCB7XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNvbnRhaW5lciAuZm9sbG93IGkge1xuICBmb250LWZhbWlseTogXCJGb250IEF3ZXNvbWUgNSBCcmFuZHNcIjtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIGNvbG9yOiAjQzAxQTI0O1xufVxuLmNvbnRhaW5lciAuZm9sbG93IGk6aG92ZXIge1xuICBjb2xvcjogIzYxMGQxMjtcbn1cbi5jb250YWluZXIgLnRhZy1ibG9nIGkge1xuICBmb250LXNpemU6IDAuOHJlbTtcbn1cbi5jb250YWluZXIgLnRhZy1ibG9nIGEge1xuICBjb2xvcjogIzM1MzUzNTtcbn1cbi5jb250YWluZXIgLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XG59XG4uY29udGFpbmVyIC5mb3JtLWdyb3VwIC5ub21icmUgbGFiZWwge1xuICBtYXJnaW46IDJyZW0gMCAwIDA7XG59XG4uY29udGFpbmVyIC5mb3JtLWdyb3VwIC5jb3JyZW8gbGFiZWwsIC5jb250YWluZXIgLmZvcm0tZ3JvdXAgLmNvbWVudGFyaW8gbGFiZWwsIC5jb250YWluZXIgLmZvcm0tZ3JvdXAgLndlYiBsYWJlbCB7XG4gIG1hcmdpbjogMC41cmVtIDAgMCAwO1xufVxuLmNvbnRhaW5lciBidXR0b24ge1xuICBtYXJnaW4tdG9wOiAzcmVtO1xuICBoZWlnaHQ6IDNyZW07XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA4MDBweCkgYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xuICAuYmcgaW1nIHtcbiAgICB3aWR0aDogMTIyNHB4O1xuICAgIGxlZnQ6IC0yMDBweDtcbiAgfVxuICAuYmcgLmNvbnRlbmlkbyBoMSB7XG4gICAgbWFyZ2luLWJvdHRvbTogN3JlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDU2MHB4KSBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgLmJnIGltZyB7XG4gICAgd2lkdGg6IDEyMjRweDtcbiAgICBsZWZ0OiAtNDU2cHg7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8ge1xuICAgIHBhZGRpbmc6IDEwcmVtIDExcmVtIDA7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8gaDEge1xuICAgIGZvbnQtc2l6ZTogMnJlbTtcbiAgICB3aWR0aDogNDYwcHg7XG4gIH1cblxuICAuY29udGFpbmVyIGgyIHtcbiAgICBtYXJnaW4tdG9wOiAxMXJlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDM1NnB4KSBhbmQgKG1heC13aWR0aDogNDI1cHgpIHtcbiAgLmJnIGltZyB7XG4gICAgd2lkdGg6IDk0NHB4O1xuICAgIGhlaWdodDogYXV0bztcbiAgICBsZWZ0OiAtNTE5cHg7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8ge1xuICAgIHBhZGRpbmc6IDEwcmVtIDFyZW0gMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8gaDEge1xuICAgIGZvbnQtc2l6ZTogMnJlbTtcbiAgICB3aWR0aDogMzkzcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNXJlbTtcbiAgfVxuICAuYmcgLmNvbnRlbmlkbyBwIHtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgd2lkdGg6IDM5M3B4O1xuICB9XG5cbiAgLmNvbnRhaW5lciBoMiB7XG4gICAgbWFyZ2luLXRvcDogMTByZW07XG4gIH1cbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PremioOroComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-premio-oro',
                templateUrl: './premio-oro.component.html',
                styleUrls: ['./premio-oro.component.scss']
            }]
    }], function () { return [{ type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"] }, { type: src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__["BlogService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/pages/blog/entradas/premio-oro/premio-oro.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/blog/entradas/premio-oro/premio-oro.module.ts ***!
  \*********************************************************************/
/*! exports provided: PremioOroModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PremioOroModule", function() { return PremioOroModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _premio_oro_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./premio-oro-routing.module */ "./src/app/pages/blog/entradas/premio-oro/premio-oro-routing.module.ts");
/* harmony import */ var _premio_oro_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./premio-oro.component */ "./src/app/pages/blog/entradas/premio-oro/premio-oro.component.ts");





class PremioOroModule {
}
PremioOroModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: PremioOroModule });
PremioOroModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function PremioOroModule_Factory(t) { return new (t || PremioOroModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _premio_oro_routing_module__WEBPACK_IMPORTED_MODULE_2__["PremioOroRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](PremioOroModule, { declarations: [_premio_oro_component__WEBPACK_IMPORTED_MODULE_3__["PremioOroComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _premio_oro_routing_module__WEBPACK_IMPORTED_MODULE_2__["PremioOroRoutingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PremioOroModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [_premio_oro_component__WEBPACK_IMPORTED_MODULE_3__["PremioOroComponent"]],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _premio_oro_routing_module__WEBPACK_IMPORTED_MODULE_2__["PremioOroRoutingModule"]
                ]
            }]
    }], null, null); })();


/***/ })

}]);
//# sourceMappingURL=pages-blog-entradas-premio-oro-premio-oro-module-es2015.js.map