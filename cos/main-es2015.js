(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _pages_home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pages/home/home.component */ "./src/app/pages/home/home.component.ts");





const routes = [
    {
        pathMatch: 'prefix',
        path: '',
        component: _pages_home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"],
        children: [
            {
                path: '',
                loadChildren: () => __webpack_require__.e(/*! import() | pages-home-home-module */ "pages-home-home-module").then(__webpack_require__.bind(null, /*! ./pages/home/home.module */ "./src/app/pages/home/home.module.ts")).then(m => m.HomeModule),
            },
        ]
    },
    {
        pathMatch: 'prefix',
        path: 'en',
        component: _pages_home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"],
        children: [
            {
                path: '',
                loadChildren: () => __webpack_require__.e(/*! import() | pages-home-home-module */ "pages-home-home-module").then(__webpack_require__.bind(null, /*! ./pages/home/home.module */ "./src/app/pages/home/home.module.ts")).then(m => m.HomeModule),
            },
        ]
    },
    {
        path: 'blog',
        loadChildren: () => Promise.all(/*! import() | pages-blog-blog-module */[__webpack_require__.e("default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"), __webpack_require__.e("pages-blog-blog-module")]).then(__webpack_require__.bind(null, /*! ./pages/blog/blog.module */ "./src/app/pages/blog/blog.module.ts")).then(m => m.BlogModule),
    },
    {
        path: 'contacto',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-comercial-comercial-module */ "pages-comercial-comercial-module").then(__webpack_require__.bind(null, /*! ./pages/comercial/comercial.module */ "./src/app/pages/comercial/comercial.module.ts")).then(m => m.ComercialModule),
    },
    {
        path: 'contact',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-comercial-comercial-module */ "pages-comercial-comercial-module").then(__webpack_require__.bind(null, /*! ./pages/comercial/comercial.module */ "./src/app/pages/comercial/comercial.module.ts")).then(m => m.ComercialModule),
    },
    {
        path: 'unete-a-nuestro-equipo',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-unete-unete-module */ "pages-unete-unete-module").then(__webpack_require__.bind(null, /*! ./pages/unete/unete.module */ "./src/app/pages/unete/unete.module.ts")).then(m => m.UneteModule),
    },
    {
        path: 'join-our-family',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-unete-unete-module */ "pages-unete-unete-module").then(__webpack_require__.bind(null, /*! ./pages/unete/unete.module */ "./src/app/pages/unete/unete.module.ts")).then(m => m.UneteModule),
    },
    {
        path: 'politicas-de-privacidad',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-politicas-politicas-module */ "pages-politicas-politicas-module").then(__webpack_require__.bind(null, /*! ./pages/politicas/politicas.module */ "./src/app/pages/politicas/politicas.module.ts")).then(m => m.PoliticasModule),
    },
    // Blogs Routes
    {
        path: 'colombian-outsourcing-solutions-en-el-vii-foro-multilatinas',
        loadChildren: () => Promise.all(/*! import() | pages-blog-entradas-multilatinas-multilatinas-module */[__webpack_require__.e("default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"), __webpack_require__.e("pages-blog-entradas-multilatinas-multilatinas-module")]).then(__webpack_require__.bind(null, /*! ./pages/blog/entradas/multilatinas/multilatinas.module */ "./src/app/pages/blog/entradas/multilatinas/multilatinas.module.ts")).then(m => m.MultilatinasModule),
    },
    {
        path: 'convencion-internacional-de-seguros',
        loadChildren: () => Promise.all(/*! import() | pages-blog-entradas-convencion-seguros-convencion-seguros-module */[__webpack_require__.e("default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"), __webpack_require__.e("pages-blog-entradas-convencion-seguros-convencion-seguros-module")]).then(__webpack_require__.bind(null, /*! ./pages/blog/entradas/convencion-seguros/convencion-seguros.module */ "./src/app/pages/blog/entradas/convencion-seguros/convencion-seguros.module.ts")).then(m => m.ConvencionSegurosModule),
    },
    {
        path: 'cos-fue-participe-del-foro-de-la-salud',
        loadChildren: () => Promise.all(/*! import() | pages-blog-entradas-foro-salud-foro-salud-module */[__webpack_require__.e("default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"), __webpack_require__.e("pages-blog-entradas-foro-salud-foro-salud-module")]).then(__webpack_require__.bind(null, /*! ./pages/blog/entradas/foro-salud/foro-salud.module */ "./src/app/pages/blog/entradas/foro-salud/foro-salud.module.ts")).then(m => m.ForoSaludModule),
    },
    {
        path: 'cos-presente-en-el-ix-congreso-latinoamericano-de-talento-humano',
        loadChildren: () => Promise.all(/*! import() | pages-blog-entradas-congreso-th-congreso-th-module */[__webpack_require__.e("default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"), __webpack_require__.e("pages-blog-entradas-congreso-th-congreso-th-module")]).then(__webpack_require__.bind(null, /*! ./pages/blog/entradas/congreso-th/congreso-th.module */ "./src/app/pages/blog/entradas/congreso-th/congreso-th.module.ts")).then(m => m.CongresoThModule),
    },
    {
        path: 'nueva-alianza-cos-se-globaliza-con-tele-web',
        loadChildren: () => Promise.all(/*! import() | pages-blog-entradas-alianza-alianza-module */[__webpack_require__.e("default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"), __webpack_require__.e("pages-blog-entradas-alianza-alianza-module")]).then(__webpack_require__.bind(null, /*! ./pages/blog/entradas/alianza/alianza.module */ "./src/app/pages/blog/entradas/alianza/alianza.module.ts")).then(m => m.AlianzaModule),
    },
    {
        path: 'ix-conferencia-panamericana-de-logistica',
        loadChildren: () => Promise.all(/*! import() | pages-blog-entradas-conferencia-logistica-conferencia-logistica-module */[__webpack_require__.e("default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"), __webpack_require__.e("pages-blog-entradas-conferencia-logistica-conferencia-logistica-module")]).then(__webpack_require__.bind(null, /*! ./pages/blog/entradas/conferencia-logistica/conferencia-logistica.module */ "./src/app/pages/blog/entradas/conferencia-logistica/conferencia-logistica.module.ts")).then(m => m.ConferenciaLogisticaModule),
    },
    {
        path: 'premio-oro-en-el-6-premio-nacional-a-la-excelencia-de-la-industria-en-las-interacciones-con-clientes',
        loadChildren: () => Promise.all(/*! import() | pages-blog-entradas-premio-oro-premio-oro-module */[__webpack_require__.e("default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"), __webpack_require__.e("pages-blog-entradas-premio-oro-premio-oro-module")]).then(__webpack_require__.bind(null, /*! ./pages/blog/entradas/premio-oro/premio-oro.module */ "./src/app/pages/blog/entradas/premio-oro/premio-oro.module.ts")).then(m => m.PremioOroModule),
    },
    {
        path: 'agendamiento-de-citas-comerciales',
        loadChildren: () => Promise.all(/*! import() | pages-blog-entradas-agendamiento-citas-agendamiento-citas-module */[__webpack_require__.e("default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"), __webpack_require__.e("pages-blog-entradas-agendamiento-citas-agendamiento-citas-module")]).then(__webpack_require__.bind(null, /*! ./pages/blog/entradas/agendamiento-citas/agendamiento-citas.module */ "./src/app/pages/blog/entradas/agendamiento-citas/agendamiento-citas.module.ts")).then(m => m.AgendamientoCitasModule),
    },
    {
        path: 'son-rentables-los-servicios-de-outsourcing-en-colombia',
        loadChildren: () => Promise.all(/*! import() | pages-blog-entradas-servicio-rentable-servicio-rentable-module */[__webpack_require__.e("default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"), __webpack_require__.e("pages-blog-entradas-servicio-rentable-servicio-rentable-module")]).then(__webpack_require__.bind(null, /*! ./pages/blog/entradas/servicio-rentable/servicio-rentable.module */ "./src/app/pages/blog/entradas/servicio-rentable/servicio-rentable.module.ts")).then(m => m.ServicioRentableModule),
    },
    {
        path: 'por-que-triunfa-el-call-center-en-colombia',
        loadChildren: () => Promise.all(/*! import() | pages-blog-entradas-triunfo-call-triunfo-call-module */[__webpack_require__.e("default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"), __webpack_require__.e("pages-blog-entradas-triunfo-call-triunfo-call-module")]).then(__webpack_require__.bind(null, /*! ./pages/blog/entradas/triunfo-call/triunfo-call.module */ "./src/app/pages/blog/entradas/triunfo-call/triunfo-call.module.ts")).then(m => m.TriunfoCallModule),
    },
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, {
                initialNavigation: 'enabled'
            })],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, {
                        initialNavigation: 'enabled'
                    })],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/__ivy_ngcc__/fesm2015/ngx-cookie-service.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./servicios/i18nservice/language.service */ "./src/app/servicios/i18nservice/language.service.ts");
/* harmony import */ var _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/navbar/navbar.component */ "./src/app/components/navbar/navbar.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");









function AppComponent_p_76_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "1888 4123836 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
class AppComponent {
    constructor(cookieService, metaTags, translate, router, lang, sharedService) {
        this.cookieService = cookieService;
        this.metaTags = metaTags;
        this.translate = translate;
        this.router = router;
        this.lang = lang;
        this.sharedService = sharedService;
        this.title = 'Colombian-Outsourcing-Solutions';
        this.activeLang = 'es';
        this.cookieValue = 'UNKNOWN';
        this.visible = true;
        this.navMobile = false;
        this.close = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ngOnInit() {
        this.metaTags.addTags([
            { name: 'keywords', content: 'cos, colombian outsourcing solutions, call center, contact center, bpo' },
            { name: 'robots', content: 'index, follow' },
            { name: 'autor', content: 'montechelo' },
        ]);
        this.cookieService.set('Cookie', 'GDPR');
        this.cookieValue = this.cookieService.get('Cookie');
        this.lang.localeEvent.subscribe(locale => this.translate.use(this.activeLang));
        console.log(this.router.url);
        this.sharedService.Message.subscribe(m => { this.message = m; });
        console.log(this.message);
        if (this.router.url == '/contacto') {
            this.activeLang = 'es';
            this.translate.use(this.activeLang);
        }
        else if (this.router.url == '/contact') {
            this.activeLang = 'en';
            this.translate.use(this.activeLang);
            this.translate.currentLang == 'en';
        }
    }
    cambiarLenguaje(lang) {
        this.activeLang = lang;
        this.translate.use(lang);
        localStorage.setItem('idioma', JSON.stringify(this.activeLang));
        const idio = localStorage.getItem("idioma");
        console.log(typeof this.translate.currentLang);
        if (this.activeLang == 'es') {
        }
        else if (this.activeLang == 'en') {
        }
    }
    onGRDP() {
        this.visible = !this.visible;
        if (this.visible) {
            this.close.emit(null);
        }
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_cookie_service__WEBPACK_IMPORTED_MODULE_1__["CookieService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Meta"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"])); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], outputs: { close: "close" }, decls: 77, vars: 40, consts: [[1, "content"], [1, "brand-footer"], ["src", "assets/img/brand-color.svg", "alt", ""], [1, "footer-link"], [1, "line"], [1, "footer-service"], ["href", "#BPO"], ["href", "#tecno"], [1, "footer-contact"], [1, "email"], [1, "social"], ["href", "https://www.facebook.com/Outsourcingcos/", "target", "blank"], [1, "fab", "fa-facebook-square"], ["href", "https://www.instagram.com/cosoutsourcing/", "target", "blank"], [1, "fab", "fa-instagram-square", "ml-3"], ["href", "https://www.linkedin.com/company/colombianoutsourcingcos/about/", "target", "blank"], [1, "fab", "fa-linkedin", "ml-3"], ["href", "https://www.youtube.com/channel/UCL89AlspoSj_exe6qidqHew", "target", "blank"], [1, "fab", "fa-youtube", "ml-3"], [1, "phone"], [4, "ngIf"]], template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "footer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "ul");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](12, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](16, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](20, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](24, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](28, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](32, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](36, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](40, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](44, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](49, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "info@cos.com.co");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "a.juridico@groupcos.com.co");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](57, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](60, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](61, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "i", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "i", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "a", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "i", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](73, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "(+57 1) 4863290");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](76, AppComponent_p_76_Template, 2, 0, "p", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](12, 14, "footer.services"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](16, 16, "footer.sales"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](20, 18, "footer.backoffice"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](24, 20, "footer.tphone"), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](28, 22, "footer.collections"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](32, 24, "footer.omni"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](36, 26, "footer.software"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](40, 28, "footer.digital"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](44, 30, "footer.data"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](49, 32, "footer.contact"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](57, 34, "footer.follow"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](60, 36, "footer.OurNetworks"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](73, 38, "footer.call"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.translate.currentLang == "en");
    } }, directives: [_components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_6__["NavbarComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterOutlet"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"]], pipes: [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslatePipe"]], styles: ["app-navbar[_ngcontent-%COMP%] {\n  z-index: 9;\n  position: fixed;\n}\n\n.footer[_ngcontent-%COMP%] {\n  position: absolute;\n  width: 100%;\n  bottom: 0;\n  z-index: 9;\n}\n\nfooter[_ngcontent-%COMP%] {\n  margin-top: 6rem;\n  height: 500px;\n  background: #fff;\n  box-shadow: 0 0 30px rgba(53, 53, 53, 0.2);\n  -webkit-animation: none !important;\n  animation: none !important;\n  display: grid;\n  grid-template-columns: 30% 70%;\n}\n\nfooter[_ngcontent-%COMP%]   .brand-footer[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  grid-column: 1;\n}\n\nfooter[_ngcontent-%COMP%]   .brand-footer[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 349px;\n}\n\nfooter[_ngcontent-%COMP%]   .footer-link[_ngcontent-%COMP%] {\n  padding-top: 4rem;\n  grid-column: 2;\n  display: grid;\n  grid-template-columns: 50% 50%;\n}\n\nfooter[_ngcontent-%COMP%]   .footer-link[_ngcontent-%COMP%]   .footer-services[_ngcontent-%COMP%] {\n  grid-column: 1;\n}\n\nfooter[_ngcontent-%COMP%]   .footer-link[_ngcontent-%COMP%]   .footer-services[_ngcontent-%COMP%]   .footer-contact[_ngcontent-%COMP%] {\n  grid-column: 2;\n}\n\nfooter[_ngcontent-%COMP%]   .footer-link[_ngcontent-%COMP%]   .email[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%], footer[_ngcontent-%COMP%]   .footer-link[_ngcontent-%COMP%]   .phone[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%], footer[_ngcontent-%COMP%]   .footer-link[_ngcontent-%COMP%]   .social[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n  color: #C01A24;\n  font-weight: bold;\n}\n\nfooter[_ngcontent-%COMP%]   .footer-link[_ngcontent-%COMP%]   .social[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n}\n\nfooter[_ngcontent-%COMP%]   .footer-link[_ngcontent-%COMP%]   .social[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #353535;\n  font-size: 1.2rem;\n}\n\nfooter[_ngcontent-%COMP%]   .footer-link[_ngcontent-%COMP%]   .social[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  font-size: 1rem;\n}\n\nfooter[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\n  list-style: none;\n  line-height: 2rem;\n}\n\nfooter[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  color: #C01A24;\n  font-weight: bold;\n  margin-bottom: 2rem;\n}\n\nfooter[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #353535;\n}\n\n.line[_ngcontent-%COMP%] {\n  border-left: 1px solid #D8D8D8;\n  height: 400px;\n  position: absolute;\n  left: 55%;\n  margin-left: -3px;\n}\n\n@media (max-width: 425px) {\n  footer[_ngcontent-%COMP%] {\n    grid-template-columns: none;\n    grid-template-rows: 80% 20%;\n  }\n  footer[_ngcontent-%COMP%]   .footer-link[_ngcontent-%COMP%] {\n    grid-column: 1;\n    grid-row: 1;\n  }\n  footer[_ngcontent-%COMP%]   .brand-footer[_ngcontent-%COMP%] {\n    grid-column: 1;\n    grid-row: 2;\n  }\n  footer[_ngcontent-%COMP%]   .brand-footer[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 180px;\n  }\n  footer[_ngcontent-%COMP%]   .line[_ngcontent-%COMP%] {\n    display: none;\n  }\n\n  footer[_ngcontent-%COMP%]    > *[_ngcontent-%COMP%]:nth-child(1) {\n    -ms-grid-row: 1;\n    -ms-grid-column: 1;\n  }\n\n  footer[_ngcontent-%COMP%]    > *[_ngcontent-%COMP%]:nth-child(2) {\n    -ms-grid-row: 2;\n    -ms-grid-column: 1;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  footer[_ngcontent-%COMP%] {\n    height: 420px;\n  }\n  footer[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n    font-size: 1.5rem;\n  }\n  footer[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 0.8rem;\n  }\n  footer[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    font-size: 0.8rem;\n  }\n  footer[_ngcontent-%COMP%]   .line[_ngcontent-%COMP%] {\n    left: 59%;\n    height: 312px;\n  }\n}\n\n.grdp[_ngcontent-%COMP%] {\n  position: fixed;\n  padding: 1rem;\n  bottom: 0;\n  width: 100%;\n  z-index: 100000;\n  text-align: justify;\n  display: flex;\n  background: #fff;\n  border: none;\n  box-shadow: 0 0 30px rgba(0, 0, 0, 0.1);\n  color: #353535;\n  font-size: 1rem;\n  align-items: center;\n}\n\n.bcgrdp[_ngcontent-%COMP%] {\n  cursor: pointer;\n  border: none;\n  height: 3rem;\n  width: 20rem;\n  margin-left: 3rem;\n  background-color: #C01A24;\n}\n\n.navbar-mobile[_ngcontent-%COMP%] {\n  width: 100%;\n  display: flex;\n  justify-content: flex-end;\n  padding: 1rem;\n}\n\n.btn-toggle[_ngcontent-%COMP%] {\n  background: #fff;\n  position: fixed;\n  border-radius: 0.5rem;\n  border: none;\n  z-index: 1;\n  outline: none;\n  box-shadow: 0 0 20px rgba(53, 53, 53, 0.2);\n}\n\n.btn-toggle[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  color: #C01A24;\n  font-size: 1.5rem;\n  padding: 0.8rem;\n}\n\n.navMobile[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n}\n\n.navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  transition: ease-in-out 0.3s;\n  padding: 4rem;\n  background: white;\n  width: 100%;\n  height: 100vh;\n  position: absolute;\n  top: 0;\n  left: -430px;\n}\n\n.navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  display: block;\n  width: 50%;\n  margin-bottom: 5rem;\n}\n\n.navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  width: 100%;\n  color: #353535;\n}\n\n.navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  width: 100%;\n  margin-bottom: 1rem;\n}\n\n.navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\nhr[_ngcontent-%COMP%] {\n  width: 100%;\n  margin: 0;\n}\n\n.navMobile-op[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n}\n\n.navMobile-op[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  transition: ease-in-out 0.3s;\n  padding: 4rem;\n  background: white;\n  width: 100%;\n  height: 100vh;\n  position: absolute;\n  top: 0;\n  left: 0;\n}\n\n.navMobile-op[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  display: block;\n  width: 50%;\n  margin-bottom: 5rem;\n}\n\n.navMobile-op[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  width: 100%;\n  color: #353535;\n}\n\n.navMobile-op[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  width: 100%;\n  margin-bottom: 1rem;\n}\n\n.navMobile-op[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%] {\n    left: -771px;\n  }\n}\n\n@media (min-width: 800px) {\n  .navbar-mobile[_ngcontent-%COMP%] {\n    display: none;\n  }\n}\n\n.active[_ngcontent-%COMP%] {\n  background: rgba(0, 0, 0, 0.2);\n}\n\n.navbar-brand[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n\n.navegador[_ngcontent-%COMP%] {\n  position: fixed;\n}\n\n.pbx[_ngcontent-%COMP%] {\n  background: #C01A24;\n  color: #fff;\n  font-size: 1rem;\n  padding: 0.5rem 3rem;\n  text-align: right;\n  width: 100vw;\n}\n\n.pbx[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  margin: 0;\n  padding-right: 3rem;\n}\n\n.navbar[_ngcontent-%COMP%] {\n  position: fixed;\n  width: 100vw;\n  padding: 1rem 7rem;\n  background: transparent;\n}\n\n.navbar[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #fff;\n  cursor: pointer;\n}\n\n.navbar[_ngcontent-%COMP%]   .navbar-nav[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n}\n\n.collapse[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n}\n\n.collapse[_ngcontent-%COMP%]   .services[_ngcontent-%COMP%]   .btn-services[_ngcontent-%COMP%] {\n  color: #fff;\n  background: none;\n  border: none;\n  outline: none;\n}\n\n.collapse[_ngcontent-%COMP%]   .services[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #353535;\n}\n\n.follow[_ngcontent-%COMP%] {\n  z-index: 999;\n}\n\n.follow[_ngcontent-%COMP%]:hover   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 3rem;\n}\n\n.navbar-sticky[_ngcontent-%COMP%] {\n  background: rgba(255, 255, 255, 0.8);\n  box-shadow: 0 6px 30px rgba(53, 53, 53, 0.3);\n}\n\n.navbar-sticky[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #353535;\n}\n\n.navbar-sticky[_ngcontent-%COMP%]   .collapse[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n}\n\n.navbar-sticky[_ngcontent-%COMP%]   .collapse[_ngcontent-%COMP%]   .services[_ngcontent-%COMP%]   .btn-services[_ngcontent-%COMP%] {\n  color: #353535;\n  background: none;\n  border: none;\n  outline: none;\n}\n\n@media (min-width: 256px) and (max-width: 768px) {\n  .navbar[_ngcontent-%COMP%] {\n    padding: 1rem 3rem;\n    display: none;\n  }\n\n  img[_ngcontent-%COMP%] {\n    display: none;\n  }\n\n  .pbx[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    padding: 0;\n  }\n\n  .navbar-sticky[_ngcontent-%COMP%] {\n    background: none;\n    box-shadow: none;\n    display: none;\n  }\n  .navbar-sticky[_ngcontent-%COMP%]   .btn-services[_ngcontent-%COMP%] {\n    display: none;\n  }\n}\n\n.btn-portal[_ngcontent-%COMP%] {\n  color: #fff;\n  background-color: #1B506F;\n  border-color: #1B506F;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvRDpcXHByb3llY3Rvc1xcd2Vic2l0ZS1jb3Mvc3JjXFxhcHBcXGFwcC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBO0VBQ0ksVUFBQTtFQUNBLGVBQUE7QUNKSjs7QURPQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FDSkY7O0FEUUE7RUFDSSxnQkFBQTtFQUNGLGFBQUE7RUFDQSxnQkFBQTtFQUNBLDBDQUFBO0VBQ0Esa0NBQUE7RUFDUSwwQkFBQTtFQUVSLGFBQUE7RUFFQSw4QkFBQTtBQ0xGOztBRE1FO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFFQSxjQUFBO0FDSk47O0FES007RUFDSSxZQUFBO0FDSFY7O0FETUU7RUFDSSxpQkFBQTtFQUVBLGNBQUE7RUFFQSxhQUFBO0VBRUEsOEJBQUE7QUNKTjs7QURLTTtFQUVJLGNBQUE7QUNIVjs7QURJTTtFQUVJLGNBQUE7QUNGVjs7QURNRTtFQUNJLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FDSk47O0FEU0U7RUFDSSxvQ0FBQTtBQ1BOOztBRFNFO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0FDUE47O0FEUU07RUFDSSxlQUFBO0FDTlY7O0FEY0E7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0FDWkY7O0FEYUU7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQ1hOOztBRGFFO0VBQ0ksY0FBQTtBQ1hOOztBRGdCQTtFQUNFLDhCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGlCQUFBO0FDYkY7O0FEZ0JBO0VBQ0U7SUFFSSwyQkFBQTtJQUVBLDJCQUFBO0VDYko7RURjSTtJQUVJLGNBQUE7SUFFQSxXQUFBO0VDWlI7RURjSTtJQUVJLGNBQUE7SUFFQSxXQUFBO0VDWlI7RURhUTtJQUNJLFlBQUE7RUNYWjtFRGNJO0lBQ0ksYUFBQTtFQ1pSOztFRGVBO0lBQ0ksZUFBQTtJQUNBLGtCQUFBO0VDWko7O0VEY0E7SUFDSSxlQUFBO0lBQ0Esa0JBQUE7RUNYSjtBQUNGOztBRGFBO0VBQ0U7SUFDSSxhQUFBO0VDWEo7RURZSTtJQUNJLGlCQUFBO0VDVlI7RURZSTtJQUNJLGlCQUFBO0VDVlI7RURZSTtJQUNJLGlCQUFBO0VDVlI7RURZSTtJQUNJLFNBQUE7SUFDQSxhQUFBO0VDVlI7QUFDRjs7QURjQTtFQUNJLGVBQUE7RUFDQSxhQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsdUNBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDWko7O0FEZUU7RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtBQ1pKOztBRGdCQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0FDYko7O0FEZUE7RUFDSSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLDBDQUFBO0FDWko7O0FEYUk7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDWFI7O0FEZUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ1pKOztBRGFJO0VBQ0ksYUFBQTtFQUNBLGVBQUE7RUFDQSw0QkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsWUFBQTtBQ1hSOztBRFlRO0VBQ0ksY0FBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtBQ1ZaOztBRFlRO0VBQ0ksV0FBQTtFQUNBLGNBQUE7QUNWWjs7QURZUTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtBQ1ZaOztBRFlRO0VBQ0ksaUJBQUE7QUNWWjs7QURjQTtFQUNJLFdBQUE7RUFDQSxTQUFBO0FDWEo7O0FEYUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ1ZKOztBRFdJO0VBQ0ksYUFBQTtFQUNBLGVBQUE7RUFDQSw0QkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtBQ1RSOztBRFVRO0VBQ0ksY0FBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtBQ1JaOztBRFVRO0VBQ0ksV0FBQTtFQUNBLGNBQUE7QUNSWjs7QURVUTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtBQ1JaOztBRFVRO0VBQ0ksaUJBQUE7QUNSWjs7QURhQTtFQUVRO0lBRUksWUFBQTtFQ1pWO0FBQ0Y7O0FEZUE7RUFDSTtJQUNJLGFBQUE7RUNiTjtBQUNGOztBRGlCQTtFQUNJLDhCQUFBO0FDZko7O0FEaUJBO0VBQ0ksZUFBQTtBQ2RKOztBRGdCQTtFQUNJLGVBQUE7QUNiSjs7QURlQTtFQUNJLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQ1pKOztBRGFJO0VBQ0ksU0FBQTtFQUNBLG1CQUFBO0FDWFI7O0FEZUE7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7QUNaSjs7QURhSTtFQUNJLFdBQUE7RUFDQSxlQUFBO0FDWFI7O0FEYUk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUNYUjs7QURlQTtFQUNJLGFBQUE7RUFDQSx5QkFBQTtBQ1pKOztBRGNRO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUNaWjs7QURjUTtFQUNJLGNBQUE7QUNaWjs7QURpQkE7RUFDSSxZQUFBO0FDZEo7O0FEbUJnQjtFQUNJLGVBQUE7QUNqQnBCOztBRHlCQTtFQUNJLG9DQUFBO0VBQ0EsNENBQUE7QUN0Qko7O0FEdUJJO0VBQ0ksY0FBQTtBQ3JCUjs7QUR1Qkk7RUFDSSxhQUFBO0VBQ0EseUJBQUE7QUNyQlI7O0FEdUJZO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUNyQmhCOztBRDJCQTtFQUNJO0lBQ0ksa0JBQUE7SUFDQSxhQUFBO0VDeEJOOztFRDBCRTtJQUNJLGFBQUE7RUN2Qk47O0VEMEJNO0lBQ0ksVUFBQTtFQ3ZCVjs7RUQwQkU7SUFDSSxnQkFBQTtJQUNBLGdCQUFBO0lBQ0EsYUFBQTtFQ3ZCTjtFRHdCTTtJQUNJLGFBQUE7RUN0QlY7QUFDRjs7QUQwQkE7RUFDRSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtBQ3hCRiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4vLyAuY29udGVudHtcclxuLy8gICAgIHdpZHRoOiAxMDB2dztcclxuLy8gICAgIGhlaWdodDogMTAwdmg7XHJcbi8vIH1cclxuYXBwLW5hdmJhcntcclxuICAgIHotaW5kZXg6IDk7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuXHJcbi5mb290ZXJ7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJvdHRvbTogMDtcclxuICB6LWluZGV4OiA5O1xyXG5cclxufVxyXG5cclxuZm9vdGVye1xyXG4gICAgbWFyZ2luLXRvcDogNnJlbTtcclxuICBoZWlnaHQ6IDUwMHB4O1xyXG4gIGJhY2tncm91bmQ6I2ZmZiA7XHJcbiAgYm94LXNoYWRvdzogMCAwIDMwcHggcmdiYSgkY29sb3I6ICMzNTM1MzUsICRhbHBoYTogLjIpO1xyXG4gIC13ZWJraXQtYW5pbWF0aW9uOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICBhbmltYXRpb246IG5vbmUgIWltcG9ydGFudDtcclxuICBkaXNwbGF5OiAtbXMtZ3JpZDtcclxuICBkaXNwbGF5OiBncmlkO1xyXG4gIC1tcy1ncmlkLWNvbHVtbnM6IDMwJSA3MCU7XHJcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAzMCUgNzAlO1xyXG4gIC5icmFuZC1mb290ZXJ7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XHJcbiAgICAgIGdyaWQtY29sdW1uOiAxO1xyXG4gICAgICBpbWd7XHJcbiAgICAgICAgICB3aWR0aDogMzQ5cHg7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgLmZvb3Rlci1saW5re1xyXG4gICAgICBwYWRkaW5nLXRvcDogNHJlbTtcclxuICAgICAgLW1zLWdyaWQtY29sdW1uOiAyO1xyXG4gICAgICBncmlkLWNvbHVtbjogMjtcclxuICAgICAgZGlzcGxheTogLW1zLWdyaWQ7XHJcbiAgICAgIGRpc3BsYXk6IGdyaWQ7XHJcbiAgICAgIC1tcy1ncmlkLWNvbHVtbnM6IDUwJSA1MCU7XHJcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogNTAlIDUwJTtcclxuICAgICAgLmZvb3Rlci1zZXJ2aWNlc3tcclxuICAgICAgICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcclxuICAgICAgICAgIGdyaWQtY29sdW1uOiAxO1xyXG4gICAgICAuZm9vdGVyLWNvbnRhY3R7XHJcbiAgICAgICAgICAtbXMtZ3JpZC1jb2x1bW46IDI7XHJcbiAgICAgICAgICBncmlkLWNvbHVtbjogMjtcclxuICAgICAgfVxyXG59XHJcbi5lbWFpbHtcclxuICBoMntcclxuICAgICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICAgIGNvbG9yOiAjQzAxQTI0O1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcbn1cclxuLnNvY2lhbHtcclxuICBAZXh0ZW5kLmVtYWlsO1xyXG4gIGl7XHJcbiAgICAgIGZvbnQtZmFtaWx5OiAnRm9udCBBd2Vzb21lIDUgQnJhbmRzJztcclxuICB9XHJcbiAgYXtcclxuICAgICAgY29sb3I6ICMzNTM1MzU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMS4ycmVtIDtcclxuICAgICAgJjpob3ZlcntcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgfVxyXG4gIH1cclxufVxyXG4ucGhvbmV7XHJcbkBleHRlbmQgLmVtYWlsO1xyXG59XHJcbn1cclxudWx7XHJcbiAgbGlzdC1zdHlsZTogbm9uZTtcclxuICBsaW5lLWhlaWdodDogMnJlbTtcclxuICBoMntcclxuICAgICAgY29sb3I6ICNDMDFBMjQ7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4gIH1cclxuICBhe1xyXG4gICAgICBjb2xvcjogIzM1MzUzNTtcclxuICB9XHJcblxyXG59XHJcbn1cclxuLmxpbmV7XHJcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjRDhEOEQ4O1xyXG4gIGhlaWdodDogNDAwcHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGxlZnQ6IDU1JTtcclxuICBtYXJnaW4tbGVmdDogLTNweDtcclxuICBcclxufVxyXG5AbWVkaWEobWF4LXdpZHRoOjQyNXB4KXtcclxuICBmb290ZXJ7XHJcbiAgICAgIC1tcy1ncmlkLWNvbHVtbnM6IG5vbmU7XHJcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogbm9uZTtcclxuICAgICAgLW1zLWdyaWQtcm93czogODAlIDIwJTtcclxuICAgICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiA4MCUgMjAlO1xyXG4gICAgICAuZm9vdGVyLWxpbmt7XHJcbiAgICAgICAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XHJcbiAgICAgICAgICBncmlkLWNvbHVtbjogMTtcclxuICAgICAgICAgIC1tcy1ncmlkLXJvdzogMTtcclxuICAgICAgICAgIGdyaWQtcm93OiAxO1xyXG4gICAgICB9XHJcbiAgICAgIC5icmFuZC1mb290ZXJ7XHJcbiAgICAgICAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XHJcbiAgICAgICAgICBncmlkLWNvbHVtbjogMTtcclxuICAgICAgICAgIC1tcy1ncmlkLXJvdzogMjtcclxuICAgICAgICAgIGdyaWQtcm93OiAyO1xyXG4gICAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICAgIHdpZHRoOiAxODBweDtcclxuICAgICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAubGluZXtcclxuICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgZm9vdGVyID4gKjpudGgtY2hpbGQoMSl7XHJcbiAgICAgIC1tcy1ncmlkLXJvdzogMTtcclxuICAgICAgLW1zLWdyaWQtY29sdW1uOiAxXHJcbiAgfVxyXG4gIGZvb3RlciA+ICo6bnRoLWNoaWxkKDIpe1xyXG4gICAgICAtbXMtZ3JpZC1yb3c6IDI7XHJcbiAgICAgIC1tcy1ncmlkLWNvbHVtbjogMVxyXG4gIH1cclxufVxyXG5AbWVkaWEgKG1pbi13aWR0aDo1NjBweClhbmQobWF4LXdpZHRoOjc2OHB4KXtcclxuICBmb290ZXJ7XHJcbiAgICAgIGhlaWdodDogNDIwcHg7XHJcbiAgICAgIGgye1xyXG4gICAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICAgIH1cclxuICAgICAgcHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTouOHJlbSA7IFxyXG4gICAgICB9XHJcbiAgICAgIGF7XHJcbiAgICAgICAgICBmb250LXNpemU6LjhyZW0gOyAgXHJcbiAgICAgIH1cclxuICAgICAgLmxpbmV7XHJcbiAgICAgICAgICBsZWZ0OiA1OSU7XHJcbiAgICAgICAgICBoZWlnaHQ6IDMxMnB4O1xyXG4gICAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4uZ3JkcCB7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICBwYWRkaW5nOiAxcmVtO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICB6LWluZGV4OiAxMDAwMDA7XHJcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBib3gtc2hhZG93OiAwIDAgIDMwcHggcmdiYSgwLDAsMCwwLjEpO1xyXG4gICAgY29sb3I6ICMzNTM1MzU7XHJcbiAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIH1cclxuICAgXHJcbiAgLmJjZ3JkcCB7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBoZWlnaHQ6IDNyZW07XHJcbiAgICB3aWR0aDogMjByZW07XHJcbiAgICBtYXJnaW4tbGVmdDogM3JlbTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNDMDFBMjQ7XHJcbiAgfVxyXG5cclxuICAvLyBOYXYtcmVzcG9zaXZlXHJcbi5uYXZiYXItbW9iaWxle1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIHBhZGRpbmc6IDFyZW07XHJcbn1cclxuLmJ0bi10b2dnbGV7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgYm9yZGVyLXJhZGl1czogLjVyZW0gO1xyXG4gICAgYm9yZGVyOiBub25lIDtcclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgYm94LXNoYWRvdzogMCAwIDIwcHggcmdiYSgkY29sb3I6ICMzNTM1MzUsICRhbHBoYTogLjIpO1xyXG4gICAgaXtcclxuICAgICAgICBjb2xvcjogI0MwMUEyNDtcclxuICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICAgICBwYWRkaW5nOiAuOHJlbTtcclxuXHJcbiAgICB9XHJcbn1cclxuLm5hdk1vYmlsZXtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLml0ZW0tbmF2e1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgICAgIHRyYW5zaXRpb246IGVhc2UtaW4tb3V0IDAuM3M7XHJcbiAgICAgICAgcGFkZGluZzogNHJlbTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IDA7XHJcbiAgICAgICAgbGVmdDogLTQzMHB4O1xyXG4gICAgICAgIGltZ3tcclxuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGF7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBjb2xvcjogIzM1MzUzNTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaDN7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpe1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuaHJ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG4ubmF2TW9iaWxlLW9we1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAuaXRlbS1uYXZ7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogZWFzZS1pbi1vdXQgMC4zcztcclxuICAgICAgICBwYWRkaW5nOiA0cmVtO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogMDtcclxuICAgICAgICBsZWZ0OiAwO1xyXG4gICAgICAgIGltZ3tcclxuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGF7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBjb2xvcjogIzM1MzUzNTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaDN7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpe1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA1NjBweCkgYW5kIChtYXgtd2lkdGg6NzY4cHgpe1xyXG4gICAgLm5hdk1vYmlsZXtcclxuICAgICAgICAuaXRlbS1uYXZ7XHJcblxyXG4gICAgICAgICAgICBsZWZ0OiAtNzcxcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbkBtZWRpYSAobWluLXdpZHRoOjgwMHB4KXtcclxuICAgIC5uYXZiYXItbW9iaWxle1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vLyBOYXZiYXIgQ2xhc3Nlc1xyXG4uYWN0aXZle1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgkY29sb3I6ICMwMDAwMDAsICRhbHBoYTogLjIpO1xyXG59XHJcbi5uYXZiYXItYnJhbmR7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuLm5hdmVnYWRvcntcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxufVxyXG4ucGJ4e1xyXG4gICAgYmFja2dyb3VuZDogI0MwMUEyNDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgcGFkZGluZzogLjVyZW0gM3JlbTtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgd2lkdGg6IDEwMHZ3O1xyXG4gICAgcHtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogM3JlbTtcclxuICAgIH1cclxufVxyXG5cclxuLm5hdmJhcntcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHdpZHRoOiAxMDB2dztcclxuICAgIHBhZGRpbmc6IDFyZW0gN3JlbTtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgYXtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB9XHJcbiAgICAubmF2YmFyLW5hdntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jb2xsYXBzZXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgLnNlcnZpY2Vze1xyXG4gICAgICAgIC5idG4tc2VydmljZXN7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGF7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjMzUzNTM1O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmZvbGxvd3tcclxuICAgIHotaW5kZXg6IDk5OTtcclxuICAgICY6aG92ZXJ7XHJcbiAgICAgICAgLmZvbGxvd3tcclxuICAgICAgICAgICAgYXtcclxuXHJcbiAgICAgICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogM3JlbTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLy8gc3RpY2t5IENsYXNzXHJcbi5uYXZiYXItc3RpY2t5e1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgkY29sb3I6ICNmZmYsICRhbHBoYTogLjgpO1xyXG4gICAgYm94LXNoYWRvdzogMCA2cHggMzBweCByZ2JhKCRjb2xvcjogIzM1MzUzNSwgJGFscGhhOiAuMyk7XHJcbiAgICBhe1xyXG4gICAgICAgIGNvbG9yOiAjMzUzNTM1O1xyXG4gICAgfVxyXG4gICAgLmNvbGxhcHNle1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgICAgICAuc2VydmljZXN7XHJcbiAgICAgICAgICAgIC5idG4tc2VydmljZXN7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzM1MzUzNTtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDoyNTZweClhbmQobWF4LXdpZHRoOjc2OHB4KXtcclxuICAgIC5uYXZiYXJ7XHJcbiAgICAgICAgcGFkZGluZzogMXJlbSAzcmVtO1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbiAgICBpbWd7XHJcbiAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgIH1cclxuICAgIC5wYnh7XHJcbiAgICAgICAgcHtcclxuICAgICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAubmF2YmFyLXN0aWNreXtcclxuICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgICAuYnRuLXNlcnZpY2Vze1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmJ0bi1wb3J0YWx7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFCNTA2RjtcclxuICBib3JkZXItY29sb3I6ICMxQjUwNkY7XHJcblxyXG59XHJcbiIsImFwcC1uYXZiYXIge1xuICB6LWluZGV4OiA5O1xuICBwb3NpdGlvbjogZml4ZWQ7XG59XG5cbi5mb290ZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAxMDAlO1xuICBib3R0b206IDA7XG4gIHotaW5kZXg6IDk7XG59XG5cbmZvb3RlciB7XG4gIG1hcmdpbi10b3A6IDZyZW07XG4gIGhlaWdodDogNTAwcHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJveC1zaGFkb3c6IDAgMCAzMHB4IHJnYmEoNTMsIDUzLCA1MywgMC4yKTtcbiAgLXdlYmtpdC1hbmltYXRpb246IG5vbmUgIWltcG9ydGFudDtcbiAgYW5pbWF0aW9uOiBub25lICFpbXBvcnRhbnQ7XG4gIGRpc3BsYXk6IC1tcy1ncmlkO1xuICBkaXNwbGF5OiBncmlkO1xuICAtbXMtZ3JpZC1jb2x1bW5zOiAzMCUgNzAlO1xuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDMwJSA3MCU7XG59XG5mb290ZXIgLmJyYW5kLWZvb3RlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAtbXMtZ3JpZC1jb2x1bW46IDE7XG4gIGdyaWQtY29sdW1uOiAxO1xufVxuZm9vdGVyIC5icmFuZC1mb290ZXIgaW1nIHtcbiAgd2lkdGg6IDM0OXB4O1xufVxuZm9vdGVyIC5mb290ZXItbGluayB7XG4gIHBhZGRpbmctdG9wOiA0cmVtO1xuICAtbXMtZ3JpZC1jb2x1bW46IDI7XG4gIGdyaWQtY29sdW1uOiAyO1xuICBkaXNwbGF5OiAtbXMtZ3JpZDtcbiAgZGlzcGxheTogZ3JpZDtcbiAgLW1zLWdyaWQtY29sdW1uczogNTAlIDUwJTtcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiA1MCUgNTAlO1xufVxuZm9vdGVyIC5mb290ZXItbGluayAuZm9vdGVyLXNlcnZpY2VzIHtcbiAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICBncmlkLWNvbHVtbjogMTtcbn1cbmZvb3RlciAuZm9vdGVyLWxpbmsgLmZvb3Rlci1zZXJ2aWNlcyAuZm9vdGVyLWNvbnRhY3Qge1xuICAtbXMtZ3JpZC1jb2x1bW46IDI7XG4gIGdyaWQtY29sdW1uOiAyO1xufVxuZm9vdGVyIC5mb290ZXItbGluayAuZW1haWwgaDIsIGZvb3RlciAuZm9vdGVyLWxpbmsgLnBob25lIGgyLCBmb290ZXIgLmZvb3Rlci1saW5rIC5zb2NpYWwgaDIge1xuICBmb250LXNpemU6IDEuNXJlbTtcbiAgY29sb3I6ICNDMDFBMjQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuZm9vdGVyIC5mb290ZXItbGluayAuc29jaWFsIGkge1xuICBmb250LWZhbWlseTogXCJGb250IEF3ZXNvbWUgNSBCcmFuZHNcIjtcbn1cbmZvb3RlciAuZm9vdGVyLWxpbmsgLnNvY2lhbCBhIHtcbiAgY29sb3I6ICMzNTM1MzU7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xufVxuZm9vdGVyIC5mb290ZXItbGluayAuc29jaWFsIGE6aG92ZXIge1xuICBmb250LXNpemU6IDFyZW07XG59XG5mb290ZXIgdWwge1xuICBsaXN0LXN0eWxlOiBub25lO1xuICBsaW5lLWhlaWdodDogMnJlbTtcbn1cbmZvb3RlciB1bCBoMiB7XG4gIGNvbG9yOiAjQzAxQTI0O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbn1cbmZvb3RlciB1bCBhIHtcbiAgY29sb3I6ICMzNTM1MzU7XG59XG5cbi5saW5lIHtcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjRDhEOEQ4O1xuICBoZWlnaHQ6IDQwMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDU1JTtcbiAgbWFyZ2luLWxlZnQ6IC0zcHg7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA0MjVweCkge1xuICBmb290ZXIge1xuICAgIC1tcy1ncmlkLWNvbHVtbnM6IG5vbmU7XG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiBub25lO1xuICAgIC1tcy1ncmlkLXJvd3M6IDgwJSAyMCU7XG4gICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiA4MCUgMjAlO1xuICB9XG4gIGZvb3RlciAuZm9vdGVyLWxpbmsge1xuICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcbiAgICBncmlkLWNvbHVtbjogMTtcbiAgICAtbXMtZ3JpZC1yb3c6IDE7XG4gICAgZ3JpZC1yb3c6IDE7XG4gIH1cbiAgZm9vdGVyIC5icmFuZC1mb290ZXIge1xuICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcbiAgICBncmlkLWNvbHVtbjogMTtcbiAgICAtbXMtZ3JpZC1yb3c6IDI7XG4gICAgZ3JpZC1yb3c6IDI7XG4gIH1cbiAgZm9vdGVyIC5icmFuZC1mb290ZXIgaW1nIHtcbiAgICB3aWR0aDogMTgwcHg7XG4gIH1cbiAgZm9vdGVyIC5saW5lIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgZm9vdGVyID4gKjpudGgtY2hpbGQoMSkge1xuICAgIC1tcy1ncmlkLXJvdzogMTtcbiAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XG4gIH1cblxuICBmb290ZXIgPiAqOm50aC1jaGlsZCgyKSB7XG4gICAgLW1zLWdyaWQtcm93OiAyO1xuICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDU2MHB4KSBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgZm9vdGVyIHtcbiAgICBoZWlnaHQ6IDQyMHB4O1xuICB9XG4gIGZvb3RlciBoMiB7XG4gICAgZm9udC1zaXplOiAxLjVyZW07XG4gIH1cbiAgZm9vdGVyIHAge1xuICAgIGZvbnQtc2l6ZTogMC44cmVtO1xuICB9XG4gIGZvb3RlciBhIHtcbiAgICBmb250LXNpemU6IDAuOHJlbTtcbiAgfVxuICBmb290ZXIgLmxpbmUge1xuICAgIGxlZnQ6IDU5JTtcbiAgICBoZWlnaHQ6IDMxMnB4O1xuICB9XG59XG4uZ3JkcCB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgcGFkZGluZzogMXJlbTtcbiAgYm90dG9tOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgei1pbmRleDogMTAwMDAwO1xuICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICBkaXNwbGF5OiBmbGV4O1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3JkZXI6IG5vbmU7XG4gIGJveC1zaGFkb3c6IDAgMCAzMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgY29sb3I6ICMzNTM1MzU7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLmJjZ3JkcCB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYm9yZGVyOiBub25lO1xuICBoZWlnaHQ6IDNyZW07XG4gIHdpZHRoOiAyMHJlbTtcbiAgbWFyZ2luLWxlZnQ6IDNyZW07XG4gIGJhY2tncm91bmQtY29sb3I6ICNDMDFBMjQ7XG59XG5cbi5uYXZiYXItbW9iaWxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIHBhZGRpbmc6IDFyZW07XG59XG5cbi5idG4tdG9nZ2xlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XG4gIGJvcmRlcjogbm9uZTtcbiAgei1pbmRleDogMTtcbiAgb3V0bGluZTogbm9uZTtcbiAgYm94LXNoYWRvdzogMCAwIDIwcHggcmdiYSg1MywgNTMsIDUzLCAwLjIpO1xufVxuLmJ0bi10b2dnbGUgaSB7XG4gIGNvbG9yOiAjQzAxQTI0O1xuICBmb250LXNpemU6IDEuNXJlbTtcbiAgcGFkZGluZzogMC44cmVtO1xufVxuXG4ubmF2TW9iaWxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5uYXZNb2JpbGUgLml0ZW0tbmF2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICB0cmFuc2l0aW9uOiBlYXNlLWluLW91dCAwLjNzO1xuICBwYWRkaW5nOiA0cmVtO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAtNDMwcHg7XG59XG4ubmF2TW9iaWxlIC5pdGVtLW5hdiBpbWcge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDUwJTtcbiAgbWFyZ2luLWJvdHRvbTogNXJlbTtcbn1cbi5uYXZNb2JpbGUgLml0ZW0tbmF2IGEge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6ICMzNTM1MzU7XG59XG4ubmF2TW9iaWxlIC5pdGVtLW5hdiBoMyB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xufVxuLm5hdk1vYmlsZSAuaXRlbS1uYXYgaSB7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xufVxuXG5ociB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IDA7XG59XG5cbi5uYXZNb2JpbGUtb3Age1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLm5hdk1vYmlsZS1vcCAuaXRlbS1uYXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIHRyYW5zaXRpb246IGVhc2UtaW4tb3V0IDAuM3M7XG4gIHBhZGRpbmc6IDRyZW07XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG59XG4ubmF2TW9iaWxlLW9wIC5pdGVtLW5hdiBpbWcge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDUwJTtcbiAgbWFyZ2luLWJvdHRvbTogNXJlbTtcbn1cbi5uYXZNb2JpbGUtb3AgLml0ZW0tbmF2IGEge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6ICMzNTM1MzU7XG59XG4ubmF2TW9iaWxlLW9wIC5pdGVtLW5hdiBoMyB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xufVxuLm5hdk1vYmlsZS1vcCAuaXRlbS1uYXYgaSB7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNTYwcHgpIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAubmF2TW9iaWxlIC5pdGVtLW5hdiB7XG4gICAgbGVmdDogLTc3MXB4O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogODAwcHgpIHtcbiAgLm5hdmJhci1tb2JpbGUge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbn1cbi5hY3RpdmUge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMik7XG59XG5cbi5uYXZiYXItYnJhbmQge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5uYXZlZ2Fkb3Ige1xuICBwb3NpdGlvbjogZml4ZWQ7XG59XG5cbi5wYngge1xuICBiYWNrZ3JvdW5kOiAjQzAxQTI0O1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBwYWRkaW5nOiAwLjVyZW0gM3JlbTtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIHdpZHRoOiAxMDB2dztcbn1cbi5wYnggcCB7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZy1yaWdodDogM3JlbTtcbn1cblxuLm5hdmJhciB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgd2lkdGg6IDEwMHZ3O1xuICBwYWRkaW5nOiAxcmVtIDdyZW07XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLm5hdmJhciBhIHtcbiAgY29sb3I6ICNmZmY7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5uYXZiYXIgLm5hdmJhci1uYXYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uY29sbGFwc2Uge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuLmNvbGxhcHNlIC5zZXJ2aWNlcyAuYnRuLXNlcnZpY2VzIHtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6IG5vbmU7XG4gIGJvcmRlcjogbm9uZTtcbiAgb3V0bGluZTogbm9uZTtcbn1cbi5jb2xsYXBzZSAuc2VydmljZXMgYSB7XG4gIGNvbG9yOiAjMzUzNTM1O1xufVxuXG4uZm9sbG93IHtcbiAgei1pbmRleDogOTk5O1xufVxuLmZvbGxvdzpob3ZlciAuZm9sbG93IGEgaSB7XG4gIGZvbnQtc2l6ZTogM3JlbTtcbn1cblxuLm5hdmJhci1zdGlja3kge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOCk7XG4gIGJveC1zaGFkb3c6IDAgNnB4IDMwcHggcmdiYSg1MywgNTMsIDUzLCAwLjMpO1xufVxuLm5hdmJhci1zdGlja3kgYSB7XG4gIGNvbG9yOiAjMzUzNTM1O1xufVxuLm5hdmJhci1zdGlja3kgLmNvbGxhcHNlIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cbi5uYXZiYXItc3RpY2t5IC5jb2xsYXBzZSAuc2VydmljZXMgLmJ0bi1zZXJ2aWNlcyB7XG4gIGNvbG9yOiAjMzUzNTM1O1xuICBiYWNrZ3JvdW5kOiBub25lO1xuICBib3JkZXI6IG5vbmU7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiAyNTZweCkgYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5uYXZiYXIge1xuICAgIHBhZGRpbmc6IDFyZW0gM3JlbTtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgaW1nIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgLnBieCBwIHtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG5cbiAgLm5hdmJhci1zdGlja3kge1xuICAgIGJhY2tncm91bmQ6IG5vbmU7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG4gIC5uYXZiYXItc3RpY2t5IC5idG4tc2VydmljZXMge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbn1cbi5idG4tcG9ydGFsIHtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxQjUwNkY7XG4gIGJvcmRlci1jb2xvcjogIzFCNTA2Rjtcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.scss']
            }]
    }], function () { return [{ type: ngx_cookie_service__WEBPACK_IMPORTED_MODULE_1__["CookieService"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Meta"] }, { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }, { type: _servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"] }, { type: _servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"] }]; }, { close: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }] }); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: HttpLoaderFactory, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/navbar/navbar.component */ "./src/app/components/navbar/navbar.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/__ivy_ngcc__/fesm2015/ng-bootstrap.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _servicios_contacto_contacto_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./servicios/contacto/contacto.service */ "./src/app/servicios/contacto/contacto.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/__ivy_ngcc__/fesm2015/ngx-translate-http-loader.js");









// Translate





//import { MultiTranslateHttpLoader } from "ngx-translate-multi-http-loader";
function HttpLoaderFactory(http) {
    //return new TranslateHttpLoader(http);
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_10__["TranslateHttpLoader"](http, './assets/i18n/', '.json');
}
class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [_servicios_contacto_contacto_service__WEBPACK_IMPORTED_MODULE_8__["ContactoService"]], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"].withServerTransition({ appId: 'serverApp' }),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateModule"].forRoot({
                loader: {
                    provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateLoader"],
                    useFactory: HttpLoaderFactory,
                    deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"]],
                },
            }),
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__["NavbarComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
        _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__["NavbarComponent"]
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"].withServerTransition({ appId: 'serverApp' }),
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                    _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                    _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateModule"].forRoot({
                        loader: {
                            provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateLoader"],
                            useFactory: HttpLoaderFactory,
                            deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"]],
                        },
                    }),
                ],
                providers: [_servicios_contacto_contacto_service__WEBPACK_IMPORTED_MODULE_8__["ContactoService"]],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.ts ***!
  \*******************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/servicios/i18nservice/language.service */ "./src/app/servicios/i18nservice/language.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/__ivy_ngcc__/fesm2015/ng-bootstrap.js");







function NavbarComponent_img_45_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 41);
} }
function NavbarComponent_img_46_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 42);
} }
const _c0 = function (a0) { return [a0]; };
const _c1 = function () { return ["blog"]; };
const _c2 = function (a0) { return { "active": a0 }; };
const _c3 = function () { return [""]; };
const _c4 = function () { return ["/en"]; };
const _c5 = function () { return ["/blog"]; };
class NavbarComponent {
    constructor(translate, router, lang, sharedService) {
        //this.translate.setDefaultLang(av);
        this.translate = translate;
        this.router = router;
        this.lang = lang;
        this.sharedService = sharedService;
        this.activeLang = localStorage.getItem("idioma") ? localStorage.getItem("idioma") : 'es';
        this.navMobile = false;
        this.bandera = 'es';
        this.rservices = '';
        this.rcontact = '';
        this.rfamily = '';
    }
    ngOnInit() {
        this.lang.localeEvent.subscribe(locale => this.translate.use('en'));
        this.sharedService.Message.subscribe(m => { this.message = m; });
        if (this.activeLang == "\"en\"") {
            this.bandera = 'en';
            this.rservices = '/en';
            this.rcontact = '/contact';
            this.rfamily = '/join-our-family';
        }
        else if (this.activeLang == "\"es\"") {
            this.bandera = 'es';
            this.rservices = '/';
            this.rcontact = '/contacto';
            this.rfamily = '/unete-a-nuestro-equipo';
        }
    }
    cambiarLenguaje(lang) {
        this.activeLang = lang;
        this.translate.use(lang);
        localStorage.setItem('idioma', JSON.stringify(`${lang}`));
        const idio = localStorage.getItem("idioma");
        this.sharedService.nextMessage('message prueba');
        this.lang.idioma$.emit(lang);
        if (this.activeLang == 'es') {
            this.rservices = '/';
            this.rcontact = '/contacto';
            this.rfamily = '/unete-a-nuestro-equipo';
            console.log('ruta contacto', this.rcontact);
        }
        else if (this.activeLang == 'en') {
            this.rservices = '/en';
            this.rcontact = '/contact';
            this.rfamily = '/join-our-family';
            console.log('ruta', this.rcontact);
        }
    }
    onWindowScroll(e) {
        if (window.pageYOffset > 50) {
            let element = document.getElementById('navbar');
            element.classList.add('navbar-sticky');
            this.brand = 1;
        }
        else {
            let element = document.getElementById('navbar');
            element.classList.remove('navbar-sticky');
            this.brand = 2;
        }
    }
}
NavbarComponent.ɵfac = function NavbarComponent_Factory(t) { return new (t || NavbarComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"])); };
NavbarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NavbarComponent, selectors: [["app-navbar"]], hostBindings: function NavbarComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("scroll", function NavbarComponent_scroll_HostBindingHandler($event) { return ctx.onWindowScroll($event); }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveWindow"]);
    } }, decls: 89, vars: 96, consts: [[1, "navegador"], [1, "pbx"], [1, "navbar-mobile"], [1, "btn-toggle", 3, "click"], [1, "fas", "fa-bars"], [1, "item-nav"], ["src", "../../../assets/img/brand-color.svg", "alt", "COS-logo"], [3, "routerLink"], ["href", "https://rrhh.outsourcingcos.com/RRHH", "target", "blank"], [1, "contenedor-botones"], [1, "btn", "btn-transparent", 3, "ngClass", "routerLink", "click"], [1, "flag-icon", "flag-icon-co", "flag-icon-squared"], [1, "flag-icon", "flag-icon-us", "flag-icon-squared"], [1, "follow"], ["href", "https://www.facebook.com/Outsourcingcos/", "target", "blank"], [1, "fab", "fa-facebook-square"], ["href", "https://www.instagram.com/cosoutsourcing/", "target", "blank"], [1, "fab", "fa-instagram-square", "ml-3"], ["href", "https://www.linkedin.com/company/colombianoutsourcingcos/about/", "target", "blank"], [1, "fab", "fa-linkedin", "ml-3"], ["href", "https://www.youtube.com/channel/UCL89AlspoSj_exe6qidqHew", "target", "blank"], [1, "fab", "fa-youtube", "ml-3"], ["id", "navbar", 1, "navbar", "navbar-expand-lg"], [1, "navbar-brand", 3, "routerLink"], ["src", "assets/img/brand.svg", 4, "ngIf"], ["src", "assets/img/brand-color.svg", 4, "ngIf"], ["id", "navbarSupportedContent", 1, "collapse", "navbar-collapse"], [1, "navbar-nav"], [1, "text-right", "services"], ["ngbDropdown", "", "placement", "bottom-right", 1, "d-inline-block"], ["id", "services", "ngbDropdownToggle", "", 1, "btn-services"], ["ngbDropdownMenu", "", "aria-labelledby", "rservices"], ["ngbDropdownItem", "", 3, "href"], [1, "nav-item"], ["routerLinkActive", "active", 1, "nav-link", 3, "routerLink"], ["ngbDropdown", "", 1, "d-inline-block"], ["id", "dropdownBasic1", "ngbDropdownToggle", "", 1, "btn", "btn-portal"], ["ngbDropdownMenu", "", "aria-labelledby", "dropdownBasic1"], ["href", "https://rrhh.outsourcingcos.com/RRHH", "target", "_blank", "ngbDropdownItem", "", 2, "color", "black", "text-decoration", "none"], ["href", "http://portaltransaccional.outsourcingcos.com/", "target", "_blank", "ngbDropdownItem", "", 2, "color", "black", "text-decoration", "none"], ["routerLinkActive", "router-link-active", 1, "btn", "btn-transparent", 3, "ngClass", "routerLink", "click"], ["src", "assets/img/brand.svg"], ["src", "assets/img/brand-color.svg"]], template: function NavbarComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](4, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NavbarComponent_Template_button_click_6_listener() { return ctx.navMobile = !ctx.navMobile; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "i", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "img", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](13, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](16, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](19, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](22, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](25, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NavbarComponent_Template_button_click_27_listener() { return ctx.cambiarLenguaje("es"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NavbarComponent_Template_button_click_29_listener() { return ctx.cambiarLenguaje("en"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "span", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](33, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "a", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "i", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "a", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "i", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "a", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "i", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "i", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "nav", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](45, NavbarComponent_img_45_Template, 1, 0, "img", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](46, NavbarComponent_img_46_Template, 1, 0, "img", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "ul", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "button", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](53, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](57, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](60, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "li", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "a", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](64, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "li", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "a", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](68, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "li", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "a", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](72, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "button", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](76, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "a", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](80, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "a", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](83, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NavbarComponent_Template_button_click_85_listener() { return ctx.cambiarLenguaje("es"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](86, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "button", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NavbarComponent_Template_button_click_87_listener() { return ctx.cambiarLenguaje("en"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "span", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](4, 38, "navbar.phone"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.navMobile ? "navMobile-op" : "navMobile");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](70, _c0, ctx.rservices));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](13, 40, "navbar.start"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](72, _c0, ctx.rcontact));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](16, 42, "navbar.business"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](74, _c0, ctx.rfamily));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](19, 44, "navbar.family"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](76, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](22, 46, "navbar.blog"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](25, 48, "navbar.portal"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](77, _c2, ctx.activeLang == "es"))("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](79, _c3));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](80, _c2, ctx.activeLang == "en"))("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](82, _c4));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](33, 50, "navbar.follow"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](83, _c0, ctx.rservices));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.brand == 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.brand == 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](53, 52, "navbar.services"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("href", "", ctx.rservices, "#BPO", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](57, 54, "navbar.bpo"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("href", "", ctx.rservices, "#tecno", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](60, 56, "navbar.tech"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](85, _c0, ctx.rcontact));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](64, 58, "navbar.business"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](87, _c0, ctx.rfamily));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](68, 60, "navbar.family"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](89, _c5));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](72, 62, "navbar.blog"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](76, 64, "navbar.portal"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](80, 66, "navbar.collab"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](83, 68, "navbar.proveedor"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](90, _c2, ctx.translate.currentLang == "es"))("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](92, _c3));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](93, _c2, ctx.translate.currentLang == "en"))("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](95, _c4));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgClass"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLink"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbNavbar"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbDropdown"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbDropdownToggle"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbDropdownMenu"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbDropdownItem"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkActive"]], pipes: [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslatePipe"]], styles: [".navbar-mobile[_ngcontent-%COMP%] {\n  width: 100%;\n  display: flex;\n  justify-content: flex-end;\n  padding: 1rem;\n}\n\n.btn-toggle[_ngcontent-%COMP%] {\n  background: #fff;\n  position: fixed;\n  border-radius: 0.5rem;\n  border: none;\n  z-index: 1;\n  outline: none;\n  box-shadow: 0 0 20px rgba(53, 53, 53, 0.2);\n}\n\n.btn-toggle[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  color: #C01A24;\n  font-size: 1.5rem;\n  padding: 0.8rem;\n}\n\n.navMobile[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n}\n\n.navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  transition: ease-in-out 0.3s;\n  padding: 4rem;\n  background: white;\n  width: 100%;\n  height: 100vh;\n  position: absolute;\n  top: 0;\n  left: -430px;\n}\n\n.navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  display: block;\n  width: 50%;\n  margin-bottom: 5rem;\n}\n\n.navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  width: 100%;\n  color: #353535;\n}\n\n.navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  width: 100%;\n  margin-bottom: 1rem;\n}\n\n.navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\nhr[_ngcontent-%COMP%] {\n  width: 100%;\n  margin: 0;\n}\n\n.navMobile-op[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n}\n\n.navMobile-op[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  transition: ease-in-out 0.3s;\n  padding: 4rem;\n  background: white;\n  width: 100%;\n  height: 100vh;\n  position: absolute;\n  top: 0;\n  left: 0;\n}\n\n.navMobile-op[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  display: block;\n  width: 50%;\n  margin-bottom: 5rem;\n}\n\n.navMobile-op[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  width: 100%;\n  color: #353535;\n}\n\n.navMobile-op[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  width: 100%;\n  margin-bottom: 1rem;\n}\n\n.navMobile-op[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .navMobile[_ngcontent-%COMP%]   .item-nav[_ngcontent-%COMP%] {\n    left: -771px;\n  }\n}\n\n@media (min-width: 800px) {\n  .navbar-mobile[_ngcontent-%COMP%] {\n    display: none;\n  }\n}\n\n.active[_ngcontent-%COMP%] {\n  background: rgba(0, 0, 0, 0.2);\n}\n\n.navbar-brand[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n\n.navegador[_ngcontent-%COMP%] {\n  position: fixed;\n}\n\n.pbx[_ngcontent-%COMP%] {\n  background: #C01A24;\n  color: #fff;\n  font-size: 1rem;\n  padding: 0.5rem 3rem;\n  text-align: right;\n  width: 100vw;\n}\n\n.pbx[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  margin: 0;\n  padding-right: 3rem;\n}\n\n.navbar[_ngcontent-%COMP%] {\n  position: fixed;\n  width: 100vw;\n  padding: 1rem 7rem;\n  background: transparent;\n}\n\n.navbar[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #fff;\n  cursor: pointer;\n}\n\n.navbar[_ngcontent-%COMP%]   .navbar-nav[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n}\n\n.collapse[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n}\n\n.collapse[_ngcontent-%COMP%]   .services[_ngcontent-%COMP%]   .btn-services[_ngcontent-%COMP%] {\n  color: #fff;\n  background: none;\n  border: none;\n  outline: none;\n}\n\n.collapse[_ngcontent-%COMP%]   .services[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #353535;\n}\n\n.follow[_ngcontent-%COMP%] {\n  z-index: 999;\n}\n\n.follow[_ngcontent-%COMP%]:hover   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 3rem;\n}\n\n.navbar-sticky[_ngcontent-%COMP%] {\n  background: rgba(255, 255, 255, 0.8);\n  box-shadow: 0 6px 30px rgba(53, 53, 53, 0.3);\n}\n\n.navbar-sticky[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #353535;\n}\n\n.navbar-sticky[_ngcontent-%COMP%]   .collapse[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n}\n\n.navbar-sticky[_ngcontent-%COMP%]   .collapse[_ngcontent-%COMP%]   .services[_ngcontent-%COMP%]   .btn-services[_ngcontent-%COMP%] {\n  color: #353535;\n  background: none;\n  border: none;\n  outline: none;\n}\n\n@media (min-width: 256px) and (max-width: 768px) {\n  .navbar[_ngcontent-%COMP%] {\n    padding: 1rem 3rem;\n    display: none;\n  }\n\n  img[_ngcontent-%COMP%] {\n    display: none;\n  }\n\n  .pbx[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    padding: 0;\n  }\n\n  .navbar-sticky[_ngcontent-%COMP%] {\n    background: none;\n    box-shadow: none;\n    display: none;\n  }\n  .navbar-sticky[_ngcontent-%COMP%]   .btn-services[_ngcontent-%COMP%] {\n    display: none;\n  }\n}\n\n.btn-portal[_ngcontent-%COMP%] {\n  color: #fff;\n  background-color: #1B506F;\n  border-color: #1B506F;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9uYXZiYXIvRDpcXHByb3llY3Rvc1xcd2Vic2l0ZS1jb3Mvc3JjXFxhcHBcXGNvbXBvbmVudHNcXG5hdmJhclxcbmF2YmFyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL25hdmJhci9uYXZiYXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsYUFBQTtBQ0FKOztBREVBO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSwwQ0FBQTtBQ0NKOztBREFJO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ0VSOztBREVBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNDSjs7QURBSTtFQUNJLGFBQUE7RUFDQSxlQUFBO0VBQ0EsNEJBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFlBQUE7QUNFUjs7QUREUTtFQUNJLGNBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7QUNHWjs7QUREUTtFQUNJLFdBQUE7RUFDQSxjQUFBO0FDR1o7O0FERFE7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7QUNHWjs7QUREUTtFQUNJLGlCQUFBO0FDR1o7O0FEQ0E7RUFDSSxXQUFBO0VBQ0EsU0FBQTtBQ0VKOztBREFBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNHSjs7QURGSTtFQUNJLGFBQUE7RUFDQSxlQUFBO0VBQ0EsNEJBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7QUNJUjs7QURIUTtFQUNJLGNBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7QUNLWjs7QURIUTtFQUNJLFdBQUE7RUFDQSxjQUFBO0FDS1o7O0FESFE7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7QUNLWjs7QURIUTtFQUNJLGlCQUFBO0FDS1o7O0FEQUE7RUFFUTtJQUVJLFlBQUE7RUNDVjtBQUNGOztBREVBO0VBQ0k7SUFDSSxhQUFBO0VDQU47QUFDRjs7QURJQTtFQUNJLDhCQUFBO0FDRko7O0FESUE7RUFDSSxlQUFBO0FDREo7O0FER0E7RUFDSSxlQUFBO0FDQUo7O0FERUE7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUNDSjs7QURBSTtFQUNJLFNBQUE7RUFDQSxtQkFBQTtBQ0VSOztBREVBO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0FDQ0o7O0FEQUk7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQ0VSOztBREFJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDRVI7O0FERUE7RUFDSSxhQUFBO0VBQ0EseUJBQUE7QUNDSjs7QURDUTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0FDQ1o7O0FEQ1E7RUFDSSxjQUFBO0FDQ1o7O0FESUE7RUFDSSxZQUFBO0FDREo7O0FETWdCO0VBQ0ksZUFBQTtBQ0pwQjs7QURZQTtFQUNJLG9DQUFBO0VBQ0EsNENBQUE7QUNUSjs7QURVSTtFQUNJLGNBQUE7QUNSUjs7QURVSTtFQUNJLGFBQUE7RUFDQSx5QkFBQTtBQ1JSOztBRFVZO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUNSaEI7O0FEY0E7RUFDSTtJQUNJLGtCQUFBO0lBQ0EsYUFBQTtFQ1hOOztFRGFFO0lBQ0ksYUFBQTtFQ1ZOOztFRGFNO0lBQ0ksVUFBQTtFQ1ZWOztFRGFFO0lBQ0ksZ0JBQUE7SUFDQSxnQkFBQTtJQUNBLGFBQUE7RUNWTjtFRFdNO0lBQ0ksYUFBQTtFQ1RWO0FBQ0Y7O0FEYUE7RUFDRSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtBQ1hGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTmF2LXJlc3Bvc2l2ZVxyXG4ubmF2YmFyLW1vYmlsZXtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICBwYWRkaW5nOiAxcmVtO1xyXG59XHJcbi5idG4tdG9nZ2xle1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIGJvcmRlci1yYWRpdXM6IC41cmVtIDtcclxuICAgIGJvcmRlcjogbm9uZSA7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIGJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoJGNvbG9yOiAjMzUzNTM1LCAkYWxwaGE6IC4yKTtcclxuICAgIGl7XHJcbiAgICAgICAgY29sb3I6ICNDMDFBMjQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICAgICAgcGFkZGluZzogLjhyZW07XHJcblxyXG4gICAgfVxyXG59XHJcbi5uYXZNb2JpbGV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIC5pdGVtLW5hdntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICB0cmFuc2l0aW9uOiBlYXNlLWluLW91dCAwLjNzO1xyXG4gICAgICAgIHBhZGRpbmc6IDRyZW07XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIGxlZnQ6IC00MzBweDtcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA1cmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBhe1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgY29sb3I6ICMzNTM1MzU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGgze1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaXtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbmhye1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDA7XHJcbn1cclxuLm5hdk1vYmlsZS1vcHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLml0ZW0tbmF2e1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgICAgIHRyYW5zaXRpb246IGVhc2UtaW4tb3V0IDAuM3M7XHJcbiAgICAgICAgcGFkZGluZzogNHJlbTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IDA7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA1cmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBhe1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgY29sb3I6ICMzNTM1MzU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGgze1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaXtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogNTYwcHgpIGFuZCAobWF4LXdpZHRoOjc2OHB4KXtcclxuICAgIC5uYXZNb2JpbGV7XHJcbiAgICAgICAgLml0ZW0tbmF2e1xyXG5cclxuICAgICAgICAgICAgbGVmdDogLTc3MXB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5AbWVkaWEgKG1pbi13aWR0aDo4MDBweCl7XHJcbiAgICAubmF2YmFyLW1vYmlsZXtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG59XHJcblxyXG4vLy8gTmF2YmFyIENsYXNzZXNcclxuLmFjdGl2ZXtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoJGNvbG9yOiAjMDAwMDAwLCAkYWxwaGE6IC4yKTtcclxufVxyXG4ubmF2YmFyLWJyYW5ke1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5uYXZlZ2Fkb3J7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuLnBieHtcclxuICAgIGJhY2tncm91bmQ6ICNDMDFBMjQ7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgIHBhZGRpbmc6IC41cmVtIDNyZW07XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgIHdpZHRoOiAxMDB2dztcclxuICAgIHB7XHJcbiAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDNyZW07XHJcbiAgICB9XHJcbn1cclxuXHJcbi5uYXZiYXJ7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB3aWR0aDogMTAwdnc7XHJcbiAgICBwYWRkaW5nOiAxcmVtIDdyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIGF7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgfVxyXG4gICAgLm5hdmJhci1uYXZ7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG59XHJcblxyXG4uY29sbGFwc2V7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIC5zZXJ2aWNlc3tcclxuICAgICAgICAuYnRuLXNlcnZpY2Vze1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogbm9uZTtcclxuICAgICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgIH1cclxuICAgICAgICBhe1xyXG4gICAgICAgICAgICBjb2xvcjogIzM1MzUzNTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5mb2xsb3d7XHJcbiAgICB6LWluZGV4OiA5OTk7XHJcbiAgICAmOmhvdmVye1xyXG4gICAgICAgIC5mb2xsb3d7XHJcbiAgICAgICAgICAgIGF7XHJcblxyXG4gICAgICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDNyZW07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIHN0aWNreSBDbGFzc1xyXG4ubmF2YmFyLXN0aWNreXtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoJGNvbG9yOiAjZmZmLCAkYWxwaGE6IC44KTtcclxuICAgIGJveC1zaGFkb3c6IDAgNnB4IDMwcHggcmdiYSgkY29sb3I6ICMzNTM1MzUsICRhbHBoYTogLjMpO1xyXG4gICAgYXtcclxuICAgICAgICBjb2xvcjogIzM1MzUzNTtcclxuICAgIH1cclxuICAgIC5jb2xsYXBzZXtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICAgICAgLnNlcnZpY2Vze1xyXG4gICAgICAgICAgICAuYnRuLXNlcnZpY2Vze1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICMzNTM1MzU7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6MjU2cHgpYW5kKG1heC13aWR0aDo3NjhweCl7XHJcbiAgICAubmF2YmFye1xyXG4gICAgICAgIHBhZGRpbmc6IDFyZW0gM3JlbTtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG4gICAgaW1ne1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbiAgICAucGJ4e1xyXG4gICAgICAgIHB7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLm5hdmJhci1zdGlja3l7XHJcbiAgICAgICAgYmFja2dyb3VuZDogbm9uZTtcclxuICAgICAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgLmJ0bi1zZXJ2aWNlc3tcclxuICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5idG4tcG9ydGFse1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMxQjUwNkY7XHJcbiAgYm9yZGVyLWNvbG9yOiAjMUI1MDZGO1xyXG5cclxufVxyXG4iLCIubmF2YmFyLW1vYmlsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBwYWRkaW5nOiAxcmVtO1xufVxuXG4uYnRuLXRvZ2dsZSB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xuICBib3JkZXI6IG5vbmU7XG4gIHotaW5kZXg6IDE7XG4gIG91dGxpbmU6IG5vbmU7XG4gIGJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoNTMsIDUzLCA1MywgMC4yKTtcbn1cbi5idG4tdG9nZ2xlIGkge1xuICBjb2xvcjogI0MwMUEyNDtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIHBhZGRpbmc6IDAuOHJlbTtcbn1cblxuLm5hdk1vYmlsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4ubmF2TW9iaWxlIC5pdGVtLW5hdiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAgdHJhbnNpdGlvbjogZWFzZS1pbi1vdXQgMC4zcztcbiAgcGFkZGluZzogNHJlbTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogLTQzMHB4O1xufVxuLm5hdk1vYmlsZSAuaXRlbS1uYXYgaW1nIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiA1MCU7XG4gIG1hcmdpbi1ib3R0b206IDVyZW07XG59XG4ubmF2TW9iaWxlIC5pdGVtLW5hdiBhIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiAjMzUzNTM1O1xufVxuLm5hdk1vYmlsZSAuaXRlbS1uYXYgaDMge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbn1cbi5uYXZNb2JpbGUgLml0ZW0tbmF2IGkge1xuICBmb250LXNpemU6IDEuNXJlbTtcbn1cblxuaHIge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubmF2TW9iaWxlLW9wIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5uYXZNb2JpbGUtb3AgLml0ZW0tbmF2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICB0cmFuc2l0aW9uOiBlYXNlLWluLW91dCAwLjNzO1xuICBwYWRkaW5nOiA0cmVtO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xufVxuLm5hdk1vYmlsZS1vcCAuaXRlbS1uYXYgaW1nIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiA1MCU7XG4gIG1hcmdpbi1ib3R0b206IDVyZW07XG59XG4ubmF2TW9iaWxlLW9wIC5pdGVtLW5hdiBhIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiAjMzUzNTM1O1xufVxuLm5hdk1vYmlsZS1vcCAuaXRlbS1uYXYgaDMge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbn1cbi5uYXZNb2JpbGUtb3AgLml0ZW0tbmF2IGkge1xuICBmb250LXNpemU6IDEuNXJlbTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDU2MHB4KSBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgLm5hdk1vYmlsZSAuaXRlbS1uYXYge1xuICAgIGxlZnQ6IC03NzFweDtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDgwMHB4KSB7XG4gIC5uYXZiYXItbW9iaWxlIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG59XG4uYWN0aXZlIHtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuXG4ubmF2YmFyLWJyYW5kIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4ubmF2ZWdhZG9yIHtcbiAgcG9zaXRpb246IGZpeGVkO1xufVxuXG4ucGJ4IHtcbiAgYmFja2dyb3VuZDogI0MwMUEyNDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgcGFkZGluZzogMC41cmVtIDNyZW07XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICB3aWR0aDogMTAwdnc7XG59XG4ucGJ4IHAge1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmctcmlnaHQ6IDNyZW07XG59XG5cbi5uYXZiYXIge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDB2dztcbiAgcGFkZGluZzogMXJlbSA3cmVtO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cbi5uYXZiYXIgYSB7XG4gIGNvbG9yOiAjZmZmO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4ubmF2YmFyIC5uYXZiYXItbmF2IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLmNvbGxhcHNlIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cbi5jb2xsYXBzZSAuc2VydmljZXMgLmJ0bi1zZXJ2aWNlcyB7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kOiBub25lO1xuICBib3JkZXI6IG5vbmU7XG4gIG91dGxpbmU6IG5vbmU7XG59XG4uY29sbGFwc2UgLnNlcnZpY2VzIGEge1xuICBjb2xvcjogIzM1MzUzNTtcbn1cblxuLmZvbGxvdyB7XG4gIHotaW5kZXg6IDk5OTtcbn1cbi5mb2xsb3c6aG92ZXIgLmZvbGxvdyBhIGkge1xuICBmb250LXNpemU6IDNyZW07XG59XG5cbi5uYXZiYXItc3RpY2t5IHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjgpO1xuICBib3gtc2hhZG93OiAwIDZweCAzMHB4IHJnYmEoNTMsIDUzLCA1MywgMC4zKTtcbn1cbi5uYXZiYXItc3RpY2t5IGEge1xuICBjb2xvcjogIzM1MzUzNTtcbn1cbi5uYXZiYXItc3RpY2t5IC5jb2xsYXBzZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG4ubmF2YmFyLXN0aWNreSAuY29sbGFwc2UgLnNlcnZpY2VzIC5idG4tc2VydmljZXMge1xuICBjb2xvcjogIzM1MzUzNTtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgYm9yZGVyOiBub25lO1xuICBvdXRsaW5lOiBub25lO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogMjU2cHgpIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAubmF2YmFyIHtcbiAgICBwYWRkaW5nOiAxcmVtIDNyZW07XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIGltZyB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuXG4gIC5wYnggcCB7XG4gICAgcGFkZGluZzogMDtcbiAgfVxuXG4gIC5uYXZiYXItc3RpY2t5IHtcbiAgICBiYWNrZ3JvdW5kOiBub25lO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuICAubmF2YmFyLXN0aWNreSAuYnRuLXNlcnZpY2VzIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG59XG4uYnRuLXBvcnRhbCB7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMUI1MDZGO1xuICBib3JkZXItY29sb3I6ICMxQjUwNkY7XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NavbarComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-navbar',
                templateUrl: './navbar.component.html',
                styleUrls: ['./navbar.component.scss']
            }]
    }], function () { return [{ type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }, { type: src_app_servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"] }, { type: src_app_servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"] }]; }, { onWindowScroll: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['window:scroll', ['$event']]
        }] }); })();


/***/ }),

/***/ "./src/app/pages/home/home.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/home/home.component.ts ***!
  \**********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var src_app_servicios_contacto_contacto_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/servicios/contacto/contacto.service */ "./src/app/servicios/contacto/contacto.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/servicios/i18nservice/language.service */ "./src/app/servicios/i18nservice/language.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/__ivy_ngcc__/fesm2015/ng-bootstrap.js");












function HomeComponent_div_55_h3_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("+", ctx_r11.teleT, "");
} }
function HomeComponent_div_55_h3_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("+", ctx_r12.clien, "");
} }
function HomeComponent_div_55_h3_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r13.sedes);
} }
function HomeComponent_div_55_h3_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("+", ctx_r14.inter, " ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 2, "home.mill"), "");
} }
function HomeComponent_div_55_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, HomeComponent_div_55_h3_4_Template, 2, 1, "h3", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, HomeComponent_div_55_h3_5_Template, 2, 1, "h3", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, HomeComponent_div_55_h3_6_Template, 2, 1, "h3", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, HomeComponent_div_55_h3_7_Template, 3, 4, "h3", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r10 = ctx.$implicit;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r0.bandera == "en");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](item_r10.class);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", item_r10.id == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", item_r10.id == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", item_r10.id == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", item_r10.id == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", item_r10.texto, " ");
} }
function HomeComponent_div_56_h3_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("+", ctx_r16.teleT, "");
} }
function HomeComponent_div_56_h3_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("+", ctx_r17.clien, "");
} }
function HomeComponent_div_56_h3_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r18.sedes);
} }
function HomeComponent_div_56_h3_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "translate");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("+", ctx_r19.inter, " ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 2, "home.mill"), "");
} }
function HomeComponent_div_56_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, HomeComponent_div_56_h3_4_Template, 2, 1, "h3", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, HomeComponent_div_56_h3_5_Template, 2, 1, "h3", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, HomeComponent_div_56_h3_6_Template, 2, 1, "h3", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, HomeComponent_div_56_h3_7_Template, 3, 4, "h3", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r15 = ctx.$implicit;
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r1.bandera != "en");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](item_r15.class);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", item_r15.id == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", item_r15.id == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", item_r15.id == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", item_r15.id == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", item_r15.texto, " ");
} }
function HomeComponent_div_64_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h2", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const card_r20 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r20.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", card_r20.description, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r20.item);
} }
function HomeComponent_div_67_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h2", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const card_r21 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r21.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", card_r21.description, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r21.item);
} }
function HomeComponent_div_75_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h2", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const card_r22 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", card_r22.img, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r22.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", card_r22.description, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r22.item);
} }
function HomeComponent_div_78_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h2", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const card_r23 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", card_r23.img, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r23.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", card_r23.description, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](card_r23.item);
} }
function HomeComponent_ng_template_89_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r6.bandera == "en");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r6.bandera != "en");
} }
function HomeComponent_ng_template_90_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r7.bandera == "en");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r7.bandera != "en");
} }
function HomeComponent_ng_template_91_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r8.bandera == "en");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r8.bandera != "en");
} }
function HomeComponent_ng_template_92_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r9.bandera == "en");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx_r9.bandera != "en");
} }
const _c0 = function () { return ["politicas-de-privacidad"]; };
class HomeComponent {
    constructor(builder, meta, title, conService, translate, router, sharedService) {
        this.builder = builder;
        this.meta = meta;
        this.title = title;
        this.conService = conService;
        this.translate = translate;
        this.router = router;
        this.sharedService = sharedService;
        this.activeLang = localStorage.getItem("idioma") ? localStorage.getItem("idioma") : 'es';
        this.language1 = false;
        this.language2 = true;
        this.kpiSection1 = [
            {
                id: 1,
                class: "fa-users",
                number: 0,
                texto: "Puestos de Trabajo",
            },
            {
                id: 2,
                class: "fa-user-tie",
                number: 0,
                texto: "Clientes Empresariales",
            },
            {
                id: 3,
                class: "fa-map-marker-alt",
                number: 0,
                texto: "Sedes en Colombia",
            },
            {
                id: 4,
                class: "fa-thumbs-up",
                number: 0,
                texto: "Interacciones al día",
            },
        ];
        this.kpiSection2 = [
            {
                id: 1,
                class: "fa-users",
                number: 0,
                texto: "work stations",
            },
            {
                id: 2,
                class: "fa-user-tie",
                number: 0,
                texto: "customers",
            },
            {
                id: 3,
                class: "fa-map-marker-alt",
                number: 0,
                texto: "sites",
            },
            {
                id: 4,
                class: "fa-thumbs-up",
                number: 0,
                texto: "daily interactions",
            },
        ];
        this.cardText1 = [
            {
                title: "VENTAS",
                description: "Nuestra Operación es reconocida como uno de los mejores outsourcing en ventas. Está enfocada por expertos en el entendimiento y conocimiento integral de su negocio, se alinea con las áreas comerciales de nuestros clientes para capturar valor de forma eficiente contribuyendo al cumplimiento de los objetivos estratégicos.",
                item: "· Generación de estrategias comerciales · Colocación de Productos Financieros y Seguros. · Cross Selling /Up Selling. · Ventas con pagos en línea (bajo estándares PCI) · Consecución de Leads.",
            },
            {
                title: "BACKOFFICE Y AUDITORÍA",
                description: "Somos expertos en operaciones de back office altamente complejas.  Utilizamos herramientas y tecnología para optimizar procesos y garantizar una alta productividad a los servicios de su empresa.",
                item: "· Digitación · Administración y respuestas de requerimientos legales· Asesoramiento en reducción de procesos Operativos. · Digitalización e indexación de documentos ·Emisión y expedición de pólizas · Logística de productos sensibles al riesgo y al tiempo · Auditoria a procesos de ventas.",
            },
            {
                title: "GESTIÓN TELEFÓNICA",
                description: "Optimice los procesos, fidelice y eleve la satisfacción de sus clientes con un servicio único y excepcional como su negocio.",
                item: "· Encuestas. · Bienvenidas. · Fidelización y retención. · Renovación de pólizas. · Auditorias. · Actualización base de datos y Salarft. · Soporte técnico. · Soporte Help Desk. · Confirmación de citas. · Servicio al cliente. · Atención a peticiones, quejas, reclamos y solicitudes.",
            },
            {
                title: "COBRANZAS",
                description: "Mas de 12 años de experiencia ayudando a empresas a mejorar su flujo financiero en gestión de todo tipo de cobranza y recuperación de cartera. Ofrecemos soluciones optimas a sus clientes a través de diversas técnicas de negociación, siempre manteniendo los niveles de satisfacción requerido.",
                item: "· Cobranzas prejurídicas. ·Manejo de carteras preventivas. · Cobranzas administrativas.",
            },
        ];
        this.cardText2 = [
            {
                title: "SALES",
                description: "We are known as one of the outsourcing leaders of sales management. Our operation is focused on the full understanding of each customer needs and is aligned with the sales teams of our partners. This way, we get to fulfill strategic objectives and even exceed them.",
                item: "· Creation of sales strategies- Financial and Insurance products sales- Cross selling/ Up selling - Online payments (PCI compliant)- Leads generation",
            },
            {
                title: "BACKOFFICE AND AUDIT",
                description: "High complexity back office operations management is one of our strengths. We use technological tools in order to optimize different kinds of processes and improve productivity within an organization.",
                item: "· Typing- Legal responses management- Operational process design- Documents indexing and digitizing- Insurance policies management- Sales process audit",
            },
            {
                title: "CALL CENTER",
                description: "Our inbound and outbound call services contribute to increase loyalty and improve customer satisfaction.",
                item: "· Survey application- Policies renewal- Loyalty and retention programs- Database management- Helpdesk- Appointment management- Customer service- Claims and information management.",
            },
            {
                title: "COLLECTION",
                description: "Our 12 years’ experience allows us to efficiently manage and recover delinquent portfolios from early to advanced stages.  We offer payment solutions through different negotiation schemes and keeping customer satisfaction always as our main pillar.",
                item: "· Pre-legal collections- Delinquent portfolio management.  ",
            },
        ];
        this.cardTecno1 = [
            {
                img: "assets/img/omnicanalidad.svg",
                title: "OMNICANALIDAD",
                description: "Nuestra plataforma omnicanal única en Colombia, 100% customizada y que integra todos los canales de comunicación: email, chat, formularios, telefonía, redes sociales y WhatsApp. Gestionamos de manera rápida y sencilla todas las comunicaciones con sus clientes y recopilamos en una sola herramienta toda la trazabilidad de las diferentes interacciones de sus usuarios.",
                item: "· Experiencia de usuario acorde a la nueva industria 4.0.· Práctica de compra perfectamente fluida y divertida. · Flujograma de compra que permite la multicanalidad en el inicio y cierre de la misma.",
            },
            {
                img: "assets/img/desarrollo-de-software.svg",
                title: "DESARROLLO DE SOFTWARE",
                description: "Nuestros desarrollos cuentan con gran adaptabilidad al cambio en múltiples lenguajes, herramientas y plataformas. De esta manera suplimos los requerimientos que surgen día tras día protegiendo siempre su información.",
                item: "· CRM para la gestión de campañas · Formularios Inbound y Outbound. · Control y centralización de correos electrónicos · Flujos dinámicos · Sistema de cargue de datos · Renombramiento de grabaciones · Creación de aplicaciones Móviles· Realización de informes automáticos.",
            },
            {
                img: "assets/img/estrategia-digital.svg",
                title: "ESTRATEGIAS DIGITALES",
                description: "Desarrollamos servicios completos de marketing con un enfoque de 360 °, desde la conceptualización y la estrategia hasta la ejecución en la etapa final. Nuestros servicios incluyen diseño web, SEO, generación de leads y marketing en redes sociales.",
                item: "· Posicionamiento en buscadores. · Diseño y desarrollo de sitios web. · Gestión de reputación en línea. · App store optimization. · Marketing de contenidos. · Optimización de la tasa de conversión.",
            },
            {
                img: "assets/img/ciencia-de-datos.svg",
                title: "CIENCIA DE DATOS",
                description: "Contamos con soluciones de analítica avanzada de datos, centrada en herramientas de modelamiento predictivo, machine learning e inteligencia artificial. Apoyamos la estrategia de cada una de las campañas en tiempo real.  Contamos con un equipo de científicos de datos de primer nivel que garantizan la flexibilidad, escalabilidad y efectividad de nuestras soluciones.",
                item: "· Metodologías elaboradas para el análisis estadístico. · Modelamiento de datos con machine learning. · Marketing analytics. · Business analytics. · Speech to Text · Smart Stock.",
            },
        ];
        this.cardTecno2 = [
            {
                img: "assets/img/omnicanalidad.svg",
                title: "OMNICHANEL",
                description: "Our unique omnichannel platform has been developed in-house and integrates all communication channels such as e-mail, chat, telephone, social networks and WhatsApp. We are able to efficiently handle all communications with the end users by consolidating the entire information in one tool.",
                item: "· Modern customer journey- Fluent and efficient buying experience",
            },
            {
                img: "assets/img/desarrollo-de-software.svg",
                title: "SOFTWARE DEVELOPMENT",
                description: "We develop multilanguage customized platforms to exchange information technologies adapted to each customer requirements. We are flexible to changes according to market requirements.",
                item: "· CRM- Inbound and outbound forms- Control and management of inbox- Automatic data uploading- Mobile apps- Automatic reporting.",
            },
            {
                img: "assets/img/estrategia-digital.svg",
                title: "DIGITAL STRATEGIES",
                description: "We offer end to end marketing services including web design, SEO, leads generation and social media management. ",
                item: "· Positioning- Design- Reputation management- App store optimization- Marketing content- Conversion rate improvement.",
            },
            {
                img: "assets/img/ciencia-de-datos.svg",
                title: "DATA SCIENCE",
                description: "Advanced data analysis solutions focused on predictive modeling tools, machine learning and artificial intelligence. We support companies’ campaigns in real time by analyzing the business behavior and market trends.",
                item: "· Statistic analysis through efficient methods- Data modeling with machine learning- Marketing and business analytics- Speech to text- Smart stock.",
            },
        ];
        this.bandera = 'es';
        // this.activeLang = localStorage.getItem("idioma");
        if (this.activeLang == "\"en\"") {
            this.bandera = 'en';
        }
        else if (this.activeLang == "\"es\"") {
            this.bandera = 'es';
        }
        this.translate.setDefaultLang(this.bandera);
        // Formulario
        const nombre = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4),
        ]);
        const telefono = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4),
        ]);
        const correo = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4),
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email,
        ]);
        const asunto = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(5),
        ]);
        const mensaje = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4),
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(160),
        ]);
        const terminos = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        this.contact = this.builder.group({
            nombre: nombre,
            telefono: telefono,
            correo: correo,
            asunto: asunto,
            mensaje: mensaje,
            terminos: terminos,
        });
    }
    ngOnInit() {
        aos__WEBPACK_IMPORTED_MODULE_2__["init"]({
            offset: 120,
            delay: 500,
            duration: 400,
            easing: "ease",
        });
        // Para ñadir el título de la página
        this.title.setTitle("inicio - Colombian Outsourcing Solution - COS");
        // Añadir el tag de la info de la página
        this.meta.addTag({
            name: "page.info",
            content: "home",
        });
        this.sharedService.Message.subscribe(m => { this.message = m; });
        this.message = 'new message';
        this.sharedService.nextMessage(this.message);
        if (this.router.url == '/') {
            this.activeLang = 'es';
            this.translate.use(this.activeLang);
            localStorage.setItem('idioma', JSON.stringify(`${this.activeLang}`));
        }
        else if (this.router.url == '/en') {
            this.activeLang = 'en';
            this.translate.use(this.activeLang);
            this.translate.currentLang == 'en';
            localStorage.setItem('idioma', JSON.stringify(`${this.activeLang}`));
        }
        this.banderaSuscription = this.sharedService.idioma$.subscribe(resp => {
            this.bandera = resp;
        });
    }
    cambiarLenguaje(lang) {
        this.activeLang = lang;
        this.translate.use(lang);
        localStorage.setItem('idioma', JSON.stringify(this.activeLang));
        const idio = localStorage.getItem("idioma");
    }
    ngOnDestroy() {
        // Quitar el tag "page.info" antes de ir a otra página, para que rastree correctamente la información
        this.meta.removeTag("name='page.info'");
        this.banderaSuscription.unsubscribe();
    }
    // event counter
    onWindowScroll(e) {
        let a = 0;
        let b = 0;
        let c = 0;
        let d = 0;
        let intervalo;
        let counter = document.getElementById("counter").offsetTop;
        // console.log('aqui estoy',window.pageYOffset)
        if (window.pageYOffset > 340) {
            this.intervalo = setInterval(() => {
                a = a + 50;
                b = b + 3;
                c = c + 1;
                d = d + 1;
                if (a <= 6400) {
                    this.teleT = a;
                    // console.log('conta', this.teleT);
                }
                if (b <= 74) {
                    this.clien = b;
                    // console.log('conta', this.teleT);
                }
                if (c <= 11) {
                    this.sedes = c;
                    // console.log('conta', this.teleT);
                }
                if (d <= 3) {
                    this.inter = d;
                    // console.log('conta', this.teleT);
                }
            }, 0.5);
        }
        else if (window.pageYOffset <= 340) {
            this.teleT = null;
            this.intervalo = null;
            clearInterval(this.intervalo);
        }
    }
    enviar() {
        let data = {
            nombre: this.contact.value.nombre,
            telefono: this.contact.value.telefono,
            email: this.contact.value.correo,
            asunto: this.contact.value.asunto,
            mensaje: this.contact.value.mensaje,
            cbox2: "on",
            enviar: "insert",
        };
        this.conService.senMail(data).subscribe((respuesta) => {
            console.log(respuesta);
        });
        this.contact.reset();
    }
    // validacion para cambios de estado de inputs
    get nombre() {
        return this.contact.get("nombre"); // validacion nombre
    }
    get nombreValid() {
        return this.nombre.touched && this.nombre.valid;
    }
    get nombreInvalid() {
        return this.nombre.touched && this.nombre.invalid;
    }
    get telefono() {
        return this.contact.get("telefono"); // validacion telefono
    }
    get telefonoValid() {
        return this.telefono.touched && this.telefono.valid;
    }
    get telefonoInvalid() {
        return this.telefono.touched && this.telefono.invalid;
    }
    get correo() {
        return this.contact.get("correo"); // validacion correo
    }
    get correoValid() {
        return this.correo.touched && this.correo.valid;
    }
    get correoInvalid() {
        return this.correo.touched && this.correo.invalid;
    }
    get asunto() {
        return this.contact.get("asunto"); // validacion asunto
    }
    get asuntoValid() {
        return this.asunto.touched && this.asunto.valid;
    }
    get asuntoInvalid() {
        return this.asunto.touched && this.asunto.invalid;
    }
    get mensaje() {
        return this.contact.get("mensaje"); // validacion usuario
    }
    get mensajeValid() {
        return this.mensaje.touched && this.mensaje.valid;
    }
    get mensajeInvalid() {
        return this.mensaje.touched && this.mensaje.invalid;
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Meta"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_servicios_contacto_contacto_service__WEBPACK_IMPORTED_MODULE_4__["ContactoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_7__["LanguageService"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], hostBindings: function HomeComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("scroll", function HomeComponent_scroll_HostBindingHandler($event) { return ctx.onWindowScroll($event); }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveWindow"]);
    } }, decls: 93, vars: 114, consts: [["id", "landing", 1, "bg"], ["src", "../../../assets/img/img.atf.jpg", "alt", "contact-center-BPO-COS"], [1, "contenido"], [1, "text"], [1, "mt-5", "follow"], ["href", "https://www.facebook.com/Outsourcingcos/", "target", "blank"], [1, "fab", "fa-facebook-square", "ml-5"], ["href", "https://www.instagram.com/cosoutsourcing/", "target", "blank"], [1, "fab", "fa-instagram-square", "ml-3"], ["href", "https://www.linkedin.com/company/colombianoutsourcingcos/about/", "target", "blank"], [1, "fab", "fa-linkedin", "ml-3"], ["href", "https://www.youtube.com/channel/UCL89AlspoSj_exe6qidqHew", "target", "blank"], [1, "fab", "fa-youtube", "ml-3"], ["method", "POST", 1, "contacto", 3, "formGroup", "ngSubmit"], ["type", "text", "formControlName", "nombre", 1, "form-control", 3, "placeholder", "hidden"], ["type", "text", "formControlName", "telefono", 1, "form-control", 3, "placeholder", "hidden"], ["type", "email", "formControlName", "correo", 1, "form-control", 3, "placeholder", "hidden"], ["type", "text", "formControlName", "asunto", 1, "form-control", 3, "placeholder", "hidden"], ["name", "", "id", "mensaje", "cols", "30", "rows", "5", "formControlName", "mensaje", 1, "form-control", 3, "placeholder", "hidden"], ["type", "checkbox", "name", "acepto termino", "id", "terminos", "formControlName", "terminos"], [3, "routerLink"], ["data-sitekey", "6Lf1QcAUAAAAACu8fCwgAcxDs7RB0WRKM2pqd9JO", 1, "g-recaptcha"], ["type", "submit", 1, "btn-send", 3, "disabled"], ["id", "counter", "data-aos", "fade-right", 1, "kpis", "container"], ["class", "numbers", 3, "hidden", 4, "ngFor", "ngForOf"], ["id", "BPO"], [1, "bpo-title"], [1, "bg-img-sticky", 3, "hidden"], [1, "cards", "container-fluid"], ["class", "card", "data-aos", "fade-right", 4, "ngFor", "ngForOf"], ["id", "tecno"], [1, "tecno-title"], [1, "tecno-cards", 3, "hidden"], ["id", "video", "data-aos", "fade-right", 1, "container", 3, "hidden"], ["width", "0", "data", "https://www.youtube.com/embed/nMvZY9f4mx4"], ["id", "video", "data-aos", "fade-right", 1, "container"], ["width", "0", "data", "https://www.youtube.com/embed/HhwLm4uYjfg", 3, "hidden"], [1, "col-12"], ["ngbSlide", ""], [1, "numbers", 3, "hidden"], [1, "circle"], [1, "fas"], ["class", "tilte", 4, "ngIf"], [4, "ngIf"], [1, "tilte"], ["data-aos", "fade-right", 1, "card"], [1, "card-title"], [1, "card-description"], [1, "card-item"], ["alt", "COS-tecnologia", 3, "src"], [1, "picsum-img-wrapper"], ["src", "assets/img/foto1.png", 3, "hidden"], ["src", "assets/img/foto11.png", 3, "hidden"], ["src", "assets/img/foto2.png", 3, "hidden"], ["src", "assets/img/foto22.png", 3, "hidden"], ["src", "assets/img/foto3.png", 3, "hidden"], ["src", "assets/img/foto33.png", 3, "hidden"], ["src", "assets/img/foto4.png", 3, "hidden"], ["src", "assets/img/foto44.png", 3, "hidden"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](6, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](9, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](12, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](14, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](18, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "i", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "form", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function HomeComponent_Template_form_ngSubmit_27_listener() { return ctx.enviar(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](30, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "input", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "input", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "input", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "input", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "input", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "input", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "textarea", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "textarea", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "input", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](46, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](49, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "button", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](53, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "section", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](55, HomeComponent_div_55_Template, 10, 8, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](56, HomeComponent_div_56_Template, 10, 8, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "section", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](61, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](64, HomeComponent_div_64_Template, 7, 3, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](67, HomeComponent_div_67_Template, 7, 3, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "section", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](72, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](75, HomeComponent_div_75_Template, 8, 4, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](78, HomeComponent_div_78_Template, 8, 4, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "section", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](80, "object", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "section", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](82, "object", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](87, "translate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "ngb-carousel");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](89, HomeComponent_ng_template_89_Template, 3, 2, "ng-template", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](90, HomeComponent_ng_template_90_Template, 3, 2, "ng-template", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](91, HomeComponent_ng_template_91_Template, 3, 2, "ng-template", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](92, HomeComponent_ng_template_92_Template, 3, 2, "ng-template", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](6, 89, "home.title1"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](9, 91, "home.parrafo1.1"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](12, 93, "home.parrafo1.2"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](14, 95, "home.parrafo1.3"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](18, 97, "home.subtitle1"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.contact);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](30, 99, "home.formtitle"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.nombreValid)("is-invalid", ctx.nombreInvalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx.nombreInvalid ? "El Nombre es requerido *" : "Nombre *")("hidden", ctx.bandera == "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.nombreValid)("is-invalid", ctx.nombreInvalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx.nombreInvalid ? "Name is required *" : "Name *")("hidden", ctx.bandera != "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.telefonoValid)("is-invalid", ctx.telefonoInvalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx.telefonoInvalid ? "El Tel\u00E9fono es requerido *" : "N\u00FAmero de tel\u00E9fono *")("hidden", ctx.bandera == "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.telefonoValid)("is-invalid", ctx.telefonoInvalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx.telefonoInvalid ? "Phone number is required *" : "Phone number *")("hidden", ctx.bandera != "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.correoValid)("is-invalid", ctx.correoInvalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx.correoInvalid ? "El Correo es requerido *" : "Correo Electr\u00F3nico *")("hidden", ctx.bandera == "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.correoValid)("is-invalid", ctx.correoInvalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx.correoInvalid ? "E-mail is required *" : "E-mail *")("hidden", ctx.bandera != "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.asuntoValid)("is-invalid", ctx.asuntoInvalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx.asuntoInvalid ? "El Asunto es requerido *" : "Asunto *")("hidden", ctx.bandera == "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.asuntoValid)("is-invalid", ctx.asuntoInvalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx.asuntoInvalid ? "Subject is required *" : "Subject *")("hidden", ctx.bandera != "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.mensajeValid)("is-invalid", ctx.mensajeInvalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx.mensajeInvalid ? "El Mensaje es requerido *" : "Mensaje *")("hidden", ctx.bandera == "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.mensajeValid)("is-invalid", ctx.mensajeInvalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx.mensajeInvalid ? "Message is required *" : "Message *")("hidden", ctx.bandera != "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](46, 101, "home.accept"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](113, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" \u00A0", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](49, 103, "home.terms"), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("btn-sendInvalid", ctx.contact.invalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx.contact.invalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](53, 105, "home.send"), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.kpiSection1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.kpiSection2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](61, 107, "home.services"), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx.bandera == "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.cardText1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx.bandera != "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.cardText2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](72, 109, "home.services2"), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx.bandera == "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.cardTecno1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx.bandera != "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.cardTecno2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx.bandera == "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx.bandera != "en");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](87, 111, "home.news"), " ");
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["CheckboxControlValueAccessor"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["NgbCarousel"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["NgbSlide"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"]], pipes: [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslatePipe"]], styles: ["*[_ngcontent-%COMP%] {\n  font-family: \"open-sans\", sans-serif;\n}\n\ni[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Free\";\n}\n\n.is-valid[_ngcontent-%COMP%] {\n  background: rgba(255, 255, 255, 0.2);\n  margin-bottom: 1rem;\n  width: 30rem;\n  padding-left: 2rem;\n  color: #fff;\n  border: 1px solid green !important;\n}\n\n.is-valid[_ngcontent-%COMP%]:hover {\n  background: rgba(255, 255, 255, 0.5);\n}\n\n.is-invalid[_ngcontent-%COMP%] {\n  background: rgba(192, 26, 36, 0.2) !important;\n  margin-bottom: 1rem;\n  width: 30rem;\n  padding-left: 2rem;\n  color: #fff;\n  border: 1px solid #C01A24 !important;\n}\n\n.is-invalid[_ngcontent-%COMP%]:hover {\n  background: rgba(255, 255, 255, 0.5);\n}\n\n.bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  position: absolute;\n  width: 100%;\n  height: auto;\n  -webkit-animation: size-bg 1.5s ease-in;\n  animation: size-bg 1.5s ease-in;\n}\n\n@-webkit-keyframes size-bg {\n  from {\n    width: 110%;\n  }\n  to {\n    background-size: 100%;\n  }\n}\n\n@keyframes size-bg {\n  from {\n    background-size: 110%;\n  }\n  to {\n    background-size: 100%;\n  }\n}\n\n.contenido[_ngcontent-%COMP%] {\n  position: relative;\n  display: grid;\n  grid-template-columns: 50% 50%;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n  grid-column: 1;\n  padding: 15rem 10rem;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  font-weight: 800;\n  color: #fff;\n  font-size: 4rem;\n  width: 500px;\n  margin-bottom: 2rem;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: white;\n  width: 445px;\n  margin-bottom: 2rem;\n  font-size: 1.2rem;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: white;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  font-size: 1.3rem;\n}\n\n.contacto[_ngcontent-%COMP%] {\n  grid-column: 2;\n  margin-bottom: 0;\n  padding: 15rem 0 0 5rem;\n}\n\n.contacto[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  font-weight: bold;\n  color: #fff;\n  margin-bottom: 2rem;\n}\n\n.contacto[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .contacto[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  background: rgba(255, 255, 255, 0.2);\n  margin-bottom: 1rem;\n  width: 30rem;\n  padding-left: 2rem;\n  color: #fff;\n  border: 1px solid #fff;\n}\n\n.contacto[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]:hover, .contacto[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%]:hover {\n  background: rgba(255, 255, 255, 0.5);\n}\n\n.contacto[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  color: white;\n  display: flex;\n  align-items: baseline;\n}\n\n.contacto[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .contacto[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  width: 1rem;\n  margin: 0 0.5rem 0 0;\n  cursor: pointer;\n}\n\n.contacto[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: white;\n}\n\n.contacto[_ngcontent-%COMP%]   .btn-send[_ngcontent-%COMP%], .contacto[_ngcontent-%COMP%]   .btn-sendInvalid[_ngcontent-%COMP%] {\n  width: 30rem;\n  background: white;\n  color: #A40505;\n  font-weight: bold;\n  height: 3rem;\n  border: none;\n  border-radius: 0.2rem;\n  margin-top: 1rem;\n}\n\n.contacto[_ngcontent-%COMP%]   .btn-send[_ngcontent-%COMP%]:hover, .contacto[_ngcontent-%COMP%]   .btn-sendInvalid[_ngcontent-%COMP%]:hover {\n  -webkit-animation: colorBg 0.2s ease-in;\n  animation: colorBg 0.2s ease-in;\n  background: #A40505;\n  color: white;\n}\n\n@-webkit-keyframes colorBg {\n  from {\n    background: #fff;\n  }\n  to {\n    background: #A40505;\n  }\n}\n\n@keyframes colorBg {\n  from {\n    background: #fff;\n  }\n  to {\n    background: #A40505;\n  }\n}\n\n.contacto[_ngcontent-%COMP%]   .btn-sendInvalid[_ngcontent-%COMP%] {\n  color: #9d9d9d;\n}\n\n.contacto[_ngcontent-%COMP%]   .btn-sendInvalid[_ngcontent-%COMP%]:hover {\n  background: #fff;\n  -webkit-animation: none;\n          animation: none;\n  color: #9d9d9d;\n}\n\n.disabledButton[_ngcontent-%COMP%], .disabledButton[_ngcontent-%COMP%]:hover {\n  width: 100%;\n  margin-top: 4rem;\n}\n\n.disabledButton[_ngcontent-%COMP%]:hover {\n  box-shadow: none;\n}\n\n[_ngcontent-%COMP%]::-moz-placeholder {\n  color: white;\n}\n\n[_ngcontent-%COMP%]::-ms-input-placeholder {\n  color: white;\n}\n\n[_ngcontent-%COMP%]::placeholder {\n  color: white;\n}\n\n@media (max-width: 414px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1804px !important;\n  }\n}\n\n@media (max-width: 425px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    position: absolute;\n    width: 1814px;\n    left: -1389px;\n    height: auto;\n    -webkit-animation: size-bg 1.5s ease-in;\n    animation: size-bg 1.5s ease-in;\n  }\n\n  .contenido[_ngcontent-%COMP%] {\n    display: grid;\n    -ms-grid-columns: none;\n    grid-template-rows: 30% 70%;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n    grid-column: 1;\n    padding: 7rem 2rem 0;\n    grid-row: 1;\n    text-align: center;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    width: 364px;\n    font-size: 2.5rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 364px;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n    font-size: 1rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]:hover {\n    font-size: 0.9rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    font-size: 1.5rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   .fa-facebook-square[_ngcontent-%COMP%] {\n    margin-left: 0.5rem !important;\n  }\n\n  .contenido[_ngcontent-%COMP%]    > *[_ngcontent-%COMP%]:nth-child(1) {\n    -ms-grid-row: 1;\n    -ms-grid-column: 1;\n  }\n\n  .contenido[_ngcontent-%COMP%]    > *[_ngcontent-%COMP%]:nth-child(2) {\n    -ms-grid-row: 2;\n    -ms-grid-column: 1;\n  }\n\n  .contacto[_ngcontent-%COMP%] {\n    grid-column: 1;\n    grid-row: 2;\n    padding: 5rem 2rem;\n    text-align: center;\n  }\n  .contacto[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n    width: 364px;\n    margin-top: 2rem;\n  }\n  .contacto[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .contacto[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n    width: 22rem;\n  }\n  .contacto[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n    width: 364px;\n  }\n  .contacto[_ngcontent-%COMP%]   .btn-send[_ngcontent-%COMP%], .contacto[_ngcontent-%COMP%]   .btn-sendInvalid[_ngcontent-%COMP%] {\n    width: 22rem;\n    margin-top: 2rem;\n  }\n  .contacto[_ngcontent-%COMP%]   .btn-sendInvalid[_ngcontent-%COMP%] {\n    width: 22rem;\n    margin-top: 2rem;\n    color: #9d9d9d;\n  }\n  .contacto[_ngcontent-%COMP%]   .btn-sendInvalid[_ngcontent-%COMP%]:hover {\n    background: #fff;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    position: absolute;\n    width: 1680px;\n    left: -912px;\n    height: auto;\n    -webkit-animation: size-bg 1.5s ease-in;\n    animation: size-bg 1.5s ease-in;\n  }\n\n  .contenido[_ngcontent-%COMP%] {\n    display: grid;\n    grid-template-columns: none;\n    grid-template-rows: 30% 70%;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n    grid-column: 1;\n    padding: 7rem 2rem 0;\n    grid-row: 1;\n    text-align: center;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    width: 704px;\n    font-size: 2.5rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 704px;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n    font-size: 1rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]:hover {\n    font-size: 0.9rem;\n  }\n\n  .contenido[_ngcontent-%COMP%]    > *[_ngcontent-%COMP%]:nth-child(1) {\n    -ms-grid-row: 1;\n    -ms-grid-column: 1;\n  }\n\n  .contenido[_ngcontent-%COMP%]    > *[_ngcontent-%COMP%]:nth-child(2) {\n    -ms-grid-row: 2;\n    -ms-grid-column: 1;\n  }\n\n  .contacto[_ngcontent-%COMP%] {\n    grid-column: 1;\n    grid-row: 2;\n    padding: 5rem 4rem;\n    text-align: center;\n  }\n  .contacto[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .contacto[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n    width: 40rem;\n  }\n  .contacto[_ngcontent-%COMP%]   .btn-send[_ngcontent-%COMP%], .contacto[_ngcontent-%COMP%]   .btn-sendInvalid[_ngcontent-%COMP%] {\n    width: 40rem;\n  }\n  .contacto[_ngcontent-%COMP%]   .btn-sendInvalid[_ngcontent-%COMP%] {\n    width: 40rem;\n    color: #9d9d9d;\n  }\n  .contacto[_ngcontent-%COMP%]   .btn-sendInvalid[_ngcontent-%COMP%]:hover {\n    background: #fff;\n  }\n}\n\n@media (min-width: 800px) and (max-width: 1024px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    position: absolute;\n    width: 1250px;\n    left: -226px;\n    height: auto;\n    -webkit-animation: size-bg 1.5s ease-in;\n    animation: size-bg 1.5s ease-in;\n  }\n\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n    padding: 15rem 4rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    width: 408px;\n    font-size: 3rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 408px;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n    font-size: 1rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]:hover {\n    font-size: 0.9rem;\n  }\n\n  .contacto[_ngcontent-%COMP%] {\n    padding: 8rem 0 0 0;\n  }\n}\n\n.kpis[_ngcontent-%COMP%] {\n  padding-top: 10rem;\n  display: flex;\n  -webkit-animation: fadeIn 1s ease-in;\n  animation: fadeIn 1s ease-in;\n}\n\n.kpis[_ngcontent-%COMP%]   .numbers[_ngcontent-%COMP%] {\n  display: grid;\n  justify-items: center;\n  text-align: center;\n  width: 25%;\n}\n\n.kpis[_ngcontent-%COMP%]   .numbers[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  margin: 1rem 0 0 0;\n  font-weight: bold !important;\n  font-size: 4rem;\n  color: #353535;\n}\n\n.kpis[_ngcontent-%COMP%]   .numbers[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: #353535;\n}\n\n.kpis[_ngcontent-%COMP%]   .circle[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 10rem;\n  height: 10rem;\n  border-radius: 50rem;\n  background: #C01A24;\n}\n\n.kpis[_ngcontent-%COMP%]   .circle[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 4rem;\n  color: white;\n}\n\n@media (max-width: 425px) {\n  .kpis[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n    padding-top: 0;\n    justify-items: center;\n  }\n\n  .numbers[_ngcontent-%COMP%] {\n    width: 100% !important;\n    justify-content: center;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .kpis[_ngcontent-%COMP%] {\n    display: flex;\n    flex-wrap: wrap;\n  }\n\n  .numbers[_ngcontent-%COMP%] {\n    justify-items: center;\n    width: 50% !important;\n  }\n\n  .circle[_ngcontent-%COMP%] {\n    width: 6rem;\n    height: 6rem;\n  }\n  .circle[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n    font-size: 2rem;\n  }\n}\n\n@media (max-width: 1024px) {\n  .kpis[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n    padding-top: 0;\n    justify-items: center;\n  }\n}\n\n.bpo-title[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%], .tecno-title[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  text-align: center;\n  color: #353535;\n  font-weight: bold;\n  margin: 9.5rem 0 3.5rem 0;\n}\n\n.bg-img-sticky[_ngcontent-%COMP%] {\n  background: url('BPO-img.jpg') no-repeat;\n  background-attachment: fixed, scroll;\n  height: 30rem;\n}\n\n.bg-img-sticky[_ngcontent-%COMP%]   .cards[_ngcontent-%COMP%] {\n  display: flex;\n  padding: 0 6rem;\n}\n\n.bg-img-sticky[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n  width: 30%;\n  padding: 5rem 3rem;\n  background: none;\n  border: none;\n  text-align: center;\n  color: white;\n}\n\n.bg-img-sticky[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-title[_ngcontent-%COMP%] {\n  font-weight: bold;\n  font-size: 1.5rem;\n}\n\n.bg-img-sticky[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-description[_ngcontent-%COMP%], .bg-img-sticky[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-item[_ngcontent-%COMP%] {\n  font-size: 0.8rem;\n}\n\n.bg-img-sticky[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-item[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n\n@media (max-width: 425px) {\n  .bg-img-sticky[_ngcontent-%COMP%] {\n    height: 70rem;\n    background-position: 40%;\n  }\n  .bg-img-sticky[_ngcontent-%COMP%]   .cards[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n    padding: 0 1rem;\n  }\n  .bg-img-sticky[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n    width: 100%;\n    padding: 3rem 1rem 0;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .bg-img-sticky[_ngcontent-%COMP%] {\n    height: 45rem;\n    background-position: 40%;\n  }\n  .bg-img-sticky[_ngcontent-%COMP%]   .cards[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n  }\n  .bg-img-sticky[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n    width: 50%;\n    padding: 3rem 1rem 0;\n  }\n}\n\n@media (min-width: 800px) and (max-width: 1024px) {\n  .bg-img-sticky[_ngcontent-%COMP%] {\n    height: 45rem;\n  }\n  .bg-img-sticky[_ngcontent-%COMP%]   .cards[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n  }\n  .bg-img-sticky[_ngcontent-%COMP%]   .cards[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n    width: 50%;\n    padding: 5rem 3rem 0;\n  }\n}\n\n@media only screen and (max-width: 1440px) {\n  .card-description[_ngcontent-%COMP%], .tecno-cards[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-item[_ngcontent-%COMP%], .bg-img-sticky[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-item[_ngcontent-%COMP%] {\n    font-size: 0.7rem;\n  }\n}\n\n#tecno[_ngcontent-%COMP%] {\n  -webkit-animation: fadeIn 0.5s ease-in;\n          animation: fadeIn 0.5s ease-in;\n}\n\n.tecno-cards[_ngcontent-%COMP%] {\n  -webkit-animation: fadeIn 1s ease-in;\n  animation: fadeIn 1s ease-in;\n}\n\n.tecno-cards[_ngcontent-%COMP%]   .cards[_ngcontent-%COMP%] {\n  display: flex;\n  padding: 0 6rem;\n}\n\n.tecno-cards[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n  width: 30%;\n  padding: 3rem;\n  background: none;\n  border: none;\n  text-align: center;\n  color: #353535;\n}\n\n.tecno-cards[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  margin-bottom: 3rem;\n}\n\n.tecno-cards[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-title[_ngcontent-%COMP%] {\n  font-weight: bold;\n  font-size: 1.5rem;\n}\n\n.tecno-cards[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-description[_ngcontent-%COMP%], .tecno-cards[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-item[_ngcontent-%COMP%] {\n  font-size: 0.8rem;\n}\n\n.tecno-cards[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-item[_ngcontent-%COMP%] {\n  font-weight: 700;\n}\n\n@media (max-width: 425px) {\n  .tecno-cards[_ngcontent-%COMP%]   .cards[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n    padding: 0 1rem;\n  }\n  .tecno-cards[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n    width: 100%;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .tecno-cards[_ngcontent-%COMP%]   .cards[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n    padding: 0 1rem;\n  }\n  .tecno-cards[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n}\n\n@media (min-width: 920px) and (max-width: 1024px) {\n  .tecno-cards[_ngcontent-%COMP%]   .cards[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n  }\n  .tecno-cards[_ngcontent-%COMP%]   .cards[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n}\n\nobject[_ngcontent-%COMP%] {\n  width: 100% !important;\n  max-width: 1000px !important;\n  height: 500px;\n}\n\n@-webkit-keyframes fadeIn {\n  from {\n    opacity: 0;\n    transform: translate(10%);\n  }\n  to {\n    opacity: 1;\n    transform: translate(0%);\n  }\n}\n\n@keyframes fadeIn {\n  from {\n    opacity: 0;\n    transform: translate(10%);\n  }\n  to {\n    opacity: 1;\n    transform: translate(0%);\n  }\n}\n\nngb-carousel[_ngcontent-%COMP%] {\n  max-width: 70%;\n  margin: 50px auto;\n}\n\nngb-carousel[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  outline: none;\n}\n\nngb-carousel[_ngcontent-%COMP%] {\n  width: inherit;\n  height: inherit;\n}\n\n.carousel-inner[_ngcontent-%COMP%] {\n  overflow: visible;\n}\n\n.carousel-item[_ngcontent-%COMP%] {\n  display: flex !important;\n  opacity: 0;\n  visibility: hidden;\n  transition: opacity 1.2s ease-in-out, visibility 1.2s;\n  z-index: -1;\n}\n\n.carousel-item.active[_ngcontent-%COMP%] {\n  opacity: 1;\n  visibility: visible;\n  z-index: 10;\n}\n\n.carousel-control-prev[_ngcontent-%COMP%] {\n  z-index: 20;\n}\n\n.carousel-control-next[_ngcontent-%COMP%] {\n  z-index: 20;\n}\n\n.carousel-indicators[_ngcontent-%COMP%] {\n  z-index: 20;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9EOlxccHJveWVjdG9zXFx3ZWJzaXRlLWNvcy9zcmNcXGFwcFxccGFnZXNcXGhvbWVcXGhvbWUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLG9DQUFBO0FDREo7O0FESUE7RUFDSSxrQ0FBQTtBQ0RKOztBREtBO0VBQ0ksb0NBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxrQ0FBQTtBQ0ZKOztBREdJO0VBQ0ksb0NBQUE7QUNEUjs7QURJQTtFQUNJLDZDQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0Esb0NBQUE7QUNESjs7QURFSTtFQUNJLG9DQUFBO0FDQVI7O0FES0k7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsdUNBQUE7RUFDQSwrQkFBQTtBQ0ZSOztBREtBO0VBQ0k7SUFBTyxXQUFBO0VDRFQ7RURFRTtJQUFLLHFCQUFBO0VDQ1A7QUFDRjs7QURBQTtFQUNJO0lBQU8scUJBQUE7RUNHVDtFREZFO0lBQUsscUJBQUE7RUNLUDtBQUNGOztBREhBO0VBQ0ksa0JBQUE7RUFFQSxhQUFBO0VBRUEsOEJBQUE7QUNLSjs7QURKSTtFQUVJLGNBQUE7RUFDQSxvQkFBQTtBQ01SOztBRExRO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ09aOztBRExRO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDT1o7O0FETFE7RUFDSSxpQkFBQTtBQ09aOztBRE5ZO0VBQ0ksb0NBQUE7QUNRaEI7O0FETlk7RUFDSSxZQUFBO0FDUWhCOztBRFBnQjtFQUNJLGlCQUFBO0FDU3BCOztBRERBO0VBRUksY0FBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7QUNJSjs7QURISTtFQUNJLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FDS1I7O0FESEk7RUFDSSxvQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0FDS1I7O0FESlE7RUFDSSxvQ0FBQTtBQ01aOztBREFJO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtBQ0VSOztBRERRO0VBQ0ksV0FBQTtFQUNBLG9CQUFBO0VBQ0EsZUFBQTtBQ0daOztBRERRO0VBQ0ksWUFBQTtBQ0daOztBREFJO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0FDRVI7O0FERFE7RUFDSSx1Q0FBQTtFQUNRLCtCQUFBO0VBQ1IsbUJBQUE7RUFDQSxZQUFBO0FDR1o7O0FERlk7RUFDSTtJQUFNLGdCQUFBO0VDS3BCO0VESmM7SUFBSSxtQkFBQTtFQ09sQjtBQUNGOztBRExZO0VBQ0k7SUFBTSxnQkFBQTtFQ1FwQjtFRFBjO0lBQUksbUJBQUE7RUNVbEI7QUFDRjs7QUROSTtFQUVJLGNBQUE7QUNPUjs7QUROUTtFQUNJLGdCQUFBO0VBQ0EsdUJBQUE7VUFBQSxlQUFBO0VBQ0EsY0FBQTtBQ1FaOztBREhBO0VBRUksV0FBQTtFQUNBLGdCQUFBO0FDS0o7O0FESkk7RUFFSSxnQkFBQTtBQ0tSOztBREVBO0VBQ0ksWUFBQTtBQ0tKOztBREFBO0VBQ0ksWUFBQTtBQ09KOztBRExBO0VBQ0ksWUFBQTtBQ1FKOztBRExBO0VBRVE7SUFDSSx3QkFBQTtFQ09WO0FBQ0Y7O0FESEE7RUFFUTtJQUNJLGtCQUFBO0lBQ0EsYUFBQTtJQUNBLGFBQUE7SUFDQSxZQUFBO0lBQ0EsdUNBQUE7SUFDQSwrQkFBQTtFQ0lWOztFRERFO0lBRUksYUFBQTtJQUNBLHNCQUFBO0lBRUEsMkJBQUE7RUNJTjtFREhNO0lBRUksY0FBQTtJQUNBLG9CQUFBO0lBRUEsV0FBQTtJQUNBLGtCQUFBO0VDS1Y7RURKVTtJQUNJLFlBQUE7SUFDQSxpQkFBQTtFQ01kO0VESlU7SUFDSSxlQUFBO0lBQ0EsWUFBQTtFQ01kO0VESlU7SUFDSSxlQUFBO0VDTWQ7RURMYztJQUNJLGlCQUFBO0VDT2xCO0VETGM7SUFDSSxpQkFBQTtFQ09sQjtFRE5rQjtJQUNJLDhCQUFBO0VDUXRCOztFREFFO0lBQ0ksZUFBQTtJQUNBLGtCQUFBO0VDR047O0VEREU7SUFDSSxlQUFBO0lBQ0Esa0JBQUE7RUNJTjs7RURGRTtJQUVJLGNBQUE7SUFFQSxXQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtFQ0tOO0VESk07SUFDSSxZQUFBO0lBQ0EsZ0JBQUE7RUNNVjtFREpNO0lBQ0ksWUFBQTtFQ01WO0VESk07SUFDSSxZQUFBO0VDTVY7RURKTTtJQUNJLFlBQUE7SUFDQSxnQkFBQTtFQ01WO0VESk07SUFDSSxZQUFBO0lBQ0EsZ0JBQUE7SUFDQSxjQUFBO0VDTVY7RURMVTtJQUNJLGdCQUFBO0VDT2Q7QUFDRjs7QURGQTtFQUlRO0lBQ0ksa0JBQUE7SUFDQSxhQUFBO0lBQ0EsWUFBQTtJQUNBLFlBQUE7SUFDQSx1Q0FBQTtJQUNBLCtCQUFBO0VDQ1Y7O0VERUU7SUFFSSxhQUFBO0lBRUEsMkJBQUE7SUFFQSwyQkFBQTtFQ0NOO0VEQU07SUFFSSxjQUFBO0lBQ0Esb0JBQUE7SUFFQSxXQUFBO0lBQ0Esa0JBQUE7RUNFVjtFRERVO0lBQ0ksWUFBQTtJQUNBLGlCQUFBO0VDR2Q7RUREVTtJQUNJLGVBQUE7SUFDQSxZQUFBO0VDR2Q7RUREVTtJQUNJLGVBQUE7RUNHZDtFREZjO0lBQ0ksaUJBQUE7RUNJbEI7O0VEQ0U7SUFDSSxlQUFBO0lBQ0Esa0JBQUE7RUNFTjs7RURBRTtJQUNJLGVBQUE7SUFDQSxrQkFBQTtFQ0dOOztFRERFO0lBRUksY0FBQTtJQUVBLFdBQUE7SUFDQSxrQkFBQTtJQUNBLGtCQUFBO0VDSU47RURITTtJQUNJLFlBQUE7RUNLVjtFREhNO0lBQ0ksWUFBQTtFQ0tWO0VESE07SUFDSSxZQUFBO0lBQ0EsY0FBQTtFQ0tWO0VESlU7SUFDSSxnQkFBQTtFQ01kO0FBQ0Y7O0FEREE7RUFJUTtJQUNJLGtCQUFBO0lBQ0EsYUFBQTtJQUNBLFlBQUE7SUFDQSxZQUFBO0lBQ0EsdUNBQUE7SUFDQSwrQkFBQTtFQ0FWOztFRElNO0lBQ0ksbUJBQUE7RUNEVjtFREVVO0lBQ0ksWUFBQTtJQUNBLGVBQUE7RUNBZDtFREVVO0lBQ0ksZUFBQTtJQUNBLFlBQUE7RUNBZDtFREVVO0lBQ0ksZUFBQTtFQ0FkO0VEQ2M7SUFDSSxpQkFBQTtFQ0NsQjs7RURJRTtJQUNJLG1CQUFBO0VDRE47QUFDRjs7QURLQTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLG9DQUFBO0VBQ1EsNEJBQUE7QUNIWjs7QURJSTtFQUVJLGFBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQ0ZSOztBREdRO0VBQ0ksa0JBQUE7RUFDQSw0QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDRFo7O0FER1E7RUFDSSxjQUFBO0FDRFo7O0FESUk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtBQ0ZSOztBREdRO0VBQ0ksZUFBQTtFQUNBLFlBQUE7QUNEWjs7QURLQTtFQUNJO0lBQ0ksZUFBQTtJQUNBLGNBQUE7SUFDQSxxQkFBQTtFQ0ZOOztFRElFO0lBQ0ksc0JBQUE7SUFDQSx1QkFBQTtFQ0ROO0FBQ0Y7O0FER0E7RUFDSTtJQUNJLGFBQUE7SUFDQSxlQUFBO0VDRE47O0VER0U7SUFDSSxxQkFBQTtJQUNBLHFCQUFBO0VDQU47O0VERUU7SUFDSSxXQUFBO0lBQ0EsWUFBQTtFQ0NOO0VEQU07SUFDSSxlQUFBO0VDRVY7QUFDRjs7QURFQTtFQUNJO0lBQ0ksZUFBQTtJQUNBLGNBQUE7SUFDQSxxQkFBQTtFQ0FOO0FBQ0Y7O0FES0k7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0FDSFI7O0FETUE7RUFDSSx3Q0FBQTtFQUNBLG9DQUFBO0VBQ0EsYUFBQTtBQ0hKOztBRElJO0VBQ0ksYUFBQTtFQUNBLGVBQUE7QUNGUjs7QURJSTtFQUNJLFVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ0ZSOztBREdRO0VBQ0ksaUJBQUE7RUFDQSxpQkFBQTtBQ0RaOztBREdRO0VBQ0ksaUJBQUE7QUNEWjs7QURHUTtFQUVJLGdCQUFBO0FDRlo7O0FEUUE7RUFDSTtJQUNJLGFBQUE7SUFDQSx3QkFBQTtFQ0xOO0VETU07SUFDSSxlQUFBO0lBQ0EsZUFBQTtFQ0pWO0VETU07SUFDSSxXQUFBO0lBQ0Esb0JBQUE7RUNKVjtBQUNGOztBRFFBO0VBQ0k7SUFDSSxhQUFBO0lBQ0Esd0JBQUE7RUNOTjtFRE9NO0lBQ0ksZUFBQTtFQ0xWO0VET007SUFDSSxVQUFBO0lBQ0Esb0JBQUE7RUNMVjtBQUNGOztBRFNBO0VBQ0k7SUFDSSxhQUFBO0VDUE47RURRTTtJQUNJLGVBQUE7RUNOVjtFRE9VO0lBQ0ksVUFBQTtJQUNBLG9CQUFBO0VDTGQ7QUFDRjs7QURVQTtFQUNJO0lBQ0ksaUJBQUE7RUNSTjtBQUNGOztBRFlBO0VBQ0ksc0NBQUE7VUFBQSw4QkFBQTtBQ1ZKOztBRGVDO0VBQ0csb0NBQUE7RUFDUSw0QkFBQTtBQ1paOztBRGFLO0VBQ0ksYUFBQTtFQUNBLGVBQUE7QUNYVDs7QURhSztFQUNHLFVBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FDWFI7O0FEWVE7RUFDSSxtQkFBQTtBQ1ZaOztBRFlRO0VBQ0ksaUJBQUE7RUFDQSxpQkFBQTtBQ1ZaOztBRFlRO0VBQ0ksaUJBQUE7QUNWWjs7QURZUTtFQUVJLGdCQUFBO0FDWFo7O0FEZUE7RUFFUTtJQUNJLGVBQUE7SUFDQSxlQUFBO0VDYlY7RURlTTtJQUNJLFdBQUE7RUNiVjtBQUNGOztBRGdCQztFQUVRO0lBQ0ksZUFBQTtJQUNBLGVBQUE7RUNmWDtFRGlCTztJQUNJLFVBQUE7RUNmWDtBQUNGOztBRGtCQTtFQUVRO0lBQ0ksZUFBQTtFQ2pCVjtFRGtCVTtJQUNJLFVBQUE7RUNoQmQ7QUFDRjs7QURzQkE7RUFDSSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0EsYUFBQTtBQ3BCSjs7QUR5QkE7RUFDSTtJQUFNLFVBQUE7SUFDRix5QkFBQTtFQ3JCTjtFRHNCRTtJQUFLLFVBQUE7SUFDRCx3QkFBQTtFQ25CTjtBQUNGOztBRGNBO0VBQ0k7SUFBTSxVQUFBO0lBQ0YseUJBQUE7RUNyQk47RURzQkU7SUFBSyxVQUFBO0lBQ0Qsd0JBQUE7RUNuQk47QUFDRjs7QURzQkE7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7QUNwQko7O0FEdUJBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7QUNwQko7O0FEdUJBO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUNwQko7O0FEdUJBO0VBQ0ksaUJBQUE7QUNwQko7O0FEdUJBO0VBQ0ksd0JBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxxREFBQTtFQUNBLFdBQUE7QUNwQko7O0FEdUJBO0VBQ0ksVUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtBQ3BCSjs7QUR1QkE7RUFDSyxXQUFBO0FDcEJMOztBRHdCQTtFQUNLLFdBQUE7QUNyQkw7O0FEd0JBO0VBQ0ksV0FBQTtBQ3JCSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4vLyBHZW5lcmFsc1xyXG4qe1xyXG4gICAgZm9udC1mYW1pbHk6ICdvcGVuLXNhbnMnLHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbml7XHJcbiAgICBmb250LWZhbWlseTogJ0ZvbnQgQXdlc29tZSA1IEZyZWUnO1xyXG59XHJcblxyXG4vLyBJbnB1bnQgVmFsaWQgQ2xhc3NcclxuLmlzLXZhbGlke1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgkY29sb3I6ICNmZmYsICRhbHBoYTogLjIpO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICAgIHdpZHRoOiAzMHJlbTtcclxuICAgIHBhZGRpbmctbGVmdDoycmVtO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCBncmVlbiAhaW1wb3J0YW50O1xyXG4gICAgJjpob3ZlcntcclxuICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKCRjb2xvcjogI2ZmZiwgJGFscGhhOiAuNSk7XHJcbiAgICB9XHJcbn1cclxuLmlzLWludmFsaWR7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKCRjb2xvcjogI0MwMUEyNCwgJGFscGhhOiAuMikgIWltcG9ydGFudDtcclxuICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICB3aWR0aDogMzByZW07XHJcbiAgICBwYWRkaW5nLWxlZnQ6MnJlbTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI0MwMUEyNCAhaW1wb3J0YW50O1xyXG4gICAgJjpob3ZlcntcclxuICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKCRjb2xvcjogI2ZmZiwgJGFscGhhOiAuNSk7XHJcbiAgICB9XHJcbn1cclxuLy8gU2VjdGlvbiAxICoqIENvbnRhY3QgbGFuZGluZ1xyXG4uYmd7XHJcbiAgICBpbWd7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAtd2Via2l0LWFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XHJcbiAgICAgICAgYW5pbWF0aW9uOiBzaXplLWJnIDEuNXMgZWFzZS1pbjtcclxuICAgIH1cclxufVxyXG5ALXdlYmtpdC1rZXlmcmFtZXMgc2l6ZS1iZ3tcclxuICAgIGZyb20geyB3aWR0aDogMTEwJTsgfVxyXG4gICAgdG8geyBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7fVxyXG59XHJcbkBrZXlmcmFtZXMgc2l6ZS1iZ3tcclxuICAgIGZyb20geyBiYWNrZ3JvdW5kLXNpemU6IDExMCU7IH1cclxuICAgIHRvIHsgYmFja2dyb3VuZC1zaXplOiAxMDAlO31cclxufVxyXG5cclxuLmNvbnRlbmlkb3tcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IC1tcy1ncmlkO1xyXG4gICAgZGlzcGxheTogZ3JpZDtcclxuICAgIC1tcy1ncmlkLWNvbHVtbnM6IDUwJSA1MCU7XHJcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDUwJSA1MCU7XHJcbiAgICAudGV4dHtcclxuICAgICAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XHJcbiAgICAgICAgZ3JpZC1jb2x1bW46IDE7XHJcbiAgICAgICAgcGFkZGluZzogMTVyZW0gMTByZW07XHJcbiAgICAgICAgaDF7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDRyZW07XHJcbiAgICAgICAgICAgIHdpZHRoOiA1MDBweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcHtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICB3aWR0aDogNDQ1cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDJyZW07XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuZm9sbG93e1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnRm9udCBBd2Vzb21lIDUgQnJhbmRzJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBhe1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgJjpob3ZlcntcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEuM3JlbTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG59XHJcbi5jb250YWN0b3tcclxuICAgIC1tcy1ncmlkLWNvbHVtbjogMjtcclxuICAgIGdyaWQtY29sdW1uOiAyO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgIHBhZGRpbmc6IDE1cmVtIDAgMCA1cmVtO1xyXG4gICAgaDN7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICAgIH1cclxuICAgIGlucHV0e1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHJnYmEoJGNvbG9yOiAjZmZmLCAkYWxwaGE6IC4yKTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgICAgIHdpZHRoOiAzMHJlbTtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6MnJlbTtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xyXG4gICAgICAgICY6aG92ZXJ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHJnYmEoJGNvbG9yOiAjZmZmLCAkYWxwaGE6IC41KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICB0ZXh0YXJlYXtcclxuICAgICAgICBAZXh0ZW5kIGlucHV0XHJcbiAgICB9XHJcbiAgICBsYWJlbHtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogYmFzZWxpbmU7XHJcbiAgICAgICAgaW5wdXR7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxcmVtO1xyXG4gICAgICAgICAgICBtYXJnaW46IDAgLjVyZW0gMCAwO1xyXG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGF7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuYnRuLXNlbmR7XHJcbiAgICAgICAgd2lkdGg6IDMwcmVtO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIGNvbG9yOiAjQTQwNTA1O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGhlaWdodDogM3JlbTtcclxuICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogLjJyZW07XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMXJlbTtcclxuICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICAtd2Via2l0LWFuaW1hdGlvbjogY29sb3JCZyAuMnMgZWFzZS1pbjtcclxuICAgICAgICAgICAgICAgICAgICBhbmltYXRpb246IGNvbG9yQmcgLjJzIGVhc2UtaW47XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6I0E0MDUwNTtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICBALXdlYmtpdC1rZXlmcmFtZXMgY29sb3JCZyB7XHJcbiAgICAgICAgICAgICAgICBmcm9tIHtiYWNrZ3JvdW5kOiAjZmZmO31cclxuICAgICAgICAgICAgICAgIHRvIHtiYWNrZ3JvdW5kOiAjQTQwNTA1O31cclxuXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgQGtleWZyYW1lcyBjb2xvckJnIHtcclxuICAgICAgICAgICAgICAgIGZyb20ge2JhY2tncm91bmQ6ICNmZmY7fVxyXG4gICAgICAgICAgICAgICAgdG8ge2JhY2tncm91bmQ6ICNBNDA1MDU7fVxyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5idG4tc2VuZEludmFsaWR7XHJcbiAgICAgICAgQGV4dGVuZCAuYnRuLXNlbmQ7XHJcbiAgICAgICAgY29sb3I6ICM5ZDlkOWQ7XHJcbiAgICAgICAgJjpob3ZlcntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICAgICAgYW5pbWF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICBjb2xvcjogIzlkOWQ5ZDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5kaXNhYmxlZEJ1dHRvbntcclxuXHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbi10b3A6IDRyZW07XHJcbiAgICAmOmhvdmVye1xyXG4gICAgICAgIEBleHRlbmQgLmRpc2FibGVkQnV0dG9uO1xyXG4gICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICB9XHJcbn1cclxuXHJcbjo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlcntcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG46Oi1tb3otcGxhY2Vob2xkZXJ7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuOi1tcy1pbnB1dC1wbGFjZWhvbGRlcntcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG46Oi1tcy1pbnB1dC1wbGFjZWhvbGRlcntcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG46OnBsYWNlaG9sZGVye1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogNDE0cHgpe1xyXG4gICAgLmJne1xyXG4gICAgICAgIGltZ3tcclxuICAgICAgICAgICAgd2lkdGg6IDE4MDRweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDQyNXB4KXtcclxuICAgIC5iZ3tcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgd2lkdGg6IDE4MTRweDtcclxuICAgICAgICAgICAgbGVmdDogLTEzODlweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICAtd2Via2l0LWFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNvbnRlbmlkb3tcclxuICAgICAgICBkaXNwbGF5OiAtbXMtZ3JpZDtcclxuICAgICAgICBkaXNwbGF5OiBncmlkO1xyXG4gICAgICAgIC1tcy1ncmlkLWNvbHVtbnM6IG5vbmU7XHJcbiAgICAgICAgLW1zLWdyaWQtcm93czogMzAlIDcwJTtcclxuICAgICAgICBncmlkLXRlbXBsYXRlLXJvd3M6IDMwJSA3MCU7XHJcbiAgICAgICAgLnRleHR7XHJcbiAgICAgICAgICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcclxuICAgICAgICAgICAgZ3JpZC1jb2x1bW46IDE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDdyZW0gMnJlbSAwO1xyXG4gICAgICAgICAgICAtbXMtZ3JpZC1yb3c6IDE7XHJcbiAgICAgICAgICAgIGdyaWQtcm93OiAxO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGgxe1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDM2NHB4O1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyLjVyZW07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAzNjRweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuZm9sbG93e1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgICAgICAgICAgICAgJjpob3ZlcntcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IC45cmVtO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYXtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICAgICAgICAgICAgICAgICAuZmEtZmFjZWJvb2stc3F1YXJle1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogLjVyZW0gIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jb250ZW5pZG8gPiAqOm50aC1jaGlsZCgxKXtcclxuICAgICAgICAtbXMtZ3JpZC1yb3c6IDE7XHJcbiAgICAgICAgLW1zLWdyaWQtY29sdW1uOiAxXHJcbiAgICB9XHJcbiAgICAuY29udGVuaWRvID4gKjpudGgtY2hpbGQoMil7XHJcbiAgICAgICAgLW1zLWdyaWQtcm93OiAyO1xyXG4gICAgICAgIC1tcy1ncmlkLWNvbHVtbjogMVxyXG4gICAgfVxyXG4gICAgLmNvbnRhY3Rve1xyXG4gICAgICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcclxuICAgICAgICBncmlkLWNvbHVtbjogMTtcclxuICAgICAgICAtbXMtZ3JpZC1yb3c6IDI7XHJcbiAgICAgICAgZ3JpZC1yb3c6IDI7XHJcbiAgICAgICAgcGFkZGluZzo1cmVtIDJyZW07XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGgze1xyXG4gICAgICAgICAgICB3aWR0aDogMzY0cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDJyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlucHV0e1xyXG4gICAgICAgICAgICB3aWR0aDogMjJyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICB3aWR0aDogMzY0cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5idG4tc2VuZHtcclxuICAgICAgICAgICAgd2lkdGg6IDIycmVtO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuYnRuLXNlbmRJbnZhbGlke1xyXG4gICAgICAgICAgICB3aWR0aDogMjJyZW07XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDJyZW07XHJcbiAgICAgICAgICAgIGNvbG9yOiAjOWQ5ZDlkO1xyXG4gICAgICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6NTYwcHgpIGFuZCAobWF4LXdpZHRoOiA3NjhweCl7XHJcblxyXG5cclxuICAgIC5iZ3tcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgd2lkdGg6IDE2ODBweDtcclxuICAgICAgICAgICAgbGVmdDogLTkxMnB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgIC13ZWJraXQtYW5pbWF0aW9uOiBzaXplLWJnIDEuNXMgZWFzZS1pbjtcclxuICAgICAgICAgICAgYW5pbWF0aW9uOiBzaXplLWJnIDEuNXMgZWFzZS1pbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY29udGVuaWRve1xyXG4gICAgICAgIGRpc3BsYXk6IC1tcy1ncmlkO1xyXG4gICAgICAgIGRpc3BsYXk6IGdyaWQ7XHJcbiAgICAgICAgLW1zLWdyaWQtY29sdW1uczogbm9uZTtcclxuICAgICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IG5vbmU7XHJcbiAgICAgICAgLW1zLWdyaWQtcm93czogMzAlIDcwJTtcclxuICAgICAgICBncmlkLXRlbXBsYXRlLXJvd3M6IDMwJSA3MCU7XHJcbiAgICAgICAgLnRleHR7XHJcbiAgICAgICAgICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcclxuICAgICAgICAgICAgZ3JpZC1jb2x1bW46IDE7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDdyZW0gMnJlbSAwO1xyXG4gICAgICAgICAgICAtbXMtZ3JpZC1yb3c6IDE7XHJcbiAgICAgICAgICAgIGdyaWQtcm93OiAxO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGgxe1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDcwNHB4O1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyLjVyZW07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA3MDRweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuZm9sbG93e1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgICAgICAgICAgICAgJjpob3ZlcntcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IC45cmVtO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNvbnRlbmlkbyA+ICo6bnRoLWNoaWxkKDEpe1xyXG4gICAgICAgIC1tcy1ncmlkLXJvdzogMTtcclxuICAgICAgICAtbXMtZ3JpZC1jb2x1bW46IDFcclxuICAgIH1cclxuICAgIC5jb250ZW5pZG8gPiAqOm50aC1jaGlsZCgyKXtcclxuICAgICAgICAtbXMtZ3JpZC1yb3c6IDI7XHJcbiAgICAgICAgLW1zLWdyaWQtY29sdW1uOiAxXHJcbiAgICB9XHJcbiAgICAuY29udGFjdG97XHJcbiAgICAgICAgLW1zLWdyaWQtY29sdW1uOiAxO1xyXG4gICAgICAgIGdyaWQtY29sdW1uOiAxO1xyXG4gICAgICAgIC1tcy1ncmlkLXJvdzogMjtcclxuICAgICAgICBncmlkLXJvdzogMjtcclxuICAgICAgICBwYWRkaW5nOjVyZW0gNHJlbTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgaW5wdXR7XHJcbiAgICAgICAgICAgIHdpZHRoOiA0MHJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmJ0bi1zZW5ke1xyXG4gICAgICAgICAgICB3aWR0aDogNDByZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5idG4tc2VuZEludmFsaWR7XHJcbiAgICAgICAgICAgIHdpZHRoOiA0MHJlbTtcclxuICAgICAgICAgICAgY29sb3I6ICM5ZDlkOWQ7XHJcbiAgICAgICAgICAgICY6aG92ZXJ7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDo4MDBweClhbmQobWF4LXdpZHRoOjEwMjRweCl7XHJcblxyXG5cclxuICAgIC5iZ3tcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgd2lkdGg6IDEyNTBweDtcclxuICAgICAgICAgICAgbGVmdDogLTIyNnB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgIC13ZWJraXQtYW5pbWF0aW9uOiBzaXplLWJnIDEuNXMgZWFzZS1pbjtcclxuICAgICAgICAgICAgYW5pbWF0aW9uOiBzaXplLWJnIDEuNXMgZWFzZS1pbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY29udGVuaWRve1xyXG4gICAgICAgIC50ZXh0e1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxNXJlbSA0cmVtO1xyXG4gICAgICAgICAgICBoMXtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA0MDhweDtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogM3JlbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBwe1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDQwOHB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5mb2xsb3d7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICAgICAgICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogLjlyZW07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY29udGFjdG97XHJcbiAgICAgICAgcGFkZGluZzogOHJlbSAwIDAgMDtcclxuICAgIH1cclxufVxyXG5cclxuLy8gU2VjdGlvbiAyICoqIGNvdW50ZXJcclxuLmtwaXN7XHJcbiAgICBwYWRkaW5nLXRvcDogMTByZW07XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1hbmltYXRpb246IGZhZGVJbiAxcyBlYXNlLWluO1xyXG4gICAgICAgICAgICBhbmltYXRpb246IGZhZGVJbiAxcyBlYXNlLWluO1xyXG4gICAgLm51bWJlcnN7XHJcbiAgICAgICAgZGlzcGxheTogLW1zLWdyaWQ7XHJcbiAgICAgICAgZGlzcGxheTogZ3JpZDtcclxuICAgICAgICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOiAyNSU7XHJcbiAgICAgICAgaDN7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMXJlbSAwIDAgMDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiA0cmVtO1xyXG4gICAgICAgICAgICBjb2xvcjogIzM1MzUzNTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcHtcclxuICAgICAgICAgICAgY29sb3I6ICMzNTM1MzU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNpcmNsZXtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6IDEwcmVtO1xyXG4gICAgICAgIGhlaWdodDogMTByZW07XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTByZW07XHJcbiAgICAgICAgYmFja2dyb3VuZDogI0MwMUEyNDtcclxuICAgICAgICBpe1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDRyZW07XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIChtYXgtd2lkdGg6IDQyNXB4KXtcclxuICAgIC5rcGlze1xyXG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMDtcclxuICAgICAgICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICAubnVtYmVyc3tcclxuICAgICAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgfVxyXG59XHJcbkBtZWRpYSAobWluLXdpZHRoOiA1NjBweClhbmQgKG1heC13aWR0aDogNzY4cHgpe1xyXG4gICAgLmtwaXN7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICB9XHJcbiAgICAubnVtYmVyc3tcclxuICAgICAgICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6IDUwJSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gICAgLmNpcmNsZXtcclxuICAgICAgICB3aWR0aDogNnJlbTtcclxuICAgICAgICBoZWlnaHQ6IDZyZW07XHJcbiAgICAgICAgaXtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAycmVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6MTAyNHB4KXtcclxuICAgIC5rcGlze1xyXG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMDtcclxuICAgICAgICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIFNlY3Rpb24gMyAqKiBCUE8gU2VjdGlvblxyXG4uYnBvLXRpdGxle1xyXG4gICAgaDJ7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGNvbG9yOiAjMzUzNTM1O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIG1hcmdpbjogOS41cmVtIDAgMy41cmVtIDAgO1xyXG4gICAgfVxyXG59XHJcbi5iZy1pbWctc3RpY2t5e1xyXG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWcvQlBPLWltZy5qcGcpIG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQsIHNjcm9sbDtcclxuICAgIGhlaWdodDogMzByZW07XHJcbiAgICAuY2FyZHN7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBwYWRkaW5nOjAgNnJlbSA7XHJcbiAgICB9XHJcbiAgICAuY2FyZHtcclxuICAgICAgICB3aWR0aDogMzAlO1xyXG4gICAgICAgIHBhZGRpbmc6NXJlbSAzcmVtO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgLmNhcmQtdGl0bGV7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNhcmQtZGVzY3JpcHRpb257XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogLjhyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jYXJkLWl0ZW17XHJcbiAgICAgICAgICAgIEBleHRlbmQgLmNhcmQtZGVzY3JpcHRpb247XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6NDI1cHgpe1xyXG4gICAgLmJnLWltZy1zdGlja3l7XHJcbiAgICAgICAgaGVpZ2h0OiA3MHJlbTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA0MCU7XHJcbiAgICAgICAgLmNhcmRze1xyXG4gICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDAgMXJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNhcmR7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAzcmVtIDFyZW0gMDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA1NjBweCkgYW5kIChtYXgtd2lkdGg6NzY4cHgpe1xyXG4gICAgLmJnLWltZy1zdGlja3l7XHJcbiAgICAgICAgaGVpZ2h0OiA0NXJlbTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA0MCU7XHJcbiAgICAgICAgLmNhcmRze1xyXG4gICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jYXJke1xyXG4gICAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAzcmVtIDFyZW0gMDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOjgwMHB4KSBhbmQgKG1heC13aWR0aDoxMDI0cHgpe1xyXG4gICAgLmJnLWltZy1zdGlja3l7XHJcbiAgICAgICAgaGVpZ2h0OiA0NXJlbTtcclxuICAgICAgICAuY2FyZHN7XHJcbiAgICAgICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICAgICAgLmNhcmR7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNXJlbSAzcmVtIDA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDoxNDQwcHgpe1xyXG4gICAgLmNhcmQtZGVzY3JpcHRpb257XHJcbiAgICAgICAgZm9udC1zaXplOiAuN3JlbTtcclxuICAgIH1cclxufVxyXG4vLyBTZWN0aW9uIDQgKiogVGVjaG5vbG9neSBzZWN0aW9uXHJcblxyXG4jdGVjbm97XHJcbiAgICBhbmltYXRpb246IGZhZGVJbiAuNXMgZWFzZS1pbjtcclxufVxyXG4udGVjbm8tdGl0bGV7XHJcbiAgICBAZXh0ZW5kIC5icG8tdGl0bGU7XHJcbn1cclxuIC50ZWNuby1jYXJkc3tcclxuICAgIC13ZWJraXQtYW5pbWF0aW9uOiBmYWRlSW4gMXMgZWFzZS1pbjtcclxuICAgICAgICAgICAgYW5pbWF0aW9uOiBmYWRlSW4gMXMgZWFzZS1pbjtcclxuICAgICAuY2FyZHN7XHJcbiAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgIHBhZGRpbmc6IDAgNnJlbTtcclxuICAgICB9XHJcbiAgICAgLmNhcmR7XHJcbiAgICAgICAgd2lkdGg6IDMwJTtcclxuICAgICAgICBwYWRkaW5nOiAzcmVtO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBjb2xvcjogIzM1MzUzNTtcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDNyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jYXJkLXRpdGxle1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jYXJkLWRlc2NyaXB0aW9ue1xyXG4gICAgICAgICAgICBmb250LXNpemU6IC44cmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY2FyZC1pdGVte1xyXG4gICAgICAgICAgICBAZXh0ZW5kIC5jYXJkLWRlc2NyaXB0aW9uO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICAgIH1cclxuICAgICB9XHJcbiB9XHJcbkBtZWRpYSAobWF4LXdpZHRoOiA0MjVweCl7XHJcbiAgICAudGVjbm8tY2FyZHN7XHJcbiAgICAgICAgLmNhcmRze1xyXG4gICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6MCAxcmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY2FyZHtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gfVxyXG4gQG1lZGlhKG1pbi13aWR0aDogNTYwcHgpYW5kKG1heC13aWR0aDo3NjhweCl7XHJcbiAgICAgLnRlY25vLWNhcmRze1xyXG4gICAgICAgICAuY2FyZHN7XHJcbiAgICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAgICAgICAgICBwYWRkaW5nOiAwIDFyZW07XHJcbiAgICAgICAgIH1cclxuICAgICAgICAgLmNhcmR7XHJcbiAgICAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgICB9XHJcbiAgICAgfVxyXG4gfVxyXG5AbWVkaWEgKG1pbi13aWR0aDogOTIwcHgpIGFuZCAobWF4LXdpZHRoOiAxMDI0cHgpe1xyXG4gICAgLnRlY25vLWNhcmRze1xyXG4gICAgICAgIC5jYXJkc3tcclxuICAgICAgICAgICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgICAgICAgICAuY2FyZHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIFZpZGVvXHJcbm9iamVjdHtcclxuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbiAgICBtYXgtd2lkdGg6IDEwMDBweCAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiA1MDBweDtcclxufVxyXG5cclxuXHJcbi8vIGFuaW1hdGlvbnNcclxuQGtleWZyYW1lcyBmYWRlSW4ge1xyXG4gICAgZnJvbSB7b3BhY2l0eSA6IDA7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMTAlKX1cclxuICAgIHRvIHsgb3BhY2l0eTogMTtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwJSl9XHJcbn1cclxuXHJcblxyXG5uZ2ItY2Fyb3VzZWwge1xyXG4gICAgbWF4LXdpZHRoOiA3MCU7XHJcbiAgICBtYXJnaW46IDUwcHggYXV0bztcclxufVxyXG5cclxubmdiLWNhcm91c2VsIGltZyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbn1cclxuXHJcbm5nYi1jYXJvdXNlbCB7XHJcbiAgICB3aWR0aDogaW5oZXJpdDtcclxuICAgIGhlaWdodDogaW5oZXJpdDtcclxufVxyXG5cclxuLmNhcm91c2VsLWlubmVyIHtcclxuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xyXG59XHJcblxyXG4uY2Fyb3VzZWwtaXRlbSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgdHJhbnNpdGlvbjogb3BhY2l0eSAxLjJzIGVhc2UtaW4tb3V0LCB2aXNpYmlsaXR5IDEuMnM7XHJcbiAgICB6LWluZGV4OiAtMTtcclxufVxyXG5cclxuLmNhcm91c2VsLWl0ZW0uYWN0aXZle1xyXG4gICAgb3BhY2l0eTogMTtcclxuICAgIHZpc2liaWxpdHk6IHZpc2libGU7XHJcbiAgICB6LWluZGV4OiAxMDtcclxufVxyXG5cclxuLmNhcm91c2VsLWNvbnRyb2wtcHJldiB7XHJcbiAgICAgei1pbmRleDogMjA7XHJcbn1cclxuXHJcblxyXG4uY2Fyb3VzZWwtY29udHJvbC1uZXh0IHtcclxuICAgICB6LWluZGV4OiAyMDtcclxufVxyXG5cclxuLmNhcm91c2VsLWluZGljYXRvcnN7XHJcbiAgICB6LWluZGV4OiAyMDtcclxufVxyXG4iLCIqIHtcbiAgZm9udC1mYW1pbHk6IFwib3Blbi1zYW5zXCIsIHNhbnMtc2VyaWY7XG59XG5cbmkge1xuICBmb250LWZhbWlseTogXCJGb250IEF3ZXNvbWUgNSBGcmVlXCI7XG59XG5cbi5pcy12YWxpZCB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgd2lkdGg6IDMwcmVtO1xuICBwYWRkaW5nLWxlZnQ6IDJyZW07XG4gIGNvbG9yOiAjZmZmO1xuICBib3JkZXI6IDFweCBzb2xpZCBncmVlbiAhaW1wb3J0YW50O1xufVxuLmlzLXZhbGlkOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjUpO1xufVxuXG4uaXMtaW52YWxpZCB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMTkyLCAyNiwgMzYsIDAuMikgIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgd2lkdGg6IDMwcmVtO1xuICBwYWRkaW5nLWxlZnQ6IDJyZW07XG4gIGNvbG9yOiAjZmZmO1xuICBib3JkZXI6IDFweCBzb2xpZCAjQzAxQTI0ICFpbXBvcnRhbnQ7XG59XG4uaXMtaW52YWxpZDpob3ZlciB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41KTtcbn1cblxuLmJnIGltZyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbiAgLXdlYmtpdC1hbmltYXRpb246IHNpemUtYmcgMS41cyBlYXNlLWluO1xuICBhbmltYXRpb246IHNpemUtYmcgMS41cyBlYXNlLWluO1xufVxuXG5ALXdlYmtpdC1rZXlmcmFtZXMgc2l6ZS1iZyB7XG4gIGZyb20ge1xuICAgIHdpZHRoOiAxMTAlO1xuICB9XG4gIHRvIHtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7XG4gIH1cbn1cbkBrZXlmcmFtZXMgc2l6ZS1iZyB7XG4gIGZyb20ge1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTEwJTtcbiAgfVxuICB0byB7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xuICB9XG59XG4uY29udGVuaWRvIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiAtbXMtZ3JpZDtcbiAgZGlzcGxheTogZ3JpZDtcbiAgLW1zLWdyaWQtY29sdW1uczogNTAlIDUwJTtcbiAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiA1MCUgNTAlO1xufVxuLmNvbnRlbmlkbyAudGV4dCB7XG4gIC1tcy1ncmlkLWNvbHVtbjogMTtcbiAgZ3JpZC1jb2x1bW46IDE7XG4gIHBhZGRpbmc6IDE1cmVtIDEwcmVtO1xufVxuLmNvbnRlbmlkbyAudGV4dCBoMSB7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDRyZW07XG4gIHdpZHRoOiA1MDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbn1cbi5jb250ZW5pZG8gLnRleHQgcCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IDQ0NXB4O1xuICBtYXJnaW4tYm90dG9tOiAycmVtO1xuICBmb250LXNpemU6IDEuMnJlbTtcbn1cbi5jb250ZW5pZG8gLnRleHQgLmZvbGxvdyB7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xufVxuLmNvbnRlbmlkbyAudGV4dCAuZm9sbG93IGkge1xuICBmb250LWZhbWlseTogXCJGb250IEF3ZXNvbWUgNSBCcmFuZHNcIjtcbn1cbi5jb250ZW5pZG8gLnRleHQgLmZvbGxvdyBhIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmNvbnRlbmlkbyAudGV4dCAuZm9sbG93IGE6aG92ZXIge1xuICBmb250LXNpemU6IDEuM3JlbTtcbn1cblxuLmNvbnRhY3RvIHtcbiAgLW1zLWdyaWQtY29sdW1uOiAyO1xuICBncmlkLWNvbHVtbjogMjtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgcGFkZGluZzogMTVyZW0gMCAwIDVyZW07XG59XG4uY29udGFjdG8gaDMge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICNmZmY7XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG59XG4uY29udGFjdG8gaW5wdXQsIC5jb250YWN0byB0ZXh0YXJlYSB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgd2lkdGg6IDMwcmVtO1xuICBwYWRkaW5nLWxlZnQ6IDJyZW07XG4gIGNvbG9yOiAjZmZmO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xufVxuLmNvbnRhY3RvIGlucHV0OmhvdmVyLCAuY29udGFjdG8gdGV4dGFyZWE6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNSk7XG59XG4uY29udGFjdG8gbGFiZWwge1xuICBjb2xvcjogd2hpdGU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBiYXNlbGluZTtcbn1cbi5jb250YWN0byBsYWJlbCBpbnB1dCwgLmNvbnRhY3RvIGxhYmVsIHRleHRhcmVhIHtcbiAgd2lkdGg6IDFyZW07XG4gIG1hcmdpbjogMCAwLjVyZW0gMCAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uY29udGFjdG8gbGFiZWwgYSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5jb250YWN0byAuYnRuLXNlbmQsIC5jb250YWN0byAuYnRuLXNlbmRJbnZhbGlkIHtcbiAgd2lkdGg6IDMwcmVtO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgY29sb3I6ICNBNDA1MDU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBoZWlnaHQ6IDNyZW07XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogMC4ycmVtO1xuICBtYXJnaW4tdG9wOiAxcmVtO1xufVxuLmNvbnRhY3RvIC5idG4tc2VuZDpob3ZlciwgLmNvbnRhY3RvIC5idG4tc2VuZEludmFsaWQ6aG92ZXIge1xuICAtd2Via2l0LWFuaW1hdGlvbjogY29sb3JCZyAwLjJzIGVhc2UtaW47XG4gIGFuaW1hdGlvbjogY29sb3JCZyAwLjJzIGVhc2UtaW47XG4gIGJhY2tncm91bmQ6ICNBNDA1MDU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbkAtd2Via2l0LWtleWZyYW1lcyBjb2xvckJnIHtcbiAgZnJvbSB7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgfVxuICB0byB7XG4gICAgYmFja2dyb3VuZDogI0E0MDUwNTtcbiAgfVxufVxuQGtleWZyYW1lcyBjb2xvckJnIHtcbiAgZnJvbSB7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgfVxuICB0byB7XG4gICAgYmFja2dyb3VuZDogI0E0MDUwNTtcbiAgfVxufVxuLmNvbnRhY3RvIC5idG4tc2VuZEludmFsaWQge1xuICBjb2xvcjogIzlkOWQ5ZDtcbn1cbi5jb250YWN0byAuYnRuLXNlbmRJbnZhbGlkOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgYW5pbWF0aW9uOiBub25lO1xuICBjb2xvcjogIzlkOWQ5ZDtcbn1cblxuLmRpc2FibGVkQnV0dG9uLCAuZGlzYWJsZWRCdXR0b246aG92ZXIge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLXRvcDogNHJlbTtcbn1cbi5kaXNhYmxlZEJ1dHRvbjpob3ZlciB7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG59XG5cbjo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlciB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuOjotbW96LXBsYWNlaG9sZGVyIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG46LW1zLWlucHV0LXBsYWNlaG9sZGVyIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG46Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuOjpwbGFjZWhvbGRlciB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDQxNHB4KSB7XG4gIC5iZyBpbWcge1xuICAgIHdpZHRoOiAxODA0cHggIWltcG9ydGFudDtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDQyNXB4KSB7XG4gIC5iZyBpbWcge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB3aWR0aDogMTgxNHB4O1xuICAgIGxlZnQ6IC0xMzg5cHg7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIC13ZWJraXQtYW5pbWF0aW9uOiBzaXplLWJnIDEuNXMgZWFzZS1pbjtcbiAgICBhbmltYXRpb246IHNpemUtYmcgMS41cyBlYXNlLWluO1xuICB9XG5cbiAgLmNvbnRlbmlkbyB7XG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICAtbXMtZ3JpZC1jb2x1bW5zOiBub25lO1xuICAgIC1tcy1ncmlkLXJvd3M6IDMwJSA3MCU7XG4gICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiAzMCUgNzAlO1xuICB9XG4gIC5jb250ZW5pZG8gLnRleHQge1xuICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcbiAgICBncmlkLWNvbHVtbjogMTtcbiAgICBwYWRkaW5nOiA3cmVtIDJyZW0gMDtcbiAgICAtbXMtZ3JpZC1yb3c6IDE7XG4gICAgZ3JpZC1yb3c6IDE7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIC5jb250ZW5pZG8gLnRleHQgaDEge1xuICAgIHdpZHRoOiAzNjRweDtcbiAgICBmb250LXNpemU6IDIuNXJlbTtcbiAgfVxuICAuY29udGVuaWRvIC50ZXh0IHAge1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICB3aWR0aDogMzY0cHg7XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCAuZm9sbG93IHtcbiAgICBmb250LXNpemU6IDFyZW07XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCAuZm9sbG93OmhvdmVyIHtcbiAgICBmb250LXNpemU6IDAuOXJlbTtcbiAgfVxuICAuY29udGVuaWRvIC50ZXh0IC5mb2xsb3cgYSB7XG4gICAgZm9udC1zaXplOiAxLjVyZW07XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCAuZm9sbG93IGEgLmZhLWZhY2Vib29rLXNxdWFyZSB7XG4gICAgbWFyZ2luLWxlZnQ6IDAuNXJlbSAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmNvbnRlbmlkbyA+ICo6bnRoLWNoaWxkKDEpIHtcbiAgICAtbXMtZ3JpZC1yb3c6IDE7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICB9XG5cbiAgLmNvbnRlbmlkbyA+ICo6bnRoLWNoaWxkKDIpIHtcbiAgICAtbXMtZ3JpZC1yb3c6IDI7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICB9XG5cbiAgLmNvbnRhY3RvIHtcbiAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XG4gICAgZ3JpZC1jb2x1bW46IDE7XG4gICAgLW1zLWdyaWQtcm93OiAyO1xuICAgIGdyaWQtcm93OiAyO1xuICAgIHBhZGRpbmc6IDVyZW0gMnJlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmNvbnRhY3RvIGgzIHtcbiAgICB3aWR0aDogMzY0cHg7XG4gICAgbWFyZ2luLXRvcDogMnJlbTtcbiAgfVxuICAuY29udGFjdG8gaW5wdXQsIC5jb250YWN0byB0ZXh0YXJlYSB7XG4gICAgd2lkdGg6IDIycmVtO1xuICB9XG4gIC5jb250YWN0byBsYWJlbCB7XG4gICAgd2lkdGg6IDM2NHB4O1xuICB9XG4gIC5jb250YWN0byAuYnRuLXNlbmQsIC5jb250YWN0byAuYnRuLXNlbmRJbnZhbGlkIHtcbiAgICB3aWR0aDogMjJyZW07XG4gICAgbWFyZ2luLXRvcDogMnJlbTtcbiAgfVxuICAuY29udGFjdG8gLmJ0bi1zZW5kSW52YWxpZCB7XG4gICAgd2lkdGg6IDIycmVtO1xuICAgIG1hcmdpbi10b3A6IDJyZW07XG4gICAgY29sb3I6ICM5ZDlkOWQ7XG4gIH1cbiAgLmNvbnRhY3RvIC5idG4tc2VuZEludmFsaWQ6aG92ZXIge1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA1NjBweCkgYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5iZyBpbWcge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB3aWR0aDogMTY4MHB4O1xuICAgIGxlZnQ6IC05MTJweDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgLXdlYmtpdC1hbmltYXRpb246IHNpemUtYmcgMS41cyBlYXNlLWluO1xuICAgIGFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XG4gIH1cblxuICAuY29udGVuaWRvIHtcbiAgICBkaXNwbGF5OiAtbXMtZ3JpZDtcbiAgICBkaXNwbGF5OiBncmlkO1xuICAgIC1tcy1ncmlkLWNvbHVtbnM6IG5vbmU7XG4gICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiBub25lO1xuICAgIC1tcy1ncmlkLXJvd3M6IDMwJSA3MCU7XG4gICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiAzMCUgNzAlO1xuICB9XG4gIC5jb250ZW5pZG8gLnRleHQge1xuICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcbiAgICBncmlkLWNvbHVtbjogMTtcbiAgICBwYWRkaW5nOiA3cmVtIDJyZW0gMDtcbiAgICAtbXMtZ3JpZC1yb3c6IDE7XG4gICAgZ3JpZC1yb3c6IDE7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIC5jb250ZW5pZG8gLnRleHQgaDEge1xuICAgIHdpZHRoOiA3MDRweDtcbiAgICBmb250LXNpemU6IDIuNXJlbTtcbiAgfVxuICAuY29udGVuaWRvIC50ZXh0IHAge1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICB3aWR0aDogNzA0cHg7XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCAuZm9sbG93IHtcbiAgICBmb250LXNpemU6IDFyZW07XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCAuZm9sbG93OmhvdmVyIHtcbiAgICBmb250LXNpemU6IDAuOXJlbTtcbiAgfVxuXG4gIC5jb250ZW5pZG8gPiAqOm50aC1jaGlsZCgxKSB7XG4gICAgLW1zLWdyaWQtcm93OiAxO1xuICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcbiAgfVxuXG4gIC5jb250ZW5pZG8gPiAqOm50aC1jaGlsZCgyKSB7XG4gICAgLW1zLWdyaWQtcm93OiAyO1xuICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcbiAgfVxuXG4gIC5jb250YWN0byB7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICAgIGdyaWQtY29sdW1uOiAxO1xuICAgIC1tcy1ncmlkLXJvdzogMjtcbiAgICBncmlkLXJvdzogMjtcbiAgICBwYWRkaW5nOiA1cmVtIDRyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIC5jb250YWN0byBpbnB1dCwgLmNvbnRhY3RvIHRleHRhcmVhIHtcbiAgICB3aWR0aDogNDByZW07XG4gIH1cbiAgLmNvbnRhY3RvIC5idG4tc2VuZCwgLmNvbnRhY3RvIC5idG4tc2VuZEludmFsaWQge1xuICAgIHdpZHRoOiA0MHJlbTtcbiAgfVxuICAuY29udGFjdG8gLmJ0bi1zZW5kSW52YWxpZCB7XG4gICAgd2lkdGg6IDQwcmVtO1xuICAgIGNvbG9yOiAjOWQ5ZDlkO1xuICB9XG4gIC5jb250YWN0byAuYnRuLXNlbmRJbnZhbGlkOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogODAwcHgpIGFuZCAobWF4LXdpZHRoOiAxMDI0cHgpIHtcbiAgLmJnIGltZyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiAxMjUwcHg7XG4gICAgbGVmdDogLTIyNnB4O1xuICAgIGhlaWdodDogYXV0bztcbiAgICAtd2Via2l0LWFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XG4gICAgYW5pbWF0aW9uOiBzaXplLWJnIDEuNXMgZWFzZS1pbjtcbiAgfVxuXG4gIC5jb250ZW5pZG8gLnRleHQge1xuICAgIHBhZGRpbmc6IDE1cmVtIDRyZW07XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCBoMSB7XG4gICAgd2lkdGg6IDQwOHB4O1xuICAgIGZvbnQtc2l6ZTogM3JlbTtcbiAgfVxuICAuY29udGVuaWRvIC50ZXh0IHAge1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICB3aWR0aDogNDA4cHg7XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCAuZm9sbG93IHtcbiAgICBmb250LXNpemU6IDFyZW07XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCAuZm9sbG93OmhvdmVyIHtcbiAgICBmb250LXNpemU6IDAuOXJlbTtcbiAgfVxuXG4gIC5jb250YWN0byB7XG4gICAgcGFkZGluZzogOHJlbSAwIDAgMDtcbiAgfVxufVxuLmtwaXMge1xuICBwYWRkaW5nLXRvcDogMTByZW07XG4gIGRpc3BsYXk6IGZsZXg7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBmYWRlSW4gMXMgZWFzZS1pbjtcbiAgYW5pbWF0aW9uOiBmYWRlSW4gMXMgZWFzZS1pbjtcbn1cbi5rcGlzIC5udW1iZXJzIHtcbiAgZGlzcGxheTogLW1zLWdyaWQ7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIGp1c3RpZnktaXRlbXM6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMjUlO1xufVxuLmtwaXMgLm51bWJlcnMgaDMge1xuICBtYXJnaW46IDFyZW0gMCAwIDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogNHJlbTtcbiAgY29sb3I6ICMzNTM1MzU7XG59XG4ua3BpcyAubnVtYmVycyBwIHtcbiAgY29sb3I6ICMzNTM1MzU7XG59XG4ua3BpcyAuY2lyY2xlIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHdpZHRoOiAxMHJlbTtcbiAgaGVpZ2h0OiAxMHJlbTtcbiAgYm9yZGVyLXJhZGl1czogNTByZW07XG4gIGJhY2tncm91bmQ6ICNDMDFBMjQ7XG59XG4ua3BpcyAuY2lyY2xlIGkge1xuICBmb250LXNpemU6IDRyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDQyNXB4KSB7XG4gIC5rcGlzIHtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAganVzdGlmeS1pdGVtczogY2VudGVyO1xuICB9XG5cbiAgLm51bWJlcnMge1xuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA1NjBweCkgYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5rcGlzIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgfVxuXG4gIC5udW1iZXJzIHtcbiAgICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6IDUwJSAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmNpcmNsZSB7XG4gICAgd2lkdGg6IDZyZW07XG4gICAgaGVpZ2h0OiA2cmVtO1xuICB9XG4gIC5jaXJjbGUgaSB7XG4gICAgZm9udC1zaXplOiAycmVtO1xuICB9XG59XG5AbWVkaWEgKG1heC13aWR0aDogMTAyNHB4KSB7XG4gIC5rcGlzIHtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAganVzdGlmeS1pdGVtczogY2VudGVyO1xuICB9XG59XG4uYnBvLXRpdGxlIGgyLCAudGVjbm8tdGl0bGUgaDIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjMzUzNTM1O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWFyZ2luOiA5LjVyZW0gMCAzLjVyZW0gMDtcbn1cblxuLmJnLWltZy1zdGlja3kge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZy9CUE8taW1nLmpwZykgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkLCBzY3JvbGw7XG4gIGhlaWdodDogMzByZW07XG59XG4uYmctaW1nLXN0aWNreSAuY2FyZHMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nOiAwIDZyZW07XG59XG4uYmctaW1nLXN0aWNreSAuY2FyZCB7XG4gIHdpZHRoOiAzMCU7XG4gIHBhZGRpbmc6IDVyZW0gM3JlbTtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgYm9yZGVyOiBub25lO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5iZy1pbWctc3RpY2t5IC5jYXJkIC5jYXJkLXRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xufVxuLmJnLWltZy1zdGlja3kgLmNhcmQgLmNhcmQtZGVzY3JpcHRpb24sIC5iZy1pbWctc3RpY2t5IC5jYXJkIC5jYXJkLWl0ZW0ge1xuICBmb250LXNpemU6IDAuOHJlbTtcbn1cbi5iZy1pbWctc3RpY2t5IC5jYXJkIC5jYXJkLWl0ZW0ge1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogNDI1cHgpIHtcbiAgLmJnLWltZy1zdGlja3kge1xuICAgIGhlaWdodDogNzByZW07XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogNDAlO1xuICB9XG4gIC5iZy1pbWctc3RpY2t5IC5jYXJkcyB7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICAgIHBhZGRpbmc6IDAgMXJlbTtcbiAgfVxuICAuYmctaW1nLXN0aWNreSAuY2FyZCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogM3JlbSAxcmVtIDA7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA1NjBweCkgYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5iZy1pbWctc3RpY2t5IHtcbiAgICBoZWlnaHQ6IDQ1cmVtO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IDQwJTtcbiAgfVxuICAuYmctaW1nLXN0aWNreSAuY2FyZHMge1xuICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgfVxuICAuYmctaW1nLXN0aWNreSAuY2FyZCB7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBwYWRkaW5nOiAzcmVtIDFyZW0gMDtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDgwMHB4KSBhbmQgKG1heC13aWR0aDogMTAyNHB4KSB7XG4gIC5iZy1pbWctc3RpY2t5IHtcbiAgICBoZWlnaHQ6IDQ1cmVtO1xuICB9XG4gIC5iZy1pbWctc3RpY2t5IC5jYXJkcyB7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9XG4gIC5iZy1pbWctc3RpY2t5IC5jYXJkcyAuY2FyZCB7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBwYWRkaW5nOiA1cmVtIDNyZW0gMDtcbiAgfVxufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxNDQwcHgpIHtcbiAgLmNhcmQtZGVzY3JpcHRpb24sIC50ZWNuby1jYXJkcyAuY2FyZCAuY2FyZC1pdGVtLCAuYmctaW1nLXN0aWNreSAuY2FyZCAuY2FyZC1pdGVtIHtcbiAgICBmb250LXNpemU6IDAuN3JlbTtcbiAgfVxufVxuI3RlY25vIHtcbiAgYW5pbWF0aW9uOiBmYWRlSW4gMC41cyBlYXNlLWluO1xufVxuXG4udGVjbm8tY2FyZHMge1xuICAtd2Via2l0LWFuaW1hdGlvbjogZmFkZUluIDFzIGVhc2UtaW47XG4gIGFuaW1hdGlvbjogZmFkZUluIDFzIGVhc2UtaW47XG59XG4udGVjbm8tY2FyZHMgLmNhcmRzIHtcbiAgZGlzcGxheTogZmxleDtcbiAgcGFkZGluZzogMCA2cmVtO1xufVxuLnRlY25vLWNhcmRzIC5jYXJkIHtcbiAgd2lkdGg6IDMwJTtcbiAgcGFkZGluZzogM3JlbTtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgYm9yZGVyOiBub25lO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjMzUzNTM1O1xufVxuLnRlY25vLWNhcmRzIC5jYXJkIGltZyB7XG4gIG1hcmdpbi1ib3R0b206IDNyZW07XG59XG4udGVjbm8tY2FyZHMgLmNhcmQgLmNhcmQtdGl0bGUge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxLjVyZW07XG59XG4udGVjbm8tY2FyZHMgLmNhcmQgLmNhcmQtZGVzY3JpcHRpb24sIC50ZWNuby1jYXJkcyAuY2FyZCAuY2FyZC1pdGVtIHtcbiAgZm9udC1zaXplOiAwLjhyZW07XG59XG4udGVjbm8tY2FyZHMgLmNhcmQgLmNhcmQtaXRlbSB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA0MjVweCkge1xuICAudGVjbm8tY2FyZHMgLmNhcmRzIHtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgcGFkZGluZzogMCAxcmVtO1xuICB9XG4gIC50ZWNuby1jYXJkcyAuY2FyZCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA1NjBweCkgYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC50ZWNuby1jYXJkcyAuY2FyZHMge1xuICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICBwYWRkaW5nOiAwIDFyZW07XG4gIH1cbiAgLnRlY25vLWNhcmRzIC5jYXJkIHtcbiAgICB3aWR0aDogNTAlO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogOTIwcHgpIGFuZCAobWF4LXdpZHRoOiAxMDI0cHgpIHtcbiAgLnRlY25vLWNhcmRzIC5jYXJkcyB7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICB9XG4gIC50ZWNuby1jYXJkcyAuY2FyZHMgLmNhcmQge1xuICAgIHdpZHRoOiA1MCU7XG4gIH1cbn1cbm9iamVjdCB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMTAwMHB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogNTAwcHg7XG59XG5cbkBrZXlmcmFtZXMgZmFkZUluIHtcbiAgZnJvbSB7XG4gICAgb3BhY2l0eTogMDtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgxMCUpO1xuICB9XG4gIHRvIHtcbiAgICBvcGFjaXR5OiAxO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDAlKTtcbiAgfVxufVxubmdiLWNhcm91c2VsIHtcbiAgbWF4LXdpZHRoOiA3MCU7XG4gIG1hcmdpbjogNTBweCBhdXRvO1xufVxuXG5uZ2ItY2Fyb3VzZWwgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG91dGxpbmU6IG5vbmU7XG59XG5cbm5nYi1jYXJvdXNlbCB7XG4gIHdpZHRoOiBpbmhlcml0O1xuICBoZWlnaHQ6IGluaGVyaXQ7XG59XG5cbi5jYXJvdXNlbC1pbm5lciB7XG4gIG92ZXJmbG93OiB2aXNpYmxlO1xufVxuXG4uY2Fyb3VzZWwtaXRlbSB7XG4gIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcbiAgb3BhY2l0eTogMDtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICB0cmFuc2l0aW9uOiBvcGFjaXR5IDEuMnMgZWFzZS1pbi1vdXQsIHZpc2liaWxpdHkgMS4ycztcbiAgei1pbmRleDogLTE7XG59XG5cbi5jYXJvdXNlbC1pdGVtLmFjdGl2ZSB7XG4gIG9wYWNpdHk6IDE7XG4gIHZpc2liaWxpdHk6IHZpc2libGU7XG4gIHotaW5kZXg6IDEwO1xufVxuXG4uY2Fyb3VzZWwtY29udHJvbC1wcmV2IHtcbiAgei1pbmRleDogMjA7XG59XG5cbi5jYXJvdXNlbC1jb250cm9sLW5leHQge1xuICB6LWluZGV4OiAyMDtcbn1cblxuLmNhcm91c2VsLWluZGljYXRvcnMge1xuICB6LWluZGV4OiAyMDtcbn0iXX0= */"] });
HomeComponent.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: HomeComponent, factory: HomeComponent.ɵfac, providedIn: "root" });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: "root",
            }]
    }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: "app-home",
                templateUrl: "./home.component.html",
                styleUrls: ["./home.component.scss"],
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Meta"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] }, { type: src_app_servicios_contacto_contacto_service__WEBPACK_IMPORTED_MODULE_4__["ContactoService"] }, { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] }, { type: src_app_servicios_i18nservice_language_service__WEBPACK_IMPORTED_MODULE_7__["LanguageService"] }]; }, { onWindowScroll: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ["window:scroll", ["$event"]]
        }] }); })();


/***/ }),

/***/ "./src/app/servicios/contacto/contacto.service.ts":
/*!********************************************************!*\
  !*** ./src/app/servicios/contacto/contacto.service.ts ***!
  \********************************************************/
/*! exports provided: ContactoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoService", function() { return ContactoService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");





const endpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].endpoint;
const httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
class ContactoService {
    constructor(http) {
        this.http = http;
    }
    // metodo para enviar correo
    senMail($data, Lang) {
        const params = {
            nombre: $data.nombre,
            telefono: $data.telefono,
            email: $data.email,
            asunto: $data.asunto,
            mensaje: $data.mensaje,
            cbox2: $data.cbox2,
            enviar: $data.enviar,
            lang: Lang,
        };
        return this.http.post(endpoint + 'formcos/datos_web', params, httpOptions);
    }
}
ContactoService.ɵfac = function ContactoService_Factory(t) { return new (t || ContactoService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
ContactoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ContactoService, factory: ContactoService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContactoService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/servicios/i18nservice/language.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/servicios/i18nservice/language.service.ts ***!
  \***********************************************************/
/*! exports provided: LanguageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageService", function() { return LanguageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");




class LanguageService {
    constructor(translate) {
        this.translate = translate;
        this.localeEvent = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.idioma$ = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        //sharedMessage = this.message.asObservable();
        this.msg = "msg";
        this.Message = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](this.msg);
    }
    changeLocale(locale) {
        this.translate.use(locale);
        this.localeEvent.next(locale);
    }
    nextMessage(message) {
        this.Message.next(message);
        console.log(message);
    }
}
LanguageService.ɵfac = function LanguageService_Factory(t) { return new (t || LanguageService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"])); };
LanguageService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: LanguageService, factory: LanguageService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LanguageService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"] }]; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    endpoint: 'https://app.outsourcingcos.com/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
document.addEventListener('DOMContentLoaded', () => {
    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
        .catch(err => console.error(err));
});


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\proyectos\website-cos\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map