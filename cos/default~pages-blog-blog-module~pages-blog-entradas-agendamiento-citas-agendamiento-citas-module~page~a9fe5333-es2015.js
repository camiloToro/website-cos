(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333"],{

/***/ "./src/app/servicios/blog.service.ts":
/*!*******************************************!*\
  !*** ./src/app/servicios/blog.service.ts ***!
  \*******************************************/
/*! exports provided: BlogService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogService", function() { return BlogService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class BlogService {
    constructor() {
        this.entrada = [
            {
                id: 1,
                img: "assets/img/blog-img.png",
                url: "/colombian-outsourcing-solutions-en-el-vii-foro-multilatinas",
                fecha: "noviembre 22, 2017 by tecnologia cos",
                title: "Colombian Outsourcing Solutions en el VII Foro Multilatinas",
                description: "La capital del país Inca recibió en el VII Foro Multilatinas a Colombian Outsourcing Solutions para reflexionar sobre los puntos débiles del modelo actual de globalización y lo que es necesario para reinventarlo y fortalecerlo; con el apoyo del Ministerio de Relaciones […]",
                text: "La capital del país Inca recibió en el VII Foro Multilatinas a Colombian Outsourcing Solutions para reflexionar sobre los puntos débiles del modelo actual de globalización y lo que es necesario para reinventarlo y fortalecerlo; con el apoyo del Ministerio de Relaciones Exteriores del Perú se dieron cita grandes empresas como: Telefónica, axialent, Zebra, Internexa los días 11 y 12 de octubre en “The Westin Hotel and convention Center”. Sin duda, una gran oportunidad para fortalecer las relaciones  comerciales de nuestra compañía."
            },
            {
                id: 2,
                img: "",
                url: "/convencion-internacional-de-seguros",
                fecha: "noviembre 22, 2017 by tecnologia cos",
                title: "Convención internacional de seguros",
                description: "Colombian Outsourcing Solutions estuvo presente en la Convención Internacional de Seguros realizada en el Hotel Hilton de la ciudad de Cartagena los días miércoles 4, jueves 5 y viernes 6 de octubre del presente año; organizada por la Federación de Aseguradores Colombianos FASECOLDA y que contó con patrocinadores del sector financiero, asegurador, salud y BPO, […]",
                text: "Colombian Outsourcing Solutions estuvo presente en la Convención Internacional de Seguros realizada en el Hotel Hilton de la ciudad de Cartagena los días miércoles 4, jueves 5 y viernes 6 de octubre del presente año; organizada por la Federación de Aseguradores Colombianos FASECOLDA y que contó con patrocinadores del sector financiero, asegurador, salud y BPO, la Heróica recibió a los Ministros de Hacienda y de Transporte, al presidente de FASECOLDA, así como destacados empresarios, escritores, conferencistas y periodistas."
            },
            {
                id: 3,
                img: "",
                url: "/cos-fue-participe-del-foro-de-la-salud",
                fecha: "junio 21, 2017 by Karen Molina",
                title: "COS fue participe del Foro de la salud",
                description: "La 24° versión del Foro de la Salud y Farmacéutico de la ANDI (Asociación Nacional de Empresarios de Colombia) llevada a cabo los días 14, 15 y 16 de Junio en el Centro de Convenciones de Cartagena de Indias,  contó con la presencia del Procurador General de la Nación, reconocidos empresarios que trataron temas coyunturales […]",
                text: "La 24° versión del Foro de la Salud y Farmacéutico de la ANDI (Asociación Nacional de Empresarios de Colombia) llevada a cabo los días 14, 15 y 16 de Junio en el Centro de Convenciones de Cartagena de Indias,  contó con la presencia del Procurador General de la Nación, reconocidos empresarios que trataron temas coyunturales del sector y Francia como país invitado de Honor, en cabeza del Señor Embajador Jean-Marc Laforêt, quienes presentaron la experiencia del país europeo en temas del sistema de salud. COS estuvo presente para dar a conocer su portafolio y destacar su fortaleza en la prestación del servicio de contact center en el sector salud, avalado por grandes empresas que constatan la calidad en infraestructura, tecnología y cercanía con el usuario."
            },
            {
                id: 4,
                img: "",
                url: "/cos-presente-en-el-ix-congreso-latinoamericano-de-talento-humano",
                fecha: "junio 16, 2017 by Karen Molina",
                title: "COS presente en el IX Congreso Latinoamericano de Talento Humano",
                description: "En el marco del IX Congreso Latinoamericano de Talento Humano organizado por la Revista Empresarial y Laboral que se llevó a cabo en el Centro Empresarial El Cubo de Bogotá los días 8 y 9 de Junio se dieron cita destacados empresarios, coach y consultores para dar desarrollo al eje temático del evento: “El valor […]",
                text: "En el marco del IX Congreso Latinoamericano de Talento Humano organizado por la Revista Empresarial y Laboral que se llevó a cabo en el Centro Empresarial El Cubo de Bogotá los días 8 y 9 de Junio se dieron cita destacados empresarios, coach y consultores para dar desarrollo al eje temático del evento: “El valor del Capital Humano”.  COS estuvo presente para poder debatir temas inherentes al recurso fundamental de toda organización, tales como: gerencia humanística, estabilidad laboral, transformación del talento humano, impacto legal del Sistema de Gestión de Seguridad y Salud en el Trabajo (SG-SST), equipos de trabajo, entre otros, toda esta retroalimentación será  sin duda aplicada y tendrá un impacto positivo al interior de COS para que sus empleados sigan compartiendo el mismo objetivo: ¡Calidad y Excelencia de vida!"
            },
            {
                id: 5,
                img: "",
                url: "/nueva-alianza-cos-se-globaliza-con-tele-web",
                fecha: "mayo 5, 2017 by tecnologia cos",
                title: "¡NUEVA ALIANZA! COS se globaliza con 95teleweb Information Inc. y Masterpiece Group Inc.",
                description: "El día miércoles 5 de abril COS se expande en el mercado asiático gracias a la firma de la nueva alianza con 95teleweb Inc. y Materpice Group Inc. De esta manera podemos ofrecer nuestros servicios no solo a clientes en América y Europa sino a nivel global. Esto nos permitirá dar soluciones con un alcance mucho […]",
                text: "El día miércoles 5 de abril COS se expande en el mercado asiático gracias a la firma de la nueva alianza con 95teleweb Inc. y Materpice Group Inc. De esta manera podemos ofrecer nuestros servicios no solo a clientes en América y Europa sino a nivel global. Esto nos permitirá dar soluciones con un alcance mucho mayor y seguir demostrando que somos la solución más efectiva."
            },
            {
                id: 6,
                img: "",
                url: "/ix-conferencia-panamericana-de-logistica",
                fecha: "abril 3, 2017 by COScolombi@",
                title: "IX Conferencia Panamericana de Logística",
                description: "Lugar: Hotel El Prado – Barranquilla (CO) Organizador: CP Logística Fecha: Marzo 7, 8, 9 de 2016 Diferentes empresarios se dan cita anualmente en este evento que se ha consolidado como uno de los más representativos del sector logístico por la alta calidad de sus contenidos académicos y las diferentes visitas de campo que se […]",
                text: "Diferentes empresarios se dan cita anualmente en este evento que se ha consolidado como uno de los más representativos del sector logístico por la alta calidad de sus contenidos académicos y las diferentes visitas de campo que se incluyen en la agenda. La Conferencia Panamericana de Logística reúne cerca de 300 empresas en este escenario para intercambiar opiniones, actualizar conocimientos y establecer vínculos comerciales sostenibles a largo plazo; una vez más Colombian Outsourcing Solutions hace parte de este gran evento en el que da a conocer a todos sus asistentes las ventajas de tercerizar sus servicios con nosotros. Cada vez son más las empresas que han encontrado su solución efectiva!"
            },
            {
                id: 7,
                img: "",
                url: "/premio-oro-en-el-6-premio-nacional-a-la-excelencia-de-la-industria-en-las-interacciones-con-clientes",
                fecha: "abril 3, 2017 by COScolombi@",
                title: "Premio Oro en el 6° Premio Nacional a la Excelencia de la Industria en las Interacciones con Clientes",
                description: "El Premio Nacional a la Excelencia de la Industria en las interacciones con Clientes busca reconocer, estimular y premiar públicamente a empresas que con la implementación de iniciativas innovadoras logran construir mejores prácticas empresariales en el marco de la gestión estratégica, operacional, tecnológica y del recurso humano en favor de un experiencia memorable del cliente. […]",
                text: "El Premio Nacional a la Excelencia de la Industria en las interacciones con Clientes busca reconocer, estimular y premiar públicamente a empresas que con la implementación de iniciativas innovadoras logran construir mejores prácticas empresariales en el marco de la gestión estratégica, operacional, tecnológica y del recurso humano en favor de un experiencia memorable del cliente.  Dentro de la categoría Mejor Contribución Tecnológica se evaluaron los proyectos de mejora en administración, optimización e implementación de tecnología, que hayan tenido un impacto directo en la satisfacción y percepción del cliente, y un impacto positivo en los resultados de la empresa. El premio fue otorgado a COS por su excelente implementación para campañas outbound con mejoras importantes en todos los niveles de la operación."
            },
            {
                id: 8,
                img: "",
                url: "/agendamiento-de-citas-comerciales",
                fecha: "enero 17, 2017 by COScolombi@",
                title: "Agendamiento de citas comerciales",
                description: "Este será el primer acercamiento con un posible cliente potencial, y por ende, el más importante ya que tendremos que despertar el interés de la persona que contactamos y a la vez destacar generando una excelente primera impresión, adicionalmente, dar a conocer nuestro producto y/o servicio. En nuestro Call center Solutions le recomendamos el Tele […]",
                text: "Este será el primer acercamiento con un posible cliente potencial, y por ende, el más importante ya que tendremos que despertar el interés de la persona que contactamos y a la vez destacar generando una excelente primera impresión, adicionalmente, dar a conocer nuestro producto y/o servicio. En nuestro Call center Solutions le recomendamos el Tele Marketing Solutions para la captación de clientes en cualquier nicho de mercado. Le brindaremos gran variedad de beneficios a nivel de Call Center Colombia para cada una de las compañías que adquieran este servicio, por ejemplo, buscar mejores oportunidades de negocio, control, seguimiento y medición del producto, además, optimizar tiempos en desplazamiento y citas poco productivas, esto, debido a que creamos preguntas filtro en cada una de las llamadas generando así mayor productividad y efectividad en la venta final. Ofrecemos la más alta experiencia comercial con el mejor recuso humano a nivel de Contact Center Solutions, también, tecnología vanguardista en dos sedes de Call center Bogotá disponibles a su servicio, pero sobre todo la mejor calidad en el servicio de las Empresas de Tele Marketing."
            },
            {
                id: 9,
                img: "",
                url: "/son-rentables-los-servicios-de-outsourcing-en-colombia",
                fecha: "enero 17, 2017 by COScolombi@",
                title: "¿Son rentables los servicios de Outsourcing en Colombia?",
                description: "Cada vez las comunicaciones son más sencillas y externalizar ciertas operaciones de la empresa nos facilita el trabajo. Hoy en día con los avances de la tecnología, estos servicios externalizados conocidos como BPO en Colombia nos permiten enfocarnos en el Core de la empresa y dejar en manos de especialistas las ramas de la empresa […]",
                text: "Cada vez las comunicaciones son más sencillas y externalizar ciertas operaciones de la empresa nos facilita el trabajo. Hoy en día con los avances de la tecnología, estos servicios externalizados conocidos como BPO en Colombia nos permiten enfocarnos en el Core de la empresa y dejar en manos de especialistas las ramas de la empresa a las que no podemos llegar o llegaríamos de una manera mucho menos efectiva y con unos costes mayores. En definitiva, Empresas de Tele marketing con servicios de Call center Bogotá y Call Center Colombia, han sabido aprovechar el momento. El Outsourcing en Colombia está en auge; y nuestro Contact Center Solutions cuenta con un servicio excepcional, trabajando en Tele Marketing Solutions y Call Center Solutions en beneficio de su empresa."
            },
            {
                id: 10,
                img: "",
                url: "/por-que-triunfa-el-call-center-en-colombia",
                fecha: "enero 17, 2017 by COScolombi@",
                title: "¿POR QUÉ TRIUNFA EL CALL CENTER EN COLOMBIA?",
                description: "Los colombianos han sabido aprovechar las ventajas que tienen las empresas con la tercerización de sus servicios a través de: Call Center Solutions, Contact Center Solutions y Tele Marketing Solutions, por esto, han renovado sus servicios de Outsourcing. La tendencia ha evolucionado y nos hemos dado cuenta que tratar bien a sus clientes no es […]",
                text: "Los colombianos han sabido aprovechar las ventajas que tienen las empresas con la tercerización de sus servicios a través de: Call Center Solutions, Contact Center Solutions y Tele Marketing Solutions, por esto, han renovado sus servicios de Outsourcing. La tendencia ha evolucionado y nos hemos dado cuenta que tratar bien a sus clientes no es suficiente, es necesario brindarles un excelente servicio, y Colombian Outsourcing Solutions se especializa en mantener altos estándares de calidad, los cuales llevarán a sus clientes a mantenerse fiel a su empresa y asimismo atraer más para que su negocio crezca a pasos agigantados. Nuestro Call Center Bogotá ha sabido formar un grupo de trabajo especializado lo cual nos ha permitido destacarnos de otras Empresas de Tele Marketing y contar a la fecha con los mejores clientes COS a nivel nacional e internacional, prestando, entre otros, nuestros servicios de contact center Colombia y Call Center Bilingüe."
            },
        ];
    }
}
BlogService.ɵfac = function BlogService_Factory(t) { return new (t || BlogService)(); };
BlogService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: BlogService, factory: BlogService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BlogService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ })

}]);
//# sourceMappingURL=default~pages-blog-blog-module~pages-blog-entradas-agendamiento-citas-agendamiento-citas-module~page~a9fe5333-es2015.js.map