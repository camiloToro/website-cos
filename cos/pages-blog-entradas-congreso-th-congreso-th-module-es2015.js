(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-blog-entradas-congreso-th-congreso-th-module"],{

/***/ "./src/app/pages/blog/entradas/congreso-th/congreso-th-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/blog/entradas/congreso-th/congreso-th-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: CongresoThRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CongresoThRoutingModule", function() { return CongresoThRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _congreso_th_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./congreso-th.component */ "./src/app/pages/blog/entradas/congreso-th/congreso-th.component.ts");





const routes = [
    {
        path: '', component: _congreso_th_component__WEBPACK_IMPORTED_MODULE_2__["CongresoThComponent"]
    }
];
class CongresoThRoutingModule {
}
CongresoThRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: CongresoThRoutingModule });
CongresoThRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function CongresoThRoutingModule_Factory(t) { return new (t || CongresoThRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](CongresoThRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CongresoThRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/pages/blog/entradas/congreso-th/congreso-th.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/blog/entradas/congreso-th/congreso-th.component.ts ***!
  \**************************************************************************/
/*! exports provided: CongresoThComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CongresoThComponent", function() { return CongresoThComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/servicios/blog.service */ "./src/app/servicios/blog.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





const _c0 = function () { return ["/blog"]; };
class CongresoThComponent {
    constructor(meta, title, _blogService) {
        this.meta = meta;
        this.title = title;
        this._blogService = _blogService;
        this.entrada = [];
    }
    ngOnInit() {
        // usar el array del servicio blog
        this.entrada = this._blogService.entrada;
        // Para ñadir el título de la página
        this.title.setTitle('Colombian Outsourcing Solution - COS');
        // Añadir el tag de la info de la página
        this.meta.addTag({
            name: 'page.info',
            content: 'COS presente en el IX Congreso Latinoamericano de Talento Humano'
        });
    }
    ngOnDestroy() {
        // Quitar el tag "page.info" antes de ir a otra página, para que rastree correctamente la información
        this.meta.removeTag('name=\'page.info\'');
    }
}
CongresoThComponent.ɵfac = function CongresoThComponent_Factory(t) { return new (t || CongresoThComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__["BlogService"])); };
CongresoThComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CongresoThComponent, selectors: [["app-congreso-th"]], decls: 67, vars: 7, consts: [["id", "landing", 1, "bg"], ["alt", "contact-center-BPO-COS-blog", 3, "src"], [1, "contenido"], [1, "text"], [1, "mt-5", "follow"], ["href", "https://www.facebook.com/Outsourcingcos/", "target", "blank"], [1, "fab", "fa-facebook-square", "ml-5"], ["href", "https://www.instagram.com/cosoutsourcing/", "target", "blank"], [1, "fab", "fa-instagram-square", "ml-3"], ["href", "https://www.linkedin.com/company/colombianoutsourcingcos/about/", "target", "blank"], [1, "fab", "fa-linkedin", "ml-3"], ["href", "https://www.youtube.com/channel/UCL89AlspoSj_exe6qidqHew", "target", "blank"], [1, "fab", "fa-youtube", "ml-3"], [1, "container"], [1, "fab", "fa-facebook-square", "ml-3"], [1, "tag-blog"], [3, "routerLink"], [1, "fas", "fa-tag"], [1, "comment"], [1, "form-group"], [1, "form-group", "nombre"], ["type", "text", "placeholder", "Ejem: Carlos, Maria, etc ...", 1, "form-control"], [1, "form-group", "correo"], ["type", "text", "placeholder", "Ejem: email@email.com ...", 1, "form-control"], [1, "form-group", "web"], ["type", "text", "placeholder", "Ejem: www.mysitioweb.com ...", 1, "form-control"], [1, "form-group", "comentario"], ["name", "", "id", "", "cols", "30", "rows", "10", "placeholder", "D\u00E9janos tu comentario", 1, "form-control"], [1, "btn", "btn-primary", "btn-block", "mt-10"]], template: function CongresoThComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "s\u00EDguenos en ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "i", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "s\u00EDguenos en ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "i", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "a", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "i", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " Noticias ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, " Deja tu comentario ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, " Tu Correo no ser\u00E1 publicado. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Los campos marcados con (*) con obligatorios");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Nombre *");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "input", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "correo *");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "input", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Web");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "input", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Comentario *");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "textarea", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "button", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, " Enviar Comentario ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.entrada[0].img, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[3].title);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.entrada[3].title, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[3].fecha);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[3].text);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](6, _c0));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"]], styles: ["*[_ngcontent-%COMP%] {\n  font-family: \"open-sans\", sans-serif;\n}\n\ni[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Free\";\n}\n\n[_ngcontent-%COMP%]::-moz-placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n[_ngcontent-%COMP%]::-ms-input-placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n[_ngcontent-%COMP%]::placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n.bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n  position: absolute;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n  position: relative;\n  padding: 16rem 12rem 0;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  font-weight: 800;\n  color: #fff;\n  font-size: 3rem;\n  width: 800px;\n  margin-bottom: 12rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: white;\n  width: 445px;\n  margin-bottom: 2rem;\n  font-size: 1.2rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: white;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  font-size: 1.3rem;\n}\n\n.container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  margin-top: 10rem;\n  font-weight: bold;\n  color: #353535;\n}\n\n.container[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin-bottom: 2rem;\n  font-weight: bold;\n}\n\n.container[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n  font-size: 1.5rem;\n  color: #C01A24;\n}\n\n.container[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]:hover {\n  color: #610d12;\n}\n\n.container[_ngcontent-%COMP%]   .tag-blog[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 0.8rem;\n}\n\n.container[_ngcontent-%COMP%]   .tag-blog[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #353535;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%] {\n  margin-bottom: 0.5rem;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .nombre[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin: 2rem 0 0 0;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .correo[_ngcontent-%COMP%]   label[_ngcontent-%COMP%], .container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .comentario[_ngcontent-%COMP%]   label[_ngcontent-%COMP%], .container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .web[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin: 0.5rem 0 0 0;\n}\n\n.container[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  margin-top: 3rem;\n  height: 3rem;\n}\n\n@media (min-width: 800px) and (max-width: 1024px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -200px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    margin-bottom: 7rem;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -456px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n    padding: 10rem 11rem 0;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2rem;\n    width: 460px;\n  }\n\n  .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n    margin-top: 11rem;\n  }\n}\n\n@media (min-width: 356px) and (max-width: 425px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 944px;\n    height: auto;\n    left: -519px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n    padding: 10rem 1rem 0;\n    text-align: center;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2rem;\n    width: 393px;\n    margin-bottom: 5rem;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 393px;\n  }\n\n  .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n    margin-top: 10rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYmxvZy9lbnRyYWRhcy9jb25ncmVzby10aC9EOlxccHJveWVjdG9zXFx3ZWJzaXRlLWNvcy9zcmNcXGFwcFxccGFnZXNcXGJsb2dcXGVudHJhZGFzXFxjb25ncmVzby10aFxcY29uZ3Jlc28tdGguY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Jsb2cvZW50cmFkYXMvY29uZ3Jlc28tdGgvY29uZ3Jlc28tdGguY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQ0FBQTtBQ0NKOztBREVBO0VBQ0ksa0NBQUE7QUNDSjs7QURDQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FDRUo7O0FESkE7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQ0VKOztBREpBO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUNFSjs7QURFSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNDUjs7QURDSTtFQUNJLGtCQUFBO0VBQ0Esc0JBQUE7QUNDUjs7QURBUTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7QUNFWjs7QURBUTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQ0VaOztBREFRO0VBQ0ksaUJBQUE7QUNFWjs7QUREWTtFQUNJLG9DQUFBO0FDR2hCOztBRERZO0VBQ0ksWUFBQTtBQ0doQjs7QURGZ0I7RUFDSSxpQkFBQTtBQ0lwQjs7QURHSTtFQUNJLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FDQVI7O0FERUk7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0FDQVI7O0FER1E7RUFDSSxvQ0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQ0RaOztBREVZO0VBQ0ksY0FBQTtBQ0FoQjs7QURLUTtFQUNJLGlCQUFBO0FDSFo7O0FES1E7RUFDSSxjQUFBO0FDSFo7O0FETUk7RUFDSSxxQkFBQTtBQ0pSOztBRE1ZO0VBQ0ksa0JBQUE7QUNKaEI7O0FEUVk7RUFDSSxvQkFBQTtBQ05oQjs7QURnQkk7RUFDSSxnQkFBQTtFQUNBLFlBQUE7QUNkUjs7QURrQkE7RUFFUTtJQUNJLGFBQUE7SUFDQSxZQUFBO0VDaEJWO0VEb0JVO0lBQ0ksbUJBQUE7RUNsQmQ7QUFDRjs7QUR3QkE7RUFFUTtJQUNJLGFBQUE7SUFDQSxZQUFBO0VDdkJWO0VEeUJNO0lBQ0ksc0JBQUE7RUN2QlY7RUR3QlU7SUFDSSxlQUFBO0lBQ0EsWUFBQTtFQ3RCZDs7RUQyQk07SUFDSSxpQkFBQTtFQ3hCVjtBQUNGOztBRDRCQTtFQUVRO0lBQ0ksWUFBQTtJQUNBLFlBQUE7SUFDQSxZQUFBO0VDM0JWO0VENkJNO0lBQ0kscUJBQUE7SUFDQSxrQkFBQTtFQzNCVjtFRDRCVTtJQUNJLGVBQUE7SUFDQSxZQUFBO0lBQ0EsbUJBQUE7RUMxQmQ7RUQ0QlU7SUFDSSxlQUFBO0lBQ0EsWUFBQTtFQzFCZDs7RURnQ007SUFDSSxpQkFBQTtFQzdCVjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYmxvZy9lbnRyYWRhcy9jb25ncmVzby10aC9jb25ncmVzby10aC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIip7XHJcbiAgICBmb250LWZhbWlseTogJ29wZW4tc2FucycsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbml7XHJcbiAgICBmb250LWZhbWlseTogJ0ZvbnQgQXdlc29tZSA1IEZyZWUnO1xyXG59XHJcbjo6cGxhY2Vob2xkZXJ7XHJcbiAgICBjb2xvcjogI2M0YzRjNDtcclxuICAgIHBhZGRpbmc6IC41cmVtO1xyXG59XHJcbi8vIEJhY2tncm91bmcgY2xhc3NcclxuLmJne1xyXG4gICAgaW1ne1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7ICAgXHJcbiAgICB9XHJcbiAgICAuY29udGVuaWRve1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBwYWRkaW5nOiAxNnJlbSAxMnJlbSAwO1xyXG4gICAgICAgIGgxe1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogODAwO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAzcmVtO1xyXG4gICAgICAgICAgICB3aWR0aDogODAwcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEycmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwe1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIHdpZHRoOiA0NDVweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjJyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5mb2xsb3d7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6ICdGb250IEF3ZXNvbWUgNSBCcmFuZHMnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGF7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMS4zcmVtO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbi5jb250YWluZXJ7XHJcbiAgICBoMntcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHJlbTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzM1MzUzNTtcclxuICAgIH1cclxuICAgIGxhYmVse1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDJyZW07XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcbiAgICAuZm9sbG93e1xyXG4gICAgICAgIGl7XHJcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnRm9udCBBd2Vzb21lIDUgQnJhbmRzJztcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICAgICAgICAgIGNvbG9yOiAjQzAxQTI0O1xyXG4gICAgICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6cmdiKDk3LCAxMywgMTgpIDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC50YWctYmxvZ3tcclxuICAgICAgICBpe1xyXG4gICAgICAgICAgICBmb250LXNpemU6IC44cmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBhe1xyXG4gICAgICAgICAgICBjb2xvcjogIzM1MzUzNTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuZm9ybS1ncm91cHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAuNXJlbTtcclxuICAgICAgICAubm9tYnJle1xyXG4gICAgICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjoycmVtIDAgMCAwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb3JyZW97XHJcbiAgICAgICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOi41cmVtIDAgMCAwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC53ZWJ7XHJcbiAgICAgICAgICAgIEBleHRlbmQgLmNvcnJlbztcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNvbWVudGFyaW97XHJcbiAgICAgICAgICAgIEBleHRlbmQgLmNvcnJlbztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBidXR0b257XHJcbiAgICAgICAgbWFyZ2luLXRvcDogM3JlbTtcclxuICAgICAgICBoZWlnaHQ6IDNyZW07XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOjgwMHB4KWFuZChtYXgtd2lkdGg6MTAyNHB4KXtcclxuICAgIC5iZ3tcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMjI0cHg7XHJcbiAgICAgICAgICAgIGxlZnQ6IC0yMDBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNvbnRlbmlkb3tcclxuXHJcbiAgICAgICAgICAgIGgxe1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogN3JlbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOjU2MHB4KWFuZChtYXgtd2lkdGg6NzY4cHgpe1xyXG4gICAgLmJne1xyXG4gICAgICAgIGltZ3tcclxuICAgICAgICAgICAgd2lkdGg6IDEyMjRweDtcclxuICAgICAgICAgICAgbGVmdDogLTQ1NnB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY29udGVuaWRve1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHJlbSAxMXJlbSAwO1xyXG4gICAgICAgICAgICBoMXtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMnJlbTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA0NjBweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jb250YWluZXJ7XHJcbiAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDExcmVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDM1NnB4KWFuZChtYXgtd2lkdGg6NDI1cHgpe1xyXG4gICAgLmJne1xyXG4gICAgICAgIGltZ3tcclxuICAgICAgICAgICAgd2lkdGg6IDk0NHB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgIGxlZnQ6IC01MTlweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNvbnRlbmlkb3tcclxuICAgICAgICAgICAgcGFkZGluZzogMTByZW0gMXJlbSAwO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGgxe1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAycmVtO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6MzkzcHg7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOjVyZW0gOyBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBwe1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDM5M3B4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNvbnRhaW5lcntcclxuICAgICAgICBcclxuICAgICAgICBoMntcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTByZW07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiKiB7XG4gIGZvbnQtZmFtaWx5OiBcIm9wZW4tc2Fuc1wiLCBzYW5zLXNlcmlmO1xufVxuXG5pIHtcbiAgZm9udC1mYW1pbHk6IFwiRm9udCBBd2Vzb21lIDUgRnJlZVwiO1xufVxuXG46OnBsYWNlaG9sZGVyIHtcbiAgY29sb3I6ICNjNGM0YzQ7XG4gIHBhZGRpbmc6IDAuNXJlbTtcbn1cblxuLmJnIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi5iZyAuY29udGVuaWRvIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBwYWRkaW5nOiAxNnJlbSAxMnJlbSAwO1xufVxuLmJnIC5jb250ZW5pZG8gaDEge1xuICBmb250LXdlaWdodDogODAwO1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1zaXplOiAzcmVtO1xuICB3aWR0aDogODAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEycmVtO1xufVxuLmJnIC5jb250ZW5pZG8gcCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IDQ0NXB4O1xuICBtYXJnaW4tYm90dG9tOiAycmVtO1xuICBmb250LXNpemU6IDEuMnJlbTtcbn1cbi5iZyAuY29udGVuaWRvIC5mb2xsb3cge1xuICBmb250LXNpemU6IDEuNXJlbTtcbn1cbi5iZyAuY29udGVuaWRvIC5mb2xsb3cgaSB7XG4gIGZvbnQtZmFtaWx5OiBcIkZvbnQgQXdlc29tZSA1IEJyYW5kc1wiO1xufVxuLmJnIC5jb250ZW5pZG8gLmZvbGxvdyBhIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmJnIC5jb250ZW5pZG8gLmZvbGxvdyBhOmhvdmVyIHtcbiAgZm9udC1zaXplOiAxLjNyZW07XG59XG5cbi5jb250YWluZXIgaDIge1xuICBtYXJnaW4tdG9wOiAxMHJlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMzUzNTM1O1xufVxuLmNvbnRhaW5lciBsYWJlbCB7XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNvbnRhaW5lciAuZm9sbG93IGkge1xuICBmb250LWZhbWlseTogXCJGb250IEF3ZXNvbWUgNSBCcmFuZHNcIjtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIGNvbG9yOiAjQzAxQTI0O1xufVxuLmNvbnRhaW5lciAuZm9sbG93IGk6aG92ZXIge1xuICBjb2xvcjogIzYxMGQxMjtcbn1cbi5jb250YWluZXIgLnRhZy1ibG9nIGkge1xuICBmb250LXNpemU6IDAuOHJlbTtcbn1cbi5jb250YWluZXIgLnRhZy1ibG9nIGEge1xuICBjb2xvcjogIzM1MzUzNTtcbn1cbi5jb250YWluZXIgLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XG59XG4uY29udGFpbmVyIC5mb3JtLWdyb3VwIC5ub21icmUgbGFiZWwge1xuICBtYXJnaW46IDJyZW0gMCAwIDA7XG59XG4uY29udGFpbmVyIC5mb3JtLWdyb3VwIC5jb3JyZW8gbGFiZWwsIC5jb250YWluZXIgLmZvcm0tZ3JvdXAgLmNvbWVudGFyaW8gbGFiZWwsIC5jb250YWluZXIgLmZvcm0tZ3JvdXAgLndlYiBsYWJlbCB7XG4gIG1hcmdpbjogMC41cmVtIDAgMCAwO1xufVxuLmNvbnRhaW5lciBidXR0b24ge1xuICBtYXJnaW4tdG9wOiAzcmVtO1xuICBoZWlnaHQ6IDNyZW07XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA4MDBweCkgYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xuICAuYmcgaW1nIHtcbiAgICB3aWR0aDogMTIyNHB4O1xuICAgIGxlZnQ6IC0yMDBweDtcbiAgfVxuICAuYmcgLmNvbnRlbmlkbyBoMSB7XG4gICAgbWFyZ2luLWJvdHRvbTogN3JlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDU2MHB4KSBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgLmJnIGltZyB7XG4gICAgd2lkdGg6IDEyMjRweDtcbiAgICBsZWZ0OiAtNDU2cHg7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8ge1xuICAgIHBhZGRpbmc6IDEwcmVtIDExcmVtIDA7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8gaDEge1xuICAgIGZvbnQtc2l6ZTogMnJlbTtcbiAgICB3aWR0aDogNDYwcHg7XG4gIH1cblxuICAuY29udGFpbmVyIGgyIHtcbiAgICBtYXJnaW4tdG9wOiAxMXJlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDM1NnB4KSBhbmQgKG1heC13aWR0aDogNDI1cHgpIHtcbiAgLmJnIGltZyB7XG4gICAgd2lkdGg6IDk0NHB4O1xuICAgIGhlaWdodDogYXV0bztcbiAgICBsZWZ0OiAtNTE5cHg7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8ge1xuICAgIHBhZGRpbmc6IDEwcmVtIDFyZW0gMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8gaDEge1xuICAgIGZvbnQtc2l6ZTogMnJlbTtcbiAgICB3aWR0aDogMzkzcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNXJlbTtcbiAgfVxuICAuYmcgLmNvbnRlbmlkbyBwIHtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgd2lkdGg6IDM5M3B4O1xuICB9XG5cbiAgLmNvbnRhaW5lciBoMiB7XG4gICAgbWFyZ2luLXRvcDogMTByZW07XG4gIH1cbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CongresoThComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-congreso-th',
                templateUrl: './congreso-th.component.html',
                styleUrls: ['./congreso-th.component.scss']
            }]
    }], function () { return [{ type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"] }, { type: src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__["BlogService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/pages/blog/entradas/congreso-th/congreso-th.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/blog/entradas/congreso-th/congreso-th.module.ts ***!
  \***********************************************************************/
/*! exports provided: CongresoThModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CongresoThModule", function() { return CongresoThModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _congreso_th_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./congreso-th-routing.module */ "./src/app/pages/blog/entradas/congreso-th/congreso-th-routing.module.ts");
/* harmony import */ var _congreso_th_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./congreso-th.component */ "./src/app/pages/blog/entradas/congreso-th/congreso-th.component.ts");





class CongresoThModule {
}
CongresoThModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: CongresoThModule });
CongresoThModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function CongresoThModule_Factory(t) { return new (t || CongresoThModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _congreso_th_routing_module__WEBPACK_IMPORTED_MODULE_2__["CongresoThRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](CongresoThModule, { declarations: [_congreso_th_component__WEBPACK_IMPORTED_MODULE_3__["CongresoThComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _congreso_th_routing_module__WEBPACK_IMPORTED_MODULE_2__["CongresoThRoutingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CongresoThModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [_congreso_th_component__WEBPACK_IMPORTED_MODULE_3__["CongresoThComponent"]],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _congreso_th_routing_module__WEBPACK_IMPORTED_MODULE_2__["CongresoThRoutingModule"]
                ]
            }]
    }], null, null); })();


/***/ })

}]);
//# sourceMappingURL=pages-blog-entradas-congreso-th-congreso-th-module-es2015.js.map