(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-politicas-politicas-module"],{

/***/ "./src/app/pages/politicas/politicas-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/politicas/politicas-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: PoliticasRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PoliticasRoutingModule", function() { return PoliticasRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _politicas_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./politicas.component */ "./src/app/pages/politicas/politicas.component.ts");





const routes = [
    {
        path: '', component: _politicas_component__WEBPACK_IMPORTED_MODULE_2__["PoliticasComponent"]
    }
];
class PoliticasRoutingModule {
}
PoliticasRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: PoliticasRoutingModule });
PoliticasRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function PoliticasRoutingModule_Factory(t) { return new (t || PoliticasRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](PoliticasRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PoliticasRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/pages/politicas/politicas.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/politicas/politicas.component.ts ***!
  \********************************************************/
/*! exports provided: PoliticasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PoliticasComponent", function() { return PoliticasComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");



class PoliticasComponent {
    constructor(meta, title) {
        this.meta = meta;
        this.title = title;
    }
    ngOnInit() {
        // Para ñadir el título de la página
        this.title.setTitle('Colombian Outsourcing Solution - COS');
        // Añadir el tag de la info de la página
        this.meta.addTag({
            name: 'page.info',
            content: 'politicas-de-privacidad'
        });
    }
    ngOnDestroy() {
        // Quitar el tag "page.info" antes de ir a otra página, para que rastree correctamente la información
        this.meta.removeTag('name=\'page.info\'');
    }
}
PoliticasComponent.ɵfac = function PoliticasComponent_Factory(t) { return new (t || PoliticasComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"])); };
PoliticasComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PoliticasComponent, selectors: [["app-politicas"]], decls: 280, vars: 0, consts: [["id", "landing", 1, "bg"], ["src", "../../../assets/img/blog-img.png", "alt", "contact-center-BPO-COS-blog"], [1, "contenido"], [1, "text"], [1, "mt-5", "follow"], ["href", "https://www.facebook.com/Outsourcingcos/", "target", "blank"], [1, "fab", "fa-facebook-square", "ml-5"], ["href", "https://www.instagram.com/cosoutsourcing/", "target", "blank"], [1, "fab", "fa-instagram-square", "ml-3"], ["href", "https://www.linkedin.com/company/colombianoutsourcingcos/about/", "target", "blank"], [1, "fab", "fa-linkedin", "ml-3"], ["href", "https://www.youtube.com/channel/UCL89AlspoSj_exe6qidqHew", "target", "blank"], [1, "fab", "fa-youtube", "ml-3"], [1, "container", "politica"]], template: function PoliticasComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Soluciones tan \u00FAnicas como su negocio");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Con nosotros vas a ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "multiplicar ventas, mejorar la experiencia de usuario y tener ideas \u00FAnicas e innovadoras");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " para tu negocio.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "s\u00EDguenos en ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "i", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " Esta Pol\u00EDtica de Protecci\u00F3n de tratamiento y protecci\u00F3n de datos personales se aplicar\u00E1 a todas las Bases de Datos y/o Archivos que contengan Datos Personales que sean objeto de Tratamiento COS considerado como responsable y/o encargado del tratamiento de los datos personales. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, " 2.\tResponsable ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "\nLa empresa Colombian Outsourcing Solutions es una persona jur\u00EDdica en derecho privado, docimicilaria principalmente en la carrera 43 # 17- 47 identificada con Nit 900292245-4 Tel\u00E9fono : 4863290 Mail: protecciondedatos@cos.com. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, " 3.\tDefiniciones ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "\n\u2022\tAutorizaci\u00F3n: Consentimiento previo, expreso e informado del Titular para llevar a cabo el Tratamiento de Datos Personales. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "\n\u2022\tAviso de Privacidad: Comunicaci\u00F3n verbal o escrita generada por el Responsable, dirigida al Titular para el Tratamiento de sus Datos Personales, mediante la cual se le informa acerca de la existencia de las Pol\u00EDticas de Tratamiento de informaci\u00F3n que le ser\u00E1n aplicables, la forma de acceder a las mismas y las finalidades del Tratamiento que se pretende dar a los datos personales.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "\n\u2022\tBase de Datos: Conjunto organizado de Datos Personales que sea objeto de Tratamiento.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "\n\u2022\tClientes: Persona natural o jur\u00EDdica, p\u00FAblica o privada con los cuales la empresa tiene una relaci\u00F3n comercial. Comprende las tiendas, supermercados, mini mercados, entre otros.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "\n\u2022\tConsumidores: Persona natural que consume los bienes producidos por la empresa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "\n\u2022\tDato Personal: Cualquier informaci\u00F3n vinculada o que pueda asociarse a una o varias personas naturales determinadas o determinables. Son algunos ejemplos de datos personales los siguientes: nombre, c\u00E9dula de ciudadan\u00EDa, direcci\u00F3n, correo electr\u00F3nico, n\u00FAmero telef\u00F3nico, estado civil, datos de salud, huella dactilar, salario, bienes, estados financieros, etc ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "\n\u2022\tDato sensible: Informaci\u00F3n que afecta la intimidad del Titular o cuyo uso indebido puede generar su discriminaci\u00F3n, tales como aquellos que revelen el origen racial o \u00E9tnico, la orientaci\u00F3n pol\u00EDtica, las convicciones religiosas o filos\u00F3ficas, la pertenencia a sindicatos, organizaciones sociales, de derechos humanos o que promueva intereses de cualquier partido pol\u00EDtico o que garanticen los derechos y garant\u00EDas de partidos pol\u00EDticos de oposici\u00F3n as\u00ED como los datos relativos a la salud, a la vida sexual y los datos biom\u00E9tricos, entre otros, la captura de imagen fija o en movimiento, huellas digitales, fotograf\u00EDas, iris, reconocimiento de voz, facial o de palma de mano, etc.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "\n\u2022\tEncargado del Tratamiento: Persona natural o jur\u00EDdica, p\u00FAblica o privada, que por s\u00ED misma o en asocio con otros, realice el Tratamiento de Datos Personales por cuenta del Responsable del Tratamiento. En los eventos en que el Responsable no ejerza como Encargado de la Base de Datos, se identificar\u00E1 expresamente qui\u00E9n ser\u00E1 el Encargado.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "\n\u2022\tResponsable del Tratamiento: Persona natural o jur\u00EDdica, p\u00FAblica o privada, que por s\u00ED misma o en asocio con otros, decida sobre la Base de Datos y/o el Tratamiento de los datos.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "\n\u2022\tReclamo: Solicitud del Titular del dato o de las personas autorizadas por \u00E9ste o por la Ley para corregir, actualizar o suprimir sus datos personales o para revocar la autorizaci\u00F3n en los casos establecidos en la Ley.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "\n\u2022\tT\u00E9rminos y Condiciones: marco general en el cual se establecen las condiciones para los participantes de actividades promocionales o afines.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "\n\u2022\tTitular: Persona natural cuyos Datos Personales sean objeto de Tratamiento.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "\n\u2022\tTransferencia: La transferencia de datos tiene lugar cuando el Responsable y/o Encargado del Tratamiento de datos personales, ubicado en Colombia, env\u00EDa la informaci\u00F3n o los datos personales a un receptor, que a su vez es Responsable del Tratamiento y se encuentra dentro o fuera del pa\u00EDs.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](61, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "\n\u2022\tTransmisi\u00F3n: Tratamiento de Datos Personales que implica la comunicaci\u00F3n de los mismos dentro o fuera del territorio de la Rep\u00FAblica de Colombia cuando tenga por objeto la realizaci\u00F3n de un Tratamiento por el Encargado por cuenta del Responsable.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "\n\u2022\tTratamiento: Cualquier operaci\u00F3n o conjunto de operaciones sobre Datos Personales, tales como la recolecci\u00F3n, almacenamiento, uso, circulaci\u00F3n o supresi\u00F3n.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](66, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, " 4.\tPrincipios");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, " Las pol\u00EDticas que se adoptan est\u00E1n reguladas ante todo por los principios de la administraci\u00F3n de datos, los cuales constituyen el marco general que ser\u00E1 respetado por Superintendencia Financiera de Colombia en los procesos de acopio, uso y tratamiento de datos de los titulares de la informaci\u00F3n. Los principios rectores son los se\u00F1alados en el art\u00EDculo 4 de la Ley 1581 de 2018, a saber:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](72, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "\u2022\tPrincipio de finalidad.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, " La actividad del tratamiento de datos personales que realice COS o a la cual tuviere acceso, obedecer\u00E1n a una finalidad leg\u00EDtima en consonancia con la Constituci\u00F3n Pol\u00EDtica de Colombia, la cual deber\u00E1 ser informada al respectivo titular de los datos personales.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, " \u2022\tPrincipio de legalidad.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](80, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, " El tratamiento de datos es una actividad reglada, la cual deber\u00E1 estar sujeta a las disposiciones legales vigentes y aplicables rigen el tema");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](82, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, " \u2022\tPrincipio de libertad. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](85, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, " el tratamiento de los datos personales s\u00F3lo puede realizarse con el consentimiento, previo, expreso e informado del Titular. Los datos personales no podr\u00E1n ser obtenidos o divulgados sin previa autorizaci\u00F3n, o en ausencia de mandato legal, estatutario, o judicial que releve el consentimiento. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](87, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, " \u2022\tPrincipio de veracidad o calidad.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](90, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, " la informaci\u00F3n sujeta a Tratamiento de datos personales debe ser veraz, completa, exacta, actualizada, comprobable y comprensible. Se proh\u00EDbe el Tratamiento de datos parciales, incompletos, fraccionados o que induzcan a error. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](92, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, " \u2022\tPrincipio de transparencia.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](95, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "\nEn el tratamiento de datos personales, COS garantizar\u00E1 al Titular su derecho de obtener en cualquier momento y sin restricciones, informaci\u00F3n acerca de la existencia de cualquier tipo de informaci\u00F3n o dato personal que sea de su inter\u00E9s o titularidad. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](99, " \u2022\tPrincipio de acceso y circulaci\u00F3n restringida.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, "\nEl tratamiento de datos personales se sujeta a los l\u00EDmites que se derivan de la naturaleza de \u00E9stos, de las disposiciones de la ley y la Constituci\u00F3n. En consecuencia, el tratamiento s\u00F3lo podr\u00E1 hacerse por personas autorizadas por el titular y/o por las personas previstas en la ley. Los datos personales, salvo la informaci\u00F3n p\u00FAblica, no podr\u00E1n estar disponibles en internet u otros medios de divulgaci\u00F3n o comunicaci\u00F3n masiva, salvo que el acceso sea t\u00E9cnicamente controlable para brindar un conocimiento restringido s\u00F3lo a los titulares o terceros autorizados conforme a la ley. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](102, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "\u2022\tPrincipio de seguridad.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, "\nla informaci\u00F3n sujeta a tratamiento por COS, se deber\u00E1 manejar con las medidas t\u00E9cnicas, humanas y administrativas que sean necesarias para otorgar seguridad a los registros evitando su adulteraci\u00F3n, p\u00E9rdida, consulta, uso o acceso no autorizado o fraudulento. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "\u2022 Principio de confidencialidad.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](110, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "\nTodas las personas que en COS, administren, manejen, actualicen o tengan acceso a informaciones de cualquier tipo que se encuentre en Bases de Datos, est\u00E1n obligadas a garantizar la reserva de la informaci\u00F3n, por lo que se comprometen a conservar y mantener de manera estrictamente confidencial y no revelar a terceros, toda la informaci\u00F3n que llegaren a conocer en la ejecuci\u00F3n y ejercicio de sus funciones; salvo cuando se trate de actividades autorizadas expresamente por la ley de protecci\u00F3n de datos. Esta obligaci\u00F3n persiste y se mantendr\u00E1 inclusive despu\u00E9s de finalizada su relaci\u00F3n con alguna de las labores que comprende el Tratamiento. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](112, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, " 5.\tAutorizaci\u00F3n.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](117, "\nSin perjuicio de las excepciones previstas en la Ley, en el tratamiento de datos personales del titular se requiere la autorizaci\u00F3n previa e informada de \u00E9ste, la cual deber\u00E1 ser obtenida por cualquier medio que pueda ser objeto de posterior consulta. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](118, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](119, "\nLa autorizaci\u00F3n del titular de la informaci\u00F3n no ser\u00E1 necesaria en los siguientes casos: a) Cuando la informaci\u00F3n sea requerida por una entidad p\u00FAblica o administrativa en ejercicio de sus funciones legales o por orden judicial. b) Cuando los datos sean de naturaleza p\u00FAblica. c) Cuando se presente un caso de urgencia m\u00E9dica o sanitaria. d) Cuando el tratamiento de informaci\u00F3n se encuentre autorizado por la ley. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](120, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](121, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](123, " 6.\tTratamientoyfinalidad");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](124, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "\nEl tratamiento de los datos personales de los empleados, candidatos, proveedores, ex empleados, proveedores, contratistas, o de cualquier persona con la cual COS tuviere establecida o estableciera una relaci\u00F3n, permanente u ocasional, lo realizar\u00E1 en el marco legal que regula la materia y en virtud de su objeto social, como ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](126, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, "\nCOLOMBIAN OUTSOURCING SOLUTIONS S.A.S y ser\u00E1n todos los necesarios para el cumplimiento de la misi\u00F3n de la compa\u00F1\u00EDa. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](128, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](129, "\nEl tratamiento de los datos personales se podr\u00E1 realizar a trav\u00E9s de medios f\u00EDsicos, automatizados o digitales de acuerdo con el tipo y forma de recolecci\u00F3n de la informaci\u00F3n personal. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](130, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](131, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](132, "\nEn todo caso, los datos personales podr\u00E1n ser recolectados y tratados para: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](133, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](134, "\no Cumplir todos sus compromisos contractuales. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](135, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](136, "\no Cumplir las normas aplicables a proveedores y contratistas, incluyendo,pero sin limitarse a las tributarias y comerciales. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](137, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](138, "\no Controlar el acceso a las instalaciones de COS y establecer medidas de seguridad, incluyendo el establecimiento de zonas video-vigiladas. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](139, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, "\no Controlar las gestiones que se realizan desde las l\u00EDneas telef\u00F3nicas de COS, en las oficinas y en las campa\u00F1as. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](141, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](142, "\no Permitir que compa\u00F1\u00EDas vinculadas a COS, con las cuales ha celebrado contratos que incluyen disposiciones. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](143, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](144, "\no Generar los debidos procesos de verificaci\u00F3n datos y capacitaci\u00F3n. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](145, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "\no Dar respuesta a consultas, peticiones, quejas y reclamos que sean realizadas por los Titulares y organismos de control y trasmitir los Datos Personales a las dem\u00E1s autoridades que en virtud de la ley aplicable deban recibir los Datos Personales. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](147, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](148, "\no Cualquier otra actividad de naturaleza similar a las anteriormente descritas que sean necesarias para desarrollar el objeto social de COS. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](149, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](150, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](152, " 7.\tDatos sensibles");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](153, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](154, "\nEl Titular tiene derecho a optar por no suministrar cualquier informaci\u00F3n sensible solicitada por COS relacionada, entre otros, con datos sobre su origen racial o \u00E9tnico, la pertenencia a sindicatos, organizaciones sociales o de derechos humanos, convicciones pol\u00EDticas, religiosas, de la vida sexual, o historial de salud, salvo que se requieran para el ejercicio de funciones legales o cuando exista orden de autoridad administrativa o judicial. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](155, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](156, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](157, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](158, " 8.\tDerechosdelostitulares.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](159, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](160, "\no De conformidad con lo previsto en el art\u00EDculo 8 de la ley 1581 de 2012, los titulares podr\u00E1n: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](161, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](162, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](163, "\no Conocer, actualizar y rectificar sus datos personales frente a COS o a los encargados. Este derecho se podr\u00E1 ejercer, entre otros frente a datos parciales, inexactos, incompletos, fraccionados, que induzcan a error, o aquellos cuyo Tratamiento est\u00E9 expresamente prohibido o no haya sido autorizado. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](164, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](165, "\no Solicitar prueba de la autorizaci\u00F3n otorgada a la COS, salvo cuando expresamente se except\u00FAe como requisito para el Tratamiento, de conformidad con lo previsto en el art\u00EDculo 10 de la presente ley. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](166, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](167, "\no Ser informado por COS o elencargado, previasolicitud, respecto del uso que les ha dado a sus datos personales. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](168, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](169, "\no Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo dispuesto en la presente ley y las dem\u00E1s normas que la modifiquen, adicionen o complementen. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](170, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](171, "\no Revocar la autorizaci\u00F3n y/o solicitar la supresi\u00F3n del dato cuando en el Tratamiento no se respeten los principios, derechos y garant\u00EDas constitucionales y legales. La revocatoria y/o supresi\u00F3n proceder\u00E1 cuando la Superintendencia de Industria y Comercio haya determinado que COS o el encargado ha incurrido en conductas contrarias a esta ley y a la Constituci\u00F3n. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](172, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](173, "\no Acceder en forma gratuita a sus datos personales que hayan sido objeto de Tratamiento. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](174, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](175, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](177, " 9.\tObligaciones de la compa\u00F1\u00EDa.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](178, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](179, "\no Cumplir las instrucciones y requerimientos que imparta la Superintendencia de Industria y Comercio. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](180, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](181, "\no Informar a la autoridad de protecci\u00F3n de datos cuando se presenten violaciones a los c\u00F3digos de seguridad y existan riesgos en la administraci\u00F3n de la informaci\u00F3n de los Titulares. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](182, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](183, "\no Informar a solicitud del Titular sobre el uso dado a sus datos.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](184, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](185, "\no Informar al encargado cuando determinada informaci\u00F3n se encuentra en discusi\u00F3n por parte del Titular, una vez se haya presentado la reclamaci\u00F3n y no haya finalizado el tr\u00E1mite respectivo.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](186, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](187, "\no Informar debidamente al Titular sobre la finalidad de la recolecci\u00F3n y los derechos que le asisten por virtud de la autorizaci\u00F3n otorgada. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](188, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](189, "\no Adoptar un manual interno de pol\u00EDticas y procedimientos para garantizar el adecuado cumplimiento de la presente ley y en especial, para la atenci\u00F3n de consultas y reclamos.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](190, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](191, "\no Tramitar las consultas y reclamos formulados en los t\u00E9rminos se\u00F1alados en la presente ley.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](192, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](193, "\no Exigir al encargado en todo momento, el respeto a las condiciones de seguridad y privacidad de la informaci\u00F3n del Titular.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](194, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](195, "\no Garantizar que la informaci\u00F3n que se suministre al Encargado sea veraz, completa, exacta, actualizada, comprobable y comprensible.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](196, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](197, "\no Conservar la informaci\u00F3n bajo las condiciones de seguridad necesarias para impedir su adulteraci\u00F3n, p\u00E9rdida, consulta, uso o acceso no autorizado o fraudulento.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](198, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](199, "\no Suministrar al responsable, seg\u00FAn el caso, \u00FAnicamente datos cuyo Tratamiento est\u00E9 previamente autorizado de conformidad con lo previsto en la presente ley.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](200, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](201, "\no Rectificar la informaci\u00F3n cuando sea incorrecta y comunicar lo pertinente al Encargado. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](202, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](203, "\no Actualizar la informaci\u00F3n, comunicando de forma oportuna al Encargado del Tratamiento, todas las novedades respecto de los datos que previamente le haya.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](204, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](205, "\no suministrado y adoptar las dem\u00E1s medidas necesarias para que la informaci\u00F3n prestada a este se mantenga actualizada.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](206, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](207, "\no Solicitar y conservar, en las condiciones previstas en la presente ley, copia de la respectiva autorizaci\u00F3n otorgada por el Titular.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](208, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](209, "\no Garantizar al Titular, en todo tiempo, el pleno y efectivo ejercicio del derecho de h\u00E1beas data.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](210, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](211, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](212, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](213, "10.Derechos de los ni\u00F1os y adolescentes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](214, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](215, "\nEn el tratamiento de datos personales se asegurar\u00E1 el respeto a los derechos prevalentes de los menores. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](216, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](217, "\nQueda proscrito el tratamiento de datos personales de menores, salvo aquellos datos que sean de naturaleza p\u00FAblica, y en este caso el tratamiento deber\u00E1 cumplir con los siguientes par\u00E1metros: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](218, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](219, "\n\u201CLey 1581 de 2012. Art\u00EDculo 10. Casos en que no es necesaria la autorizaci\u00F3n. La autorizaci\u00F3n del Titular no ser\u00E1 necesaria cuando se trate de: a) Informaci\u00F3n requerida por una entidad p\u00FAblica o administrativa en ejercicio de sus funciones legales o por orden judicial; ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](220, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](221, " b) Datos de naturaleza p\u00FAblica; ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](222, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](223, " c) Casos de urgencia m\u00E9dica o sanitaria; ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](224, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](225, " d) Tratamiento de informaci\u00F3n autorizado por la ley para fines hist\u00F3ricos, estad\u00EDsticos o cient\u00EDficos; ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](226, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](227, " e) Datos relacionados con el Registro Civil de las Personas. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](228, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](229, " Quien acceda a los datos personales sin que medie autorizaci\u00F3n previa deber\u00E1 en todo caso cumplir con las disposiciones contenidas en la presente ley\u201D");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](230, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](231, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](232, "\n1.\tResponder y respetar el inter\u00E9s superior de los menores ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](233, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](234, "\nb. Asegurar el respeto de los derechos fundamentales de los menores. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](235, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](236, "\n11.Solicitudes y consultas. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](237, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](238, "\nLas solicitudes, consultas, peticiones, quejas o reclamos deben ser escaladas al mail protecciondedatos@cos.com., las cuales deben contener m\u00EDnimo la siguiente informaci\u00F3n: n\u00FAmero de identificaci\u00F3n, descripci\u00F3n clara en solicitud en el asunto del correo , rese\u00F1a de los hechos, direcci\u00F3n de envi\u00F3 de correspondencia, anexos como evidencias que requiera. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](239, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](240, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](241, "\n12.\tRespuesta de solicitudes y consultas. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](242, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](243, "\nLa consulta de peticiones, quejas, reclamos, correcci\u00F3n, actualizaci\u00F3n o supresi\u00F3n de los datos deber\u00E1n presentarse por escrito o por correo electr\u00F3nico, de acuerdo a la informaci\u00F3n contenida en este documento. atreves de mail protecciondedatos@cos.com. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](244, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](245, "\nLas respuestas ser\u00E1n atendidas en un t\u00E9rmino de diez (10) d\u00EDas h\u00E1biles contados a partir de la fecha del recibo de la respectiva solicitud escalada al mail anteriormente relacionado. Cuando no fuere posible atender la consulta dentro de dicho t\u00E9rmino, se informar\u00E1 al interesado, expresando los motivos de la demora y se\u00F1alando la fecha en que se atender\u00E1 su consulta, la cual en ning\u00FAn caso podr\u00E1 superar los cinco (5) d\u00EDas h\u00E1biles siguientes al vencimiento del primer t\u00E9rmino. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](246, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](247, "\nEn caso de requerirse m\u00E1s informaci\u00F3n dentro de los dos (2) siguientes d\u00EDas h\u00E1biles se responder\u00E1 al remitente el recibido del mail o si es necesario obtener m\u00E1s datos respecto a la solicitud escalada. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](248, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](249, "\nSi el requerimiento continuo incompleto, se requerir\u00E1 al interesado dentro de los cinco (5) d\u00EDas siguientes a la recepci\u00F3n del reclamo para que subsane las fallas. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](250, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](251, "\nTranscurridos dos (2) meses desde la fecha del requerimiento, sin que el solicitante presente la informaci\u00F3n requerida, se entender\u00E1 que ha desistido del reclamo. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](252, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](253, " Una vez recibido el requerimiento completo, se incluir\u00E1 en la base de datos una leyenda que diga \u201Cgesti\u00F3n en tr\u00E1mite\u201D y el motivo del mismo, en un t\u00E9rmino no mayor a dos (2) d\u00EDas h\u00E1biles. Dicha leyenda deber\u00E1 mantenerse hasta que el reclamo sea respondido. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](254, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](255, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](256, "\n13.Informaci\u00F3n de chat COS ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](257, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](258, "\nColombian Outsourcing Solutions S.A.S informa que los datos personales que pudieran llegar a ser suministrados por el usuario en su nombre de usuario y en el contenido de las conversaciones, ser\u00E1n tratados \u00FAnica y exclusivamente para la gesti\u00F3n y correcto funcionamiento del chat, a trav\u00E9s de un tercero encargado del desarrollo de la plataforma del chat, pudiendo este almacenar la informaci\u00F3n temporalmente. Para el ejercicio de los derechos de habeas data el titular o representante legal puede dirigirse al correo protecciondedatos@cos.com. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](259, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](260, "\nEl usuario es el \u00FAnico responsable por la divulgaci\u00F3n que realice del acceso al chat y por el contenido e informaci\u00F3n que comparta en este. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](261, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](262, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](263, "\n14.P\u00E1gina Web ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](264, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](265, "\nAl aceptar los t\u00E9rminos y condiciones legales, indico que conozco y autorizo de manera previa, expresa e informada a Colombian Outsourcing Solutions S.A.S., sus filiales y vinculadas, para que mis datos personales puedan ser almacenados y usados con el fin de lograr una eficiente comunicaci\u00F3n durante el presente tr\u00E1mite\no actividad y autorizo en los mismos t\u00E9rminos, que dicha informaci\u00F3n pueda ser tratada conforme a lo dispuesto en la Ley 1581 de 2011 y sus Decretos Reglamentarios, con el fin de recibir informaci\u00F3n acerca de sus productos, servicios, ofertas, vacantes, promociones, alianzas, estudios, concursos, contenidos. As\u00ED mismo he sido informado acerca de la pol\u00EDtica para la protecci\u00F3n de datos personales disponible en este sitio, en la cual se incluyen los procedimientos de consulta y reclamaci\u00F3n que me permiten hacer efectivos tus derechos al acceso, conocimiento, consulta, rectificaci\u00F3n, actualizaci\u00F3n, y supresi\u00F3n de los datos, e igualmente podr\u00E1s presentar cualquier solicitud referida a tus datos personales a trav\u00E9s del correo protecciondedatos@cos.com.\nLas bases de datos administradas por la COS se mantendr\u00E1n indefinidamente, mientras desarrolle su objeto social, y mientras sea necesario para asegurar el cumplimiento de obligaciones de car\u00E1cter legal, particularmente laboral y contable, pero los datos podr\u00E1n ser eliminados en cualquier momento a solicitud de su titular, en tanto esta solicitud no contrar\u00EDe una obligaci\u00F3n legal de la COS o una obligaci\u00F3n contenida en un contrato entre la COS y Titular. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](266, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](267, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](268, "\n15.\tAnexos ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](269, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](270, "\n\u2022 Adendo de capacitaci\u00F3n. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](271, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](272, "\n\u2022 Tratamiento de datos proveedores. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](273, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](274, "\n\u2022 Tratamiento de datos visitantes. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](275, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](276, "\n\u2022 Tratamiento de datos empleados. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](277, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](278, "\n\u2022 Tratamiento de datos candidatos. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](279, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["*[_ngcontent-%COMP%] {\n  font-family: \"open-sans\", sans-serif;\n}\n\ni[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Free\";\n}\n\n.bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n  position: absolute;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n  position: relative;\n  padding: 12rem 12rem 0;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  font-weight: 800;\n  color: #fff;\n  font-size: 4rem;\n  width: 500px;\n  margin-bottom: 2rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: white;\n  width: 445px;\n  margin-bottom: 2rem;\n  font-size: 1.2rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: white;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  font-size: 1.3rem;\n}\n\n@media (min-width: 800px) and (max-width: 1024px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -200px;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -456px;\n  }\n}\n\n@media (min-width: 356px) and (max-width: 425px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 944px;\n    height: auto;\n    left: -519px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n    padding: 10rem 1rem 0;\n    text-align: center;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2rem;\n    width: 393px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 393px;\n  }\n}\n\n@media (min-width: 356px) and (max-width: 1450px) {\n  .politica[_ngcontent-%COMP%] {\n    margin-top: 12rem !important;\n  }\n}\n\n.politica[_ngcontent-%COMP%] {\n  margin-top: 20rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcG9saXRpY2FzL0Q6XFxwcm95ZWN0b3NcXHdlYnNpdGUtY29zL3NyY1xcYXBwXFxwYWdlc1xccG9saXRpY2FzXFxwb2xpdGljYXMuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3BvbGl0aWNhcy9wb2xpdGljYXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQ0FBQTtBQ0NKOztBREVBO0VBQ0ksa0NBQUE7QUNDSjs7QURJSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNEUjs7QURHSTtFQUNJLGtCQUFBO0VBQ0Esc0JBQUE7QUNEUjs7QURFUTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUNBWjs7QURFUTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQ0FaOztBREVRO0VBQ0ksaUJBQUE7QUNBWjs7QURDWTtFQUNJLG9DQUFBO0FDQ2hCOztBRENZO0VBQ0ksWUFBQTtBQ0NoQjs7QURBZ0I7RUFDSSxpQkFBQTtBQ0VwQjs7QURLQTtFQUVRO0lBQ0ksYUFBQTtJQUNBLFlBQUE7RUNIVjtBQUNGOztBRE9BO0VBRVE7SUFDSSxhQUFBO0lBQ0EsWUFBQTtFQ05WO0FBQ0Y7O0FEVUE7RUFFUTtJQUNJLFlBQUE7SUFDQSxZQUFBO0lBQ0EsWUFBQTtFQ1RWO0VEV007SUFDSSxxQkFBQTtJQUNBLGtCQUFBO0VDVFY7RURVVTtJQUNJLGVBQUE7SUFDQSxZQUFBO0VDUmQ7RURVVTtJQUNJLGVBQUE7SUFDQSxZQUFBO0VDUmQ7QUFDRjs7QURhQTtFQUNJO0lBQ0ksNEJBQUE7RUNYTjtBQUNGOztBRGNBO0VBQ0ksaUJBQUE7QUNaSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BvbGl0aWNhcy9wb2xpdGljYXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqe1xyXG4gICAgZm9udC1mYW1pbHk6ICdvcGVuLXNhbnMnLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG5pe1xyXG4gICAgZm9udC1mYW1pbHk6ICdGb250IEF3ZXNvbWUgNSBGcmVlJztcclxufVxyXG5cclxuLy8gQmFja2dyb3VuZyBjbGFzc1xyXG4uYmd7XHJcbiAgICBpbWd7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTsgICBcclxuICAgIH1cclxuICAgIC5jb250ZW5pZG97XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHBhZGRpbmc6IDEycmVtIDEycmVtIDA7XHJcbiAgICAgICAgaDF7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDRyZW07XHJcbiAgICAgICAgICAgIHdpZHRoOiA1MDBweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcHtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICB3aWR0aDogNDQ1cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDJyZW07XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuZm9sbG93e1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnRm9udCBBd2Vzb21lIDUgQnJhbmRzJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBhe1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgJjpob3ZlcntcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEuM3JlbTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6ODAwcHgpYW5kKG1heC13aWR0aDoxMDI0cHgpe1xyXG4gICAgLmJne1xyXG4gICAgICAgIGltZ3tcclxuICAgICAgICAgICAgd2lkdGg6IDEyMjRweDtcclxuICAgICAgICAgICAgbGVmdDogLTIwMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6NTYwcHgpYW5kKG1heC13aWR0aDo3NjhweCl7XHJcbiAgICAuYmd7XHJcbiAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICB3aWR0aDogMTIyNHB4O1xyXG4gICAgICAgICAgICBsZWZ0OiAtNDU2cHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogMzU2cHgpYW5kKG1heC13aWR0aDo0MjVweCl7XHJcbiAgICAuYmd7XHJcbiAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICB3aWR0aDogOTQ0cHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAgICAgbGVmdDogLTUxOXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY29udGVuaWRve1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHJlbSAxcmVtIDA7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgaDF7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDJyZW07XHJcbiAgICAgICAgICAgICAgICB3aWR0aDozOTNweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBwe1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDM5M3B4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogMzU2cHgpYW5kKG1heC13aWR0aDogMTQ1MHB4KXtcclxuICAgIC5wb2xpdGljYXtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMnJlbSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcblxyXG4ucG9saXRpY2F7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHJlbTtcclxufSIsIioge1xuICBmb250LWZhbWlseTogXCJvcGVuLXNhbnNcIiwgc2Fucy1zZXJpZjtcbn1cblxuaSB7XG4gIGZvbnQtZmFtaWx5OiBcIkZvbnQgQXdlc29tZSA1IEZyZWVcIjtcbn1cblxuLmJnIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi5iZyAuY29udGVuaWRvIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBwYWRkaW5nOiAxMnJlbSAxMnJlbSAwO1xufVxuLmJnIC5jb250ZW5pZG8gaDEge1xuICBmb250LXdlaWdodDogODAwO1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1zaXplOiA0cmVtO1xuICB3aWR0aDogNTAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG59XG4uYmcgLmNvbnRlbmlkbyBwIHtcbiAgY29sb3I6IHdoaXRlO1xuICB3aWR0aDogNDQ1cHg7XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xufVxuLmJnIC5jb250ZW5pZG8gLmZvbGxvdyB7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xufVxuLmJnIC5jb250ZW5pZG8gLmZvbGxvdyBpIHtcbiAgZm9udC1mYW1pbHk6IFwiRm9udCBBd2Vzb21lIDUgQnJhbmRzXCI7XG59XG4uYmcgLmNvbnRlbmlkbyAuZm9sbG93IGEge1xuICBjb2xvcjogd2hpdGU7XG59XG4uYmcgLmNvbnRlbmlkbyAuZm9sbG93IGE6aG92ZXIge1xuICBmb250LXNpemU6IDEuM3JlbTtcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDgwMHB4KSBhbmQgKG1heC13aWR0aDogMTAyNHB4KSB7XG4gIC5iZyBpbWcge1xuICAgIHdpZHRoOiAxMjI0cHg7XG4gICAgbGVmdDogLTIwMHB4O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNTYwcHgpIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAuYmcgaW1nIHtcbiAgICB3aWR0aDogMTIyNHB4O1xuICAgIGxlZnQ6IC00NTZweDtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDM1NnB4KSBhbmQgKG1heC13aWR0aDogNDI1cHgpIHtcbiAgLmJnIGltZyB7XG4gICAgd2lkdGg6IDk0NHB4O1xuICAgIGhlaWdodDogYXV0bztcbiAgICBsZWZ0OiAtNTE5cHg7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8ge1xuICAgIHBhZGRpbmc6IDEwcmVtIDFyZW0gMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8gaDEge1xuICAgIGZvbnQtc2l6ZTogMnJlbTtcbiAgICB3aWR0aDogMzkzcHg7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8gcCB7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIHdpZHRoOiAzOTNweDtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDM1NnB4KSBhbmQgKG1heC13aWR0aDogMTQ1MHB4KSB7XG4gIC5wb2xpdGljYSB7XG4gICAgbWFyZ2luLXRvcDogMTJyZW0gIWltcG9ydGFudDtcbiAgfVxufVxuLnBvbGl0aWNhIHtcbiAgbWFyZ2luLXRvcDogMjByZW07XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PoliticasComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-politicas',
                templateUrl: './politicas.component.html',
                styleUrls: ['./politicas.component.scss']
            }]
    }], function () { return [{ type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"] }]; }, null); })();


/***/ }),

/***/ "./src/app/pages/politicas/politicas.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/politicas/politicas.module.ts ***!
  \*****************************************************/
/*! exports provided: PoliticasModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PoliticasModule", function() { return PoliticasModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _politicas_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./politicas-routing.module */ "./src/app/pages/politicas/politicas-routing.module.ts");
/* harmony import */ var _politicas_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./politicas.component */ "./src/app/pages/politicas/politicas.component.ts");





class PoliticasModule {
}
PoliticasModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: PoliticasModule });
PoliticasModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function PoliticasModule_Factory(t) { return new (t || PoliticasModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _politicas_routing_module__WEBPACK_IMPORTED_MODULE_2__["PoliticasRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](PoliticasModule, { declarations: [_politicas_component__WEBPACK_IMPORTED_MODULE_3__["PoliticasComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _politicas_routing_module__WEBPACK_IMPORTED_MODULE_2__["PoliticasRoutingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PoliticasModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [_politicas_component__WEBPACK_IMPORTED_MODULE_3__["PoliticasComponent"]],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _politicas_routing_module__WEBPACK_IMPORTED_MODULE_2__["PoliticasRoutingModule"]
                ]
            }]
    }], null, null); })();


/***/ })

}]);
//# sourceMappingURL=pages-politicas-politicas-module-es2015.js.map