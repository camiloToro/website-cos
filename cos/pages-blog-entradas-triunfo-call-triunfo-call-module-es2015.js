(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-blog-entradas-triunfo-call-triunfo-call-module"],{

/***/ "./src/app/pages/blog/entradas/triunfo-call/triunfo-call-routing.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/blog/entradas/triunfo-call/triunfo-call-routing.module.ts ***!
  \*********************************************************************************/
/*! exports provided: TriunfoCallRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TriunfoCallRoutingModule", function() { return TriunfoCallRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _triunfo_call_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./triunfo-call.component */ "./src/app/pages/blog/entradas/triunfo-call/triunfo-call.component.ts");





const routes = [
    {
        path: '', component: _triunfo_call_component__WEBPACK_IMPORTED_MODULE_2__["TriunfoCallComponent"]
    }
];
class TriunfoCallRoutingModule {
}
TriunfoCallRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: TriunfoCallRoutingModule });
TriunfoCallRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function TriunfoCallRoutingModule_Factory(t) { return new (t || TriunfoCallRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](TriunfoCallRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TriunfoCallRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/pages/blog/entradas/triunfo-call/triunfo-call.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/blog/entradas/triunfo-call/triunfo-call.component.ts ***!
  \****************************************************************************/
/*! exports provided: TriunfoCallComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TriunfoCallComponent", function() { return TriunfoCallComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/servicios/blog.service */ "./src/app/servicios/blog.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





const _c0 = function () { return ["/blog"]; };
class TriunfoCallComponent {
    constructor(meta, title, _blogService) {
        this.meta = meta;
        this.title = title;
        this._blogService = _blogService;
        this.entrada = [];
    }
    ngOnInit() {
        // usar el array del servicio blog
        this.entrada = this._blogService.entrada;
        // Para ñadir el título de la página
        this.title.setTitle('Colombian Outsourcing Solution - COS');
        // Añadir el tag de la info de la página
        this.meta.addTag({
            name: 'page.info',
            content: '¿POR QUÉ TRIUNFA EL CALL CENTER EN COLOMBIA?'
        });
    }
    ngOnDestroy() {
        // Quitar el tag "page.info" antes de ir a otra página, para que rastree correctamente la información
        this.meta.removeTag('name=\'page.info\'');
    }
}
TriunfoCallComponent.ɵfac = function TriunfoCallComponent_Factory(t) { return new (t || TriunfoCallComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__["BlogService"])); };
TriunfoCallComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: TriunfoCallComponent, selectors: [["app-triunfo-call"]], decls: 67, vars: 7, consts: [["id", "landing", 1, "bg"], ["alt", "contact-center-BPO-COS-blog", 3, "src"], [1, "contenido"], [1, "text"], [1, "mt-5", "follow"], ["href", "https://www.facebook.com/Outsourcingcos/", "target", "blank"], [1, "fab", "fa-facebook-square", "ml-5"], ["href", "https://www.instagram.com/cosoutsourcing/", "target", "blank"], [1, "fab", "fa-instagram-square", "ml-3"], ["href", "https://www.linkedin.com/company/colombianoutsourcingcos/about/", "target", "blank"], [1, "fab", "fa-linkedin", "ml-3"], ["href", "https://www.youtube.com/channel/UCL89AlspoSj_exe6qidqHew", "target", "blank"], [1, "fab", "fa-youtube", "ml-3"], [1, "container"], [1, "fab", "fa-facebook-square", "ml-3"], [1, "tag-blog"], [3, "routerLink"], [1, "fas", "fa-tag"], [1, "comment"], [1, "form-group"], [1, "form-group", "nombre"], ["type", "text", "placeholder", "Ejem: Carlos, Maria, etc ...", 1, "form-control"], [1, "form-group", "correo"], ["type", "text", "placeholder", "Ejem: email@email.com ...", 1, "form-control"], [1, "form-group", "web"], ["type", "text", "placeholder", "Ejem: www.mysitioweb.com ...", 1, "form-control"], [1, "form-group", "comentario"], ["name", "", "id", "", "cols", "30", "rows", "10", "placeholder", "D\u00E9janos tu comentario", 1, "form-control"], [1, "btn", "btn-primary", "btn-block", "mt-10"]], template: function TriunfoCallComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "s\u00EDguenos en ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "i", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "s\u00EDguenos en ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "i", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "a", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "i", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " Noticias ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, " Deja tu comentario ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, " Tu Correo no ser\u00E1 publicado. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Los campos marcados con (*) con obligatorios");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Nombre *");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "input", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "correo *");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "input", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Web");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "input", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Comentario *");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "textarea", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "button", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, " Enviar Comentario ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.entrada[0].img, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[9].title);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.entrada[9].title, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[9].fecha);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[9].text);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](6, _c0));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"]], styles: ["*[_ngcontent-%COMP%] {\n  font-family: \"open-sans\", sans-serif;\n}\n\ni[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Free\";\n}\n\n[_ngcontent-%COMP%]::-moz-placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n[_ngcontent-%COMP%]::-ms-input-placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n[_ngcontent-%COMP%]::placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n.bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n  position: absolute;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n  position: relative;\n  padding: 16rem 12rem 0;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  font-weight: 800;\n  color: #fff;\n  font-size: 4rem;\n  width: 800px;\n  margin-bottom: 12rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: white;\n  width: 445px;\n  margin-bottom: 2rem;\n  font-size: 1.2rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: white;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  font-size: 1.3rem;\n}\n\n.container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  margin-top: 5rem;\n  font-weight: bold;\n  color: #353535;\n}\n\n.container[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin-bottom: 2rem;\n  font-weight: bold;\n}\n\n.container[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n  font-size: 1.5rem;\n  color: #C01A24;\n}\n\n.container[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]:hover {\n  color: #610d12;\n}\n\n.container[_ngcontent-%COMP%]   .tag-blog[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 0.8rem;\n}\n\n.container[_ngcontent-%COMP%]   .tag-blog[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #353535;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%] {\n  margin-bottom: 0.5rem;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .nombre[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin: 2rem 0 0 0;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .correo[_ngcontent-%COMP%]   label[_ngcontent-%COMP%], .container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .comentario[_ngcontent-%COMP%]   label[_ngcontent-%COMP%], .container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .web[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin: 0.5rem 0 0 0;\n}\n\n.container[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  margin-top: 3rem;\n  height: 3rem;\n}\n\n@media (min-width: 800px) and (max-width: 1024px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -200px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    margin-bottom: 7rem;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -456px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n    padding: 10rem 11rem 0;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 3rem;\n    width: 460px;\n  }\n\n  .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n    margin-top: 6rem;\n  }\n}\n\n@media (min-width: 356px) and (max-width: 425px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 944px;\n    height: auto;\n    left: -519px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n    padding: 10rem 1rem 0;\n    text-align: center;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2rem;\n    width: 393px;\n    margin-bottom: 5rem;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 393px;\n  }\n\n  .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n    margin-top: 10rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYmxvZy9lbnRyYWRhcy90cml1bmZvLWNhbGwvRDpcXHByb3llY3Rvc1xcd2Vic2l0ZS1jb3Mvc3JjXFxhcHBcXHBhZ2VzXFxibG9nXFxlbnRyYWRhc1xcdHJpdW5mby1jYWxsXFx0cml1bmZvLWNhbGwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Jsb2cvZW50cmFkYXMvdHJpdW5mby1jYWxsL3RyaXVuZm8tY2FsbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9DQUFBO0FDQ0o7O0FERUE7RUFDSSxrQ0FBQTtBQ0NKOztBRENBO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUNFSjs7QURKQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FDRUo7O0FESkE7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQ0VKOztBREVJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ0NSOztBRENJO0VBQ0ksa0JBQUE7RUFDQSxzQkFBQTtBQ0NSOztBREFRO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtBQ0VaOztBREFRO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDRVo7O0FEQVE7RUFDSSxpQkFBQTtBQ0VaOztBRERZO0VBQ0ksb0NBQUE7QUNHaEI7O0FERFk7RUFDSSxZQUFBO0FDR2hCOztBREZnQjtFQUNJLGlCQUFBO0FDSXBCOztBREdJO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUNBUjs7QURFSTtFQUNJLG1CQUFBO0VBQ0EsaUJBQUE7QUNBUjs7QURHUTtFQUNJLG9DQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FDRFo7O0FERVk7RUFDSSxjQUFBO0FDQWhCOztBREtRO0VBQ0ksaUJBQUE7QUNIWjs7QURLUTtFQUNJLGNBQUE7QUNIWjs7QURNSTtFQUNJLHFCQUFBO0FDSlI7O0FETVk7RUFDSSxrQkFBQTtBQ0poQjs7QURRWTtFQUNJLG9CQUFBO0FDTmhCOztBRGdCSTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtBQ2RSOztBRGtCQTtFQUVRO0lBQ0ksYUFBQTtJQUNBLFlBQUE7RUNoQlY7RURvQlU7SUFDSSxtQkFBQTtFQ2xCZDtBQUNGOztBRHdCQTtFQUVRO0lBQ0ksYUFBQTtJQUNBLFlBQUE7RUN2QlY7RUR5Qk07SUFDSSxzQkFBQTtFQ3ZCVjtFRHdCVTtJQUNJLGVBQUE7SUFDQSxZQUFBO0VDdEJkOztFRDJCTTtJQUNJLGdCQUFBO0VDeEJWO0FBQ0Y7O0FENEJBO0VBRVE7SUFDSSxZQUFBO0lBQ0EsWUFBQTtJQUNBLFlBQUE7RUMzQlY7RUQ2Qk07SUFDSSxxQkFBQTtJQUNBLGtCQUFBO0VDM0JWO0VENEJVO0lBQ0ksZUFBQTtJQUNBLFlBQUE7SUFDQSxtQkFBQTtFQzFCZDtFRDRCVTtJQUNJLGVBQUE7SUFDQSxZQUFBO0VDMUJkOztFRGdDTTtJQUNJLGlCQUFBO0VDN0JWO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ibG9nL2VudHJhZGFzL3RyaXVuZm8tY2FsbC90cml1bmZvLWNhbGwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqe1xyXG4gICAgZm9udC1mYW1pbHk6ICdvcGVuLXNhbnMnLCBzYW5zLXNlcmlmO1xyXG59XHJcblxyXG5pe1xyXG4gICAgZm9udC1mYW1pbHk6ICdGb250IEF3ZXNvbWUgNSBGcmVlJztcclxufVxyXG46OnBsYWNlaG9sZGVye1xyXG4gICAgY29sb3I6ICNjNGM0YzQ7XHJcbiAgICBwYWRkaW5nOiAuNXJlbTtcclxufVxyXG4vLyBCYWNrZ3JvdW5nIGNsYXNzXHJcbi5iZ3tcclxuICAgIGltZ3tcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlOyAgIFxyXG4gICAgfVxyXG4gICAgLmNvbnRlbmlkb3tcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgcGFkZGluZzogMTZyZW0gMTJyZW0gMDtcclxuICAgICAgICBoMXtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDgwMDtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNHJlbTtcclxuICAgICAgICAgICAgd2lkdGg6IDgwMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMnJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcHtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICB3aWR0aDogNDQ1cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDJyZW07XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuZm9sbG93e1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnRm9udCBBd2Vzb21lIDUgQnJhbmRzJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBhe1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgJjpob3ZlcntcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEuM3JlbTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4uY29udGFpbmVye1xyXG4gICAgaDJ7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNXJlbTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzM1MzUzNTtcclxuICAgIH1cclxuICAgIGxhYmVse1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDJyZW07XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcbiAgICAuZm9sbG93e1xyXG4gICAgICAgIGl7XHJcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnRm9udCBBd2Vzb21lIDUgQnJhbmRzJztcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICAgICAgICAgIGNvbG9yOiAjQzAxQTI0O1xyXG4gICAgICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6cmdiKDk3LCAxMywgMTgpIDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC50YWctYmxvZ3tcclxuICAgICAgICBpe1xyXG4gICAgICAgICAgICBmb250LXNpemU6IC44cmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBhe1xyXG4gICAgICAgICAgICBjb2xvcjogIzM1MzUzNTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuZm9ybS1ncm91cHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAuNXJlbTtcclxuICAgICAgICAubm9tYnJle1xyXG4gICAgICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjoycmVtIDAgMCAwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb3JyZW97XHJcbiAgICAgICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOi41cmVtIDAgMCAwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC53ZWJ7XHJcbiAgICAgICAgICAgIEBleHRlbmQgLmNvcnJlbztcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNvbWVudGFyaW97XHJcbiAgICAgICAgICAgIEBleHRlbmQgLmNvcnJlbztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBidXR0b257XHJcbiAgICAgICAgbWFyZ2luLXRvcDogM3JlbTtcclxuICAgICAgICBoZWlnaHQ6IDNyZW07XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOjgwMHB4KWFuZChtYXgtd2lkdGg6MTAyNHB4KXtcclxuICAgIC5iZ3tcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMjI0cHg7XHJcbiAgICAgICAgICAgIGxlZnQ6IC0yMDBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNvbnRlbmlkb3tcclxuXHJcbiAgICAgICAgICAgIGgxe1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogN3JlbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOjU2MHB4KWFuZChtYXgtd2lkdGg6NzY4cHgpe1xyXG4gICAgLmJne1xyXG4gICAgICAgIGltZ3tcclxuICAgICAgICAgICAgd2lkdGg6IDEyMjRweDtcclxuICAgICAgICAgICAgbGVmdDogLTQ1NnB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY29udGVuaWRve1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHJlbSAxMXJlbSAwO1xyXG4gICAgICAgICAgICBoMXtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogM3JlbTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA0NjBweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jb250YWluZXJ7XHJcbiAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDZyZW07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogMzU2cHgpYW5kKG1heC13aWR0aDo0MjVweCl7XHJcbiAgICAuYmd7XHJcbiAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICB3aWR0aDogOTQ0cHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAgICAgbGVmdDogLTUxOXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY29udGVuaWRve1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHJlbSAxcmVtIDA7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgaDF7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDJyZW07XHJcbiAgICAgICAgICAgICAgICB3aWR0aDozOTNweDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206NXJlbSA7IFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzkzcHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY29udGFpbmVye1xyXG4gICAgICAgIFxyXG4gICAgICAgIGgye1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHJlbTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIqIHtcbiAgZm9udC1mYW1pbHk6IFwib3Blbi1zYW5zXCIsIHNhbnMtc2VyaWY7XG59XG5cbmkge1xuICBmb250LWZhbWlseTogXCJGb250IEF3ZXNvbWUgNSBGcmVlXCI7XG59XG5cbjo6cGxhY2Vob2xkZXIge1xuICBjb2xvcjogI2M0YzRjNDtcbiAgcGFkZGluZzogMC41cmVtO1xufVxuXG4uYmcgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLmJnIC5jb250ZW5pZG8ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHBhZGRpbmc6IDE2cmVtIDEycmVtIDA7XG59XG4uYmcgLmNvbnRlbmlkbyBoMSB7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDRyZW07XG4gIHdpZHRoOiA4MDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTJyZW07XG59XG4uYmcgLmNvbnRlbmlkbyBwIHtcbiAgY29sb3I6IHdoaXRlO1xuICB3aWR0aDogNDQ1cHg7XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xufVxuLmJnIC5jb250ZW5pZG8gLmZvbGxvdyB7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xufVxuLmJnIC5jb250ZW5pZG8gLmZvbGxvdyBpIHtcbiAgZm9udC1mYW1pbHk6IFwiRm9udCBBd2Vzb21lIDUgQnJhbmRzXCI7XG59XG4uYmcgLmNvbnRlbmlkbyAuZm9sbG93IGEge1xuICBjb2xvcjogd2hpdGU7XG59XG4uYmcgLmNvbnRlbmlkbyAuZm9sbG93IGE6aG92ZXIge1xuICBmb250LXNpemU6IDEuM3JlbTtcbn1cblxuLmNvbnRhaW5lciBoMiB7XG4gIG1hcmdpbi10b3A6IDVyZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzM1MzUzNTtcbn1cbi5jb250YWluZXIgbGFiZWwge1xuICBtYXJnaW4tYm90dG9tOiAycmVtO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jb250YWluZXIgLmZvbGxvdyBpIHtcbiAgZm9udC1mYW1pbHk6IFwiRm9udCBBd2Vzb21lIDUgQnJhbmRzXCI7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xuICBjb2xvcjogI0MwMUEyNDtcbn1cbi5jb250YWluZXIgLmZvbGxvdyBpOmhvdmVyIHtcbiAgY29sb3I6ICM2MTBkMTI7XG59XG4uY29udGFpbmVyIC50YWctYmxvZyBpIHtcbiAgZm9udC1zaXplOiAwLjhyZW07XG59XG4uY29udGFpbmVyIC50YWctYmxvZyBhIHtcbiAgY29sb3I6ICMzNTM1MzU7XG59XG4uY29udGFpbmVyIC5mb3JtLWdyb3VwIHtcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xufVxuLmNvbnRhaW5lciAuZm9ybS1ncm91cCAubm9tYnJlIGxhYmVsIHtcbiAgbWFyZ2luOiAycmVtIDAgMCAwO1xufVxuLmNvbnRhaW5lciAuZm9ybS1ncm91cCAuY29ycmVvIGxhYmVsLCAuY29udGFpbmVyIC5mb3JtLWdyb3VwIC5jb21lbnRhcmlvIGxhYmVsLCAuY29udGFpbmVyIC5mb3JtLWdyb3VwIC53ZWIgbGFiZWwge1xuICBtYXJnaW46IDAuNXJlbSAwIDAgMDtcbn1cbi5jb250YWluZXIgYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogM3JlbTtcbiAgaGVpZ2h0OiAzcmVtO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogODAwcHgpIGFuZCAobWF4LXdpZHRoOiAxMDI0cHgpIHtcbiAgLmJnIGltZyB7XG4gICAgd2lkdGg6IDEyMjRweDtcbiAgICBsZWZ0OiAtMjAwcHg7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8gaDEge1xuICAgIG1hcmdpbi1ib3R0b206IDdyZW07XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA1NjBweCkgYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5iZyBpbWcge1xuICAgIHdpZHRoOiAxMjI0cHg7XG4gICAgbGVmdDogLTQ1NnB4O1xuICB9XG4gIC5iZyAuY29udGVuaWRvIHtcbiAgICBwYWRkaW5nOiAxMHJlbSAxMXJlbSAwO1xuICB9XG4gIC5iZyAuY29udGVuaWRvIGgxIHtcbiAgICBmb250LXNpemU6IDNyZW07XG4gICAgd2lkdGg6IDQ2MHB4O1xuICB9XG5cbiAgLmNvbnRhaW5lciBoMiB7XG4gICAgbWFyZ2luLXRvcDogNnJlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDM1NnB4KSBhbmQgKG1heC13aWR0aDogNDI1cHgpIHtcbiAgLmJnIGltZyB7XG4gICAgd2lkdGg6IDk0NHB4O1xuICAgIGhlaWdodDogYXV0bztcbiAgICBsZWZ0OiAtNTE5cHg7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8ge1xuICAgIHBhZGRpbmc6IDEwcmVtIDFyZW0gMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8gaDEge1xuICAgIGZvbnQtc2l6ZTogMnJlbTtcbiAgICB3aWR0aDogMzkzcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNXJlbTtcbiAgfVxuICAuYmcgLmNvbnRlbmlkbyBwIHtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgd2lkdGg6IDM5M3B4O1xuICB9XG5cbiAgLmNvbnRhaW5lciBoMiB7XG4gICAgbWFyZ2luLXRvcDogMTByZW07XG4gIH1cbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TriunfoCallComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-triunfo-call',
                templateUrl: './triunfo-call.component.html',
                styleUrls: ['./triunfo-call.component.scss']
            }]
    }], function () { return [{ type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"] }, { type: src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__["BlogService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/pages/blog/entradas/triunfo-call/triunfo-call.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/blog/entradas/triunfo-call/triunfo-call.module.ts ***!
  \*************************************************************************/
/*! exports provided: TriunfoCallModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TriunfoCallModule", function() { return TriunfoCallModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _triunfo_call_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./triunfo-call-routing.module */ "./src/app/pages/blog/entradas/triunfo-call/triunfo-call-routing.module.ts");
/* harmony import */ var _triunfo_call_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./triunfo-call.component */ "./src/app/pages/blog/entradas/triunfo-call/triunfo-call.component.ts");





class TriunfoCallModule {
}
TriunfoCallModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: TriunfoCallModule });
TriunfoCallModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function TriunfoCallModule_Factory(t) { return new (t || TriunfoCallModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _triunfo_call_routing_module__WEBPACK_IMPORTED_MODULE_2__["TriunfoCallRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](TriunfoCallModule, { declarations: [_triunfo_call_component__WEBPACK_IMPORTED_MODULE_3__["TriunfoCallComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _triunfo_call_routing_module__WEBPACK_IMPORTED_MODULE_2__["TriunfoCallRoutingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TriunfoCallModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [_triunfo_call_component__WEBPACK_IMPORTED_MODULE_3__["TriunfoCallComponent"]],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _triunfo_call_routing_module__WEBPACK_IMPORTED_MODULE_2__["TriunfoCallRoutingModule"]
                ]
            }]
    }], null, null); })();


/***/ })

}]);
//# sourceMappingURL=pages-blog-entradas-triunfo-call-triunfo-call-module-es2015.js.map