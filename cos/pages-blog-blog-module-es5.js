function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-blog-blog-module"], {
  /***/
  "./src/app/pages/blog/blog-routing.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/blog/blog-routing.module.ts ***!
    \***************************************************/

  /*! exports provided: BlogRoutingModule */

  /***/
  function srcAppPagesBlogBlogRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BlogRoutingModule", function () {
      return BlogRoutingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _blog_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./blog.component */
    "./src/app/pages/blog/blog.component.ts");

    var routes = [{
      path: '',
      component: _blog_component__WEBPACK_IMPORTED_MODULE_2__["BlogComponent"]
    }];

    var BlogRoutingModule = function BlogRoutingModule() {
      _classCallCheck(this, BlogRoutingModule);
    };

    BlogRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: BlogRoutingModule
    });
    BlogRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function BlogRoutingModule_Factory(t) {
        return new (t || BlogRoutingModule)();
      },
      imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](BlogRoutingModule, {
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BlogRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/blog/blog.component.ts":
  /*!**********************************************!*\
    !*** ./src/app/pages/blog/blog.component.ts ***!
    \**********************************************/

  /*! exports provided: BlogComponent */

  /***/
  function srcAppPagesBlogBlogComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BlogComponent", function () {
      return BlogComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
    /* harmony import */


    var src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/servicios/blog.service */
    "./src/app/servicios/blog.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

    var _c0 = function _c0(a0) {
      return [a0];
    };

    function BlogComponent_div_25_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h1");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, " Leer m\xE1s");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r27 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", item_r27.title, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", item_r27.description, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](3, _c0, item_r27.url));
      }
    }

    var BlogComponent =
    /*#__PURE__*/
    function () {
      function BlogComponent(meta, title, _blogService) {
        _classCallCheck(this, BlogComponent);

        this.meta = meta;
        this.title = title;
        this._blogService = _blogService;
        this.entrada = [];
      }

      _createClass(BlogComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          // usar el array del servicio blog
          this.entrada = this._blogService.entrada; // Para ñadir el título de la página

          this.title.setTitle('Blog - Colombian Outsourcing Solution - COS'); // Añadir el tag de la info de la página

          this.meta.addTag({
            name: 'page.info',
            content: 'blog'
          });
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          // Quitar el tag "page.info" antes de ir a otra página, para que rastree correctamente la información
          this.meta.removeTag('name=\'page.info\'');
        }
      }]);

      return BlogComponent;
    }();

    BlogComponent.ɵfac = function BlogComponent_Factory(t) {
      return new (t || BlogComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__["BlogService"]));
    };

    BlogComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: BlogComponent,
      selectors: [["app-blog"]],
      decls: 28,
      vars: 1,
      consts: [["id", "landing", 1, "bg"], ["src", "../../../assets/img/blog-img.png", "alt", "contact-center-BPO-COS-blog"], [1, "contenido"], [1, "text"], [1, "mt-5", "follow"], ["href", "https://www.facebook.com/Outsourcingcos/", "target", "blank"], [1, "fab", "fa-facebook-square", "ml-5"], ["href", "https://www.instagram.com/cosoutsourcing/", "target", "blank"], [1, "fab", "fa-instagram-square", "ml-3"], ["href", "https://www.linkedin.com/company/colombianoutsourcingcos/about/", "target", "blank"], [1, "fab", "fa-linkedin", "ml-3"], ["href", "https://www.youtube.com/channel/UCL89AlspoSj_exe6qidqHew", "target", "blank"], [1, "fab", "fa-youtube", "ml-3"], [1, "entradas"], [1, "card-columns", "container"], ["class", "card", 4, "ngFor", "ngForOf"], ["src", "../../../assets/img/politica.jpg", "alt", "contact-center-BPO-COS", 1, "politica"], [1, "card"], [1, "btn-more", 3, "routerLink"]],
      template: function BlogComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Soluciones tan \xFAnicas como su negocio");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Con nosotros vas a ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "multiplicar ventas, mejorar la experiencia de usuario y tener ideas \xFAnicas e innovadoras");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " para tu negocio.");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "s\xEDguenos en ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "i", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "i", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "i", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "i", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "section");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, BlogComponent_div_25_Template, 7, 5, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "section");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "img", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.entrada);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLink"]],
      styles: ["*[_ngcontent-%COMP%] {\n  font-family: \"open-sans\", sans-serif;\n}\n\ni[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Free\";\n}\n\n.bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n  position: absolute;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n  position: relative;\n  padding: 12rem 12rem 0;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  font-weight: 800;\n  color: #fff;\n  font-size: 4rem;\n  width: 500px;\n  margin-bottom: 2rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: white;\n  width: 445px;\n  margin-bottom: 2rem;\n  font-size: 1.2rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: white;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  font-size: 1.3rem;\n}\n\n@media (min-width: 800px) and (max-width: 1024px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -200px;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -456px;\n  }\n}\n\n@media (min-width: 356px) and (max-width: 425px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 944px;\n    height: auto;\n    left: -519px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n    padding: 10rem 1rem 0;\n    text-align: center;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2rem;\n    width: 393px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 393px;\n  }\n}\n\n.card[_ngcontent-%COMP%] {\n  margin-top: 1rem;\n  padding: 1.5rem;\n  color: #353535;\n  border: none;\n  box-shadow: 5px 5px 20px rgba(53, 53, 53, 0.2);\n}\n\n.card[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n  margin-bottom: 1rem;\n  border-radius: 0.5rem;\n}\n\n.card[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n  font-weight: bold;\n  margin: 2rem 0;\n}\n\n.card[_ngcontent-%COMP%]   .btn-more[_ngcontent-%COMP%] {\n  margin-top: 2rem;\n  border: none;\n  outline: none;\n  border-radius: 0.5rem;\n  width: 100%;\n  padding: 0.5rem;\n  color: #C01A24;\n  border: 1px solid #C01A24;\n  background: white;\n  box-shadow: 5px 5px 10px rgba(192, 26, 36, 0.05);\n}\n\n.card[_ngcontent-%COMP%]   .btn-more[_ngcontent-%COMP%]:hover {\n  background: #C01A24;\n  color: white;\n}\n\n.politica[_ngcontent-%COMP%] {\n  display: block;\n  width: 45%;\n  height: auto;\n  margin-left: auto;\n  margin-right: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYmxvZy9EOlxccHJveWVjdG9zXFx3ZWJzaXRlLWNvcy9zcmNcXGFwcFxccGFnZXNcXGJsb2dcXGJsb2cuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Jsb2cvYmxvZy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9DQUFBO0FDQ0o7O0FERUE7RUFDSSxrQ0FBQTtBQ0NKOztBRElJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ0RSOztBREdJO0VBQ0ksa0JBQUE7RUFDQSxzQkFBQTtBQ0RSOztBREVRO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ0FaOztBREVRO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDQVo7O0FERVE7RUFDSSxpQkFBQTtBQ0FaOztBRENZO0VBQ0ksb0NBQUE7QUNDaEI7O0FEQ1k7RUFDSSxZQUFBO0FDQ2hCOztBREFnQjtFQUNJLGlCQUFBO0FDRXBCOztBREtBO0VBRVE7SUFDSSxhQUFBO0lBQ0EsWUFBQTtFQ0hWO0FBQ0Y7O0FET0E7RUFFUTtJQUNJLGFBQUE7SUFDQSxZQUFBO0VDTlY7QUFDRjs7QURVQTtFQUVRO0lBQ0ksWUFBQTtJQUNBLFlBQUE7SUFDQSxZQUFBO0VDVFY7RURXTTtJQUNJLHFCQUFBO0lBQ0Esa0JBQUE7RUNUVjtFRFVVO0lBQ0ksZUFBQTtJQUNBLFlBQUE7RUNSZDtFRFVVO0lBQ0ksZUFBQTtJQUNBLFlBQUE7RUNSZDtBQUNGOztBRGFBO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSw4Q0FBQTtBQ1hKOztBRFlJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0FDVlI7O0FEWUk7RUFDSSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQ1ZSOztBRGFJO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdEQUFBO0FDWFI7O0FEWVE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7QUNWWjs7QURlQTtFQUNJLGNBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNaSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Jsb2cvYmxvZy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIip7XHJcbiAgICBmb250LWZhbWlseTogJ29wZW4tc2FucycsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbml7XHJcbiAgICBmb250LWZhbWlseTogJ0ZvbnQgQXdlc29tZSA1IEZyZWUnO1xyXG59XHJcblxyXG4vLyBCYWNrZ3JvdW5nIGNsYXNzXHJcbi5iZ3tcclxuICAgIGltZ3tcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlOyAgIFxyXG4gICAgfVxyXG4gICAgLmNvbnRlbmlkb3tcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgcGFkZGluZzogMTJyZW0gMTJyZW0gMDtcclxuICAgICAgICBoMXtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDgwMDtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNHJlbTtcclxuICAgICAgICAgICAgd2lkdGg6IDUwMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwe1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIHdpZHRoOiA0NDVweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjJyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5mb2xsb3d7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6ICdGb250IEF3ZXNvbWUgNSBCcmFuZHMnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGF7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMS4zcmVtO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDo4MDBweClhbmQobWF4LXdpZHRoOjEwMjRweCl7XHJcbiAgICAuYmd7XHJcbiAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICB3aWR0aDogMTIyNHB4O1xyXG4gICAgICAgICAgICBsZWZ0OiAtMjAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDo1NjBweClhbmQobWF4LXdpZHRoOjc2OHB4KXtcclxuICAgIC5iZ3tcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMjI0cHg7XHJcbiAgICAgICAgICAgIGxlZnQ6IC00NTZweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiAzNTZweClhbmQobWF4LXdpZHRoOjQyNXB4KXtcclxuICAgIC5iZ3tcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIHdpZHRoOiA5NDRweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICBsZWZ0OiAtNTE5cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb250ZW5pZG97XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcmVtIDFyZW0gMDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBoMXtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMnJlbTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOjM5M3B4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzkzcHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jYXJke1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxuICAgIHBhZGRpbmc6IDEuNXJlbTtcclxuICAgIGNvbG9yOiAjMzUzNTM1O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgYm94LXNoYWRvdzogNXB4IDVweCAyMHB4IHJnYmEoJGNvbG9yOiAjMzUzNTM1LCAkYWxwaGE6IC4yKTtcclxuICAgIGltZ3tcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOi41cmVtIDtcclxuICAgIH1cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIG1hcmdpbjogMnJlbSAwO1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG4gICAgLmJ0bi1tb3Jle1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDJyZW07XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogLjVyZW07XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogLjVyZW07XHJcbiAgICAgICAgY29sb3I6ICNDMDFBMjQ7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0MwMUEyNDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBib3gtc2hhZG93OiA1cHggNXB4IDEwcHggcmdiYSgkY29sb3I6ICNDMDFBMjQsICRhbHBoYTogLjA1KTtcclxuICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjQzAxQTI0O1xyXG4gICAgICAgICAgICBjb2xvcjp3aGl0ZSA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4ucG9saXRpY2F7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHdpZHRoOiA0NSU7XHJcbiAgICBoZWlnaHQ6IGF1dG87ICBcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG59IiwiKiB7XG4gIGZvbnQtZmFtaWx5OiBcIm9wZW4tc2Fuc1wiLCBzYW5zLXNlcmlmO1xufVxuXG5pIHtcbiAgZm9udC1mYW1pbHk6IFwiRm9udCBBd2Vzb21lIDUgRnJlZVwiO1xufVxuXG4uYmcgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLmJnIC5jb250ZW5pZG8ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHBhZGRpbmc6IDEycmVtIDEycmVtIDA7XG59XG4uYmcgLmNvbnRlbmlkbyBoMSB7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDRyZW07XG4gIHdpZHRoOiA1MDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbn1cbi5iZyAuY29udGVuaWRvIHAge1xuICBjb2xvcjogd2hpdGU7XG4gIHdpZHRoOiA0NDVweDtcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbiAgZm9udC1zaXplOiAxLjJyZW07XG59XG4uYmcgLmNvbnRlbmlkbyAuZm9sbG93IHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG59XG4uYmcgLmNvbnRlbmlkbyAuZm9sbG93IGkge1xuICBmb250LWZhbWlseTogXCJGb250IEF3ZXNvbWUgNSBCcmFuZHNcIjtcbn1cbi5iZyAuY29udGVuaWRvIC5mb2xsb3cgYSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5iZyAuY29udGVuaWRvIC5mb2xsb3cgYTpob3ZlciB7XG4gIGZvbnQtc2l6ZTogMS4zcmVtO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogODAwcHgpIGFuZCAobWF4LXdpZHRoOiAxMDI0cHgpIHtcbiAgLmJnIGltZyB7XG4gICAgd2lkdGg6IDEyMjRweDtcbiAgICBsZWZ0OiAtMjAwcHg7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA1NjBweCkgYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5iZyBpbWcge1xuICAgIHdpZHRoOiAxMjI0cHg7XG4gICAgbGVmdDogLTQ1NnB4O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMzU2cHgpIGFuZCAobWF4LXdpZHRoOiA0MjVweCkge1xuICAuYmcgaW1nIHtcbiAgICB3aWR0aDogOTQ0cHg7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIGxlZnQ6IC01MTlweDtcbiAgfVxuICAuYmcgLmNvbnRlbmlkbyB7XG4gICAgcGFkZGluZzogMTByZW0gMXJlbSAwO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICAuYmcgLmNvbnRlbmlkbyBoMSB7XG4gICAgZm9udC1zaXplOiAycmVtO1xuICAgIHdpZHRoOiAzOTNweDtcbiAgfVxuICAuYmcgLmNvbnRlbmlkbyBwIHtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgd2lkdGg6IDM5M3B4O1xuICB9XG59XG4uY2FyZCB7XG4gIG1hcmdpbi10b3A6IDFyZW07XG4gIHBhZGRpbmc6IDEuNXJlbTtcbiAgY29sb3I6ICMzNTM1MzU7XG4gIGJvcmRlcjogbm9uZTtcbiAgYm94LXNoYWRvdzogNXB4IDVweCAyMHB4IHJnYmEoNTMsIDUzLCA1MywgMC4yKTtcbn1cbi5jYXJkIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG4gIG1hcmdpbi1ib3R0b206IDFyZW07XG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcbn1cbi5jYXJkIGgxIHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBtYXJnaW46IDJyZW0gMDtcbn1cbi5jYXJkIC5idG4tbW9yZSB7XG4gIG1hcmdpbi10b3A6IDJyZW07XG4gIGJvcmRlcjogbm9uZTtcbiAgb3V0bGluZTogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMC41cmVtO1xuICBjb2xvcjogI0MwMUEyNDtcbiAgYm9yZGVyOiAxcHggc29saWQgI0MwMUEyNDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJveC1zaGFkb3c6IDVweCA1cHggMTBweCByZ2JhKDE5MiwgMjYsIDM2LCAwLjA1KTtcbn1cbi5jYXJkIC5idG4tbW9yZTpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICNDMDFBMjQ7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnBvbGl0aWNhIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiA0NSU7XG4gIGhlaWdodDogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbn0iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BlogComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-blog',
          templateUrl: './blog.component.html',
          styleUrls: ['./blog.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"]
        }, {
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"]
        }, {
          type: src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__["BlogService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/blog/blog.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/pages/blog/blog.module.ts ***!
    \*******************************************/

  /*! exports provided: BlogModule */

  /***/
  function srcAppPagesBlogBlogModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BlogModule", function () {
      return BlogModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _blog_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./blog-routing.module */
    "./src/app/pages/blog/blog-routing.module.ts");
    /* harmony import */


    var _blog_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./blog.component */
    "./src/app/pages/blog/blog.component.ts");
    /* harmony import */


    var _servicios_blog_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../servicios/blog.service */
    "./src/app/servicios/blog.service.ts");

    var BlogModule = function BlogModule() {
      _classCallCheck(this, BlogModule);
    };

    BlogModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: BlogModule
    });
    BlogModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function BlogModule_Factory(t) {
        return new (t || BlogModule)();
      },
      providers: [_servicios_blog_service__WEBPACK_IMPORTED_MODULE_4__["BlogService"]],
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _blog_routing_module__WEBPACK_IMPORTED_MODULE_2__["BlogRoutingModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](BlogModule, {
        declarations: [_blog_component__WEBPACK_IMPORTED_MODULE_3__["BlogComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _blog_routing_module__WEBPACK_IMPORTED_MODULE_2__["BlogRoutingModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BlogModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [_blog_component__WEBPACK_IMPORTED_MODULE_3__["BlogComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _blog_routing_module__WEBPACK_IMPORTED_MODULE_2__["BlogRoutingModule"]],
          providers: [_servicios_blog_service__WEBPACK_IMPORTED_MODULE_4__["BlogService"]]
        }]
      }], null, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=pages-blog-blog-module-es5.js.map