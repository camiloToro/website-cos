function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-unete-unete-module"], {
  /***/
  "./src/app/pages/unete/unete-routing.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/unete/unete-routing.module.ts ***!
    \*****************************************************/

  /*! exports provided: UneteRoutingModule */

  /***/
  function srcAppPagesUneteUneteRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UneteRoutingModule", function () {
      return UneteRoutingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _unete_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./unete.component */
    "./src/app/pages/unete/unete.component.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js"); //Translation


    var routes = [{
      path: '',
      component: _unete_component__WEBPACK_IMPORTED_MODULE_2__["UneteComponent"]
    }];

    var UneteRoutingModule = function UneteRoutingModule() {
      _classCallCheck(this, UneteRoutingModule);
    };

    UneteRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: UneteRoutingModule
    });
    UneteRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function UneteRoutingModule_Factory(t) {
        return new (t || UneteRoutingModule)();
      },
      imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](UneteRoutingModule, {
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UneteRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/unete/unete.component.ts":
  /*!************************************************!*\
    !*** ./src/app/pages/unete/unete.component.ts ***!
    \************************************************/

  /*! exports provided: UneteComponent */

  /***/
  function srcAppPagesUneteUneteComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UneteComponent", function () {
      return UneteComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
    /* harmony import */


    var src_app_servicios_correo_correo_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/servicios/correo/correo.service */
    "./src/app/servicios/correo/correo.service.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");

    var UneteComponent =
    /*#__PURE__*/
    function () {
      function UneteComponent(builder, meta, title, correoService, translate) {
        _classCallCheck(this, UneteComponent);

        this.builder = builder;
        this.meta = meta;
        this.title = title;
        this.correoService = correoService;
        this.translate = translate;
        this.activeLang = localStorage.getItem("idioma") ? localStorage.getItem("idioma") : 'es';
        this.fileName = 'Adjunta tu HV'; //cambiar nombre en adjunto

        this.bandera = 'es'; // Formulario

        var nombre = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4)]);
        var telefono = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4)]);
        var correo = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]);
        var asunto = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(5)]);
        var file = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        var terminos = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        this.contact = this.builder.group({
          nombre: nombre,
          telefono: telefono,
          correo: correo,
          asunto: asunto,
          file: file,
          terminos: terminos
        });

        if (this.activeLang == "\"en\"") {
          this.bandera = 'en';
        } else if (this.activeLang == "\"es\"") {
          this.bandera = 'es';
        }

        this.translate.setDefaultLang(this.bandera);
      }

      _createClass(UneteComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          // Para ñadir el título de la página
          this.title.setTitle('Únete - Colombian Outsourcing Solution - COS'); // Añadir el tag de la info de la página

          this.meta.addTag({
            name: 'page.info',
            content: 'contacto'
          });
        }
      }, {
        key: "cambiarLenguaje",
        value: function cambiarLenguaje(lang) {
          this.activeLang = lang;
          this.translate.use(lang);
          localStorage.setItem('idioma', JSON.stringify(this.activeLang));
          var idio = localStorage.getItem("idioma");
          console.log(idio);
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          // Quitar el tag "page.info" antes de ir a otra página, para que rastree correctamente la información
          this.meta.removeTag('name=\'page.info\'');
        }
      }, {
        key: "enviar",
        value: function enviar() {
          var data = {
            nombre: this.contact.value.nombre,
            telefono: this.contact.value.telefono,
            email: this.contact.value.correo,
            asunto: this.contact.value.asunto,
            file: this.contact.value.file,
            fileName: this.fileName,
            size: this.size,
            type: this.type,
            filecomplete: this.filecomplete,
            enviar: 'insert'
          };
          this.correoService.senMail(data).subscribe(function (respuesta) {
            console.log(respuesta);
          }); // this.contact.reset();

          console.log(data);
          console.log(this.contact.value.nombre); //  this.contact.reset();
        } // validacion para cambios de estado de inputs

      }, {
        key: "onUploadOutput",
        //Función para imprimir el nombre del archivo adjunto
        value: function onUploadOutput(event) {
          this.fileName = event.srcElement.files[0].name;
          this.filecomplete = event.srcElement.files[0];
          this.size = event.srcElement.files[0].size;
          this.type = event.srcElement.files[0].type;
          console.log(event.srcElement.files[0]); // this.fileName = event.srcElement.files[0];
        }
      }, {
        key: "nombre",
        get: function get() {
          return this.contact.get('nombre'); // validacion nombre
        }
      }, {
        key: "nombreValid",
        get: function get() {
          return this.nombre.touched && this.nombre.valid;
        }
      }, {
        key: "nombreInvalid",
        get: function get() {
          return this.nombre.touched && this.nombre.invalid;
        }
      }, {
        key: "telefono",
        get: function get() {
          return this.contact.get('telefono'); // validacion telefono
        }
      }, {
        key: "telefonoValid",
        get: function get() {
          return this.telefono.touched && this.telefono.valid;
        }
      }, {
        key: "telefonoInvalid",
        get: function get() {
          return this.telefono.touched && this.telefono.invalid;
        }
      }, {
        key: "correo",
        get: function get() {
          return this.contact.get('correo'); // validacion correo
        }
      }, {
        key: "correoValid",
        get: function get() {
          return this.correo.touched && this.correo.valid;
        }
      }, {
        key: "correoInvalid",
        get: function get() {
          return this.correo.touched && this.correo.invalid;
        }
      }, {
        key: "asunto",
        get: function get() {
          return this.contact.get('asunto'); // validacion asunto
        }
      }, {
        key: "asuntoValid",
        get: function get() {
          return this.asunto.touched && this.asunto.valid;
        }
      }, {
        key: "asuntoInvalid",
        get: function get() {
          return this.asunto.touched && this.asunto.invalid;
        }
      }, {
        key: "mensaje",
        get: function get() {
          return this.contact.get('mensaje'); // validacion usuario
        }
      }, {
        key: "mensajeValid",
        get: function get() {
          return this.mensaje.touched && this.mensaje.valid;
        }
      }, {
        key: "mensajeInvalid",
        get: function get() {
          return this.mensaje.touched && this.mensaje.invalid;
        }
      }]);

      return UneteComponent;
    }();

    UneteComponent.ɵfac = function UneteComponent_Factory(t) {
      return new (t || UneteComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Meta"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Title"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_servicios_correo_correo_service__WEBPACK_IMPORTED_MODULE_3__["CorreoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"]));
    };

    UneteComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: UneteComponent,
      selectors: [["app-unete"]],
      decls: 43,
      vars: 43,
      consts: [["id", "landing", 1, "bg"], ["src", "../../../assets/img/unete-a-cos.png", "alt", "contact-center-BPO-COS"], [1, "contenido"], [1, "text"], [1, "mt-5", "follow"], ["href", "https://www.facebook.com/Outsourcingcos/", "target", "blank"], [1, "fab", "fa-facebook-square", "ml-5"], ["href", "https://www.instagram.com/cosoutsourcing/", "target", "blank"], [1, "fab", "fa-instagram-square", "ml-3"], ["href", "https://www.linkedin.com/company/colombianoutsourcingcos/about/", "target", "blank"], [1, "fab", "fa-linkedin", "ml-3"], ["href", "https://www.youtube.com/channel/UCL89AlspoSj_exe6qidqHew", "target", "blank"], [1, "fab", "fa-youtube", "ml-3"], ["method", "post", "enctype", "multipart/form-data", 1, "contacto", 3, "formGroup", "ngSubmit"], ["type", "text", "formControlName", "nombre", 1, "form-control", 3, "placeholder"], ["type", "text", "formControlName", "telefono", 1, "form-control", 3, "placeholder"], ["type", "email", "formControlName", "correo", 1, "form-control", 3, "placeholder"], ["type", "checkbox", "name", "acepto termino", "id", "terminos", "formControlName", "terminos"], ["href", ""], ["data-sitekey", "6Lf1QcAUAAAAACu8fCwgAcxDs7RB0WRKM2pqd9JO", 1, "g-recaptcha"], ["type", "submit", 1, "btn-send"]],
      template: function UneteComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](6, "translate");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](9, "translate");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "p", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](13, "translate");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "i", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "i", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "i", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "i", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "form", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function UneteComponent_Template_form_ngSubmit_22_listener() {
            return ctx.enviar();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](25, "translate");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "input", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](27, "translate");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "input", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](29, "translate");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "input", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](31, "translate");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "input", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](35, "translate");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "a", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](38, "translate");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "div", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "button", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](42, "translate");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](6, 23, "Family.title"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](9, 25, "Family.parrafo"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](13, 27, "Family.followin"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.contact);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](25, 29, "Family.contact"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.nombreValid)("is-invalid", ctx.nombreInvalid);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("placeholder", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](27, 31, "Family.name"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.telefonoValid)("is-invalid", ctx.telefonoInvalid);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("placeholder", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](29, 33, "Family.phone"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-valid", ctx.correoValid)("is-invalid", ctx.correoInvalid);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("placeholder", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](31, 35, "Family.email"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](35, 37, "Family.accept"), " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" \xA0", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](38, 39, "Family.terms"), "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](42, 41, "Family.send"), " ");
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["CheckboxControlValueAccessor"]],
      pipes: [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslatePipe"]],
      styles: ["*[_ngcontent-%COMP%] {\n  font-family: \"open-sans\", sans-serif;\n}\n\ni[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Free\";\n}\n\n.is-valid[_ngcontent-%COMP%] {\n  background: rgba(255, 255, 255, 0.2);\n  margin-bottom: 1rem;\n  width: 30rem;\n  padding-left: 2rem;\n  color: #fff;\n  border: 1px solid green !important;\n}\n\n.is-valid[_ngcontent-%COMP%]:hover {\n  background: rgba(255, 255, 255, 0.5);\n}\n\n.is-invalid[_ngcontent-%COMP%] {\n  background: rgba(192, 26, 36, 0.2) !important;\n  margin-bottom: 1rem;\n  width: 30rem;\n  padding-left: 2rem;\n  color: #fff;\n  border: 1px solid #C01A24 !important;\n}\n\n.is-invalid[_ngcontent-%COMP%]:hover {\n  background: rgba(255, 255, 255, 0.5);\n}\n\n.bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  position: absolute;\n  width: 100%;\n  height: auto;\n  -webkit-animation: size-bg 1.5s ease-in;\n  animation: size-bg 1.5s ease-in;\n}\n\n@-webkit-keyframes size-bg {\n  from {\n    width: 110%;\n  }\n  to {\n    background-size: 100%;\n  }\n}\n\n@keyframes size-bg {\n  from {\n    background-size: 110%;\n  }\n  to {\n    background-size: 100%;\n  }\n}\n\n.contenido[_ngcontent-%COMP%] {\n  position: relative;\n  display: grid;\n  grid-template-columns: 50% 50%;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n  grid-column: 1;\n  padding: 15rem 10rem;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  font-weight: 800;\n  color: #fff;\n  font-size: 4rem;\n  width: 500px;\n  margin-bottom: 2rem;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: white;\n  width: 445px;\n  margin-bottom: 2rem;\n  font-size: 1.2rem;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: white;\n}\n\n.contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  font-size: 1.3rem;\n}\n\n.contacto[_ngcontent-%COMP%] {\n  grid-column: 2;\n  margin-bottom: 0;\n  padding: 15rem 0 0 5rem;\n}\n\n.contacto[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  font-weight: bold;\n  color: #fff;\n  margin-bottom: 2rem;\n}\n\n.contacto[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .contacto[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  background: rgba(255, 255, 255, 0.2);\n  margin-bottom: 1rem;\n  width: 30rem;\n  padding-left: 2rem;\n  color: #fff;\n  border: 1px solid #fff;\n}\n\n.contacto[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]:hover, .contacto[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%]:hover {\n  background: rgba(255, 255, 255, 0.5);\n}\n\n.contacto[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  color: white;\n  display: flex;\n  align-items: baseline;\n}\n\n.contacto[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .contacto[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  width: 1rem;\n  margin: 0 0.5rem 0 0;\n  cursor: pointer;\n}\n\n.contacto[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: white;\n}\n\n.contacto[_ngcontent-%COMP%]   .btn-send[_ngcontent-%COMP%] {\n  width: 30rem;\n  background: white;\n  color: #A40505;\n  font-weight: bold;\n  height: 3rem;\n  border: none;\n  border-radius: 0.2rem;\n  margin-top: 1rem;\n}\n\n.contacto[_ngcontent-%COMP%]   .btn-send[_ngcontent-%COMP%]:hover {\n  -webkit-animation: colorBg 0.2s ease-in;\n  animation: colorBg 0.2s ease-in;\n  background: #A40505;\n  color: white;\n}\n\n@-webkit-keyframes colorBg {\n  from {\n    background: #fff;\n  }\n  to {\n    background: #A40505;\n  }\n}\n\n@keyframes colorBg {\n  from {\n    background: #fff;\n  }\n  to {\n    background: #A40505;\n  }\n}\n\n[_ngcontent-%COMP%]::-moz-placeholder {\n  color: white;\n}\n\n[_ngcontent-%COMP%]::-ms-input-placeholder {\n  color: white;\n}\n\n[_ngcontent-%COMP%]::placeholder {\n  color: white;\n}\n\ninput[type=file][_ngcontent-%COMP%]::-webkit-file-upload-button, .contacto[_ngcontent-%COMP%]   textarea[type=file][_ngcontent-%COMP%]::-webkit-file-upload-button {\n  display: none;\n}\n\n.upload[_ngcontent-%COMP%] {\n  padding: 32px 0 0 12px;\n  position: absolute;\n  display: none;\n}\n\n.adjuntar[_ngcontent-%COMP%] {\n  margin: 0 0 2rem 0;\n  background: rgba(255, 255, 255, 0.2);\n  height: 3rem;\n  width: 30rem;\n  align-items: center;\n  display: flex;\n  cursor: pointer;\n  border-radius: 0.5rem;\n  padding: 0.5rem 1rem 0;\n}\n\n.adjuntar[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  width: 100%;\n  cursor: pointer;\n}\n\n.adjuntar[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-size: 1rem;\n  margin-left: 32px;\n  width: 230px;\n  position: absolute;\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  cursor: pointer;\n  color: #fff;\n}\n\n.adjuntar[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 1.25rem;\n  cursor: pointer;\n  margin: 0;\n  color: #fff;\n}\n\n.adjuntar[_ngcontent-%COMP%]:hover {\n  margin: 0 0 2rem 0;\n  background: rgba(255, 255, 255, 0.5);\n  height: 3rem;\n  width: 30rem;\n  align-items: center;\n  display: flex;\n  cursor: pointer;\n}\n\n.adjuntar[_ngcontent-%COMP%]:hover   span[_ngcontent-%COMP%] {\n  color: #fff;\n}\n\n.adjuntar[_ngcontent-%COMP%]:hover   label[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  color: #fff;\n}\n\n@media (max-width: 425px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    position: absolute;\n    width: 1814px;\n    left: -1389px;\n    height: auto;\n    -webkit-animation: size-bg 1.5s ease-in;\n    animation: size-bg 1.5s ease-in;\n  }\n\n  .contenido[_ngcontent-%COMP%] {\n    display: grid;\n    grid-template-columns: none;\n    grid-template-rows: 30% 70%;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n    grid-column: 1;\n    padding: 7rem 2rem 0;\n    grid-row: 1;\n    text-align: center;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    width: 364px;\n    font-size: 2.5rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 364px;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n    font-size: 1rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]:hover {\n    font-size: 0.9rem;\n  }\n\n  .contenido[_ngcontent-%COMP%]    > *[_ngcontent-%COMP%]:nth-child(1) {\n    -ms-grid-row: 1;\n    -ms-grid-column: 1;\n  }\n\n  .contenido[_ngcontent-%COMP%]    > *[_ngcontent-%COMP%]:nth-child(2) {\n    -ms-grid-row: 2;\n    -ms-grid-column: 1;\n  }\n\n  .contacto[_ngcontent-%COMP%] {\n    grid-column: 1;\n    grid-row: 2;\n    padding: 5rem 2rem;\n    text-align: center;\n  }\n  .contacto[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n    width: 364px;\n  }\n  .contacto[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .contacto[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n    width: 22rem;\n  }\n  .contacto[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n    width: 364px;\n  }\n  .contacto[_ngcontent-%COMP%]   .btn-send[_ngcontent-%COMP%] {\n    width: 22rem;\n    margin-top: 2rem;\n  }\n\n  .adjuntar[_ngcontent-%COMP%] {\n    width: 22rem;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    position: absolute;\n    width: 1825px;\n    left: -1057px;\n    height: auto;\n    -webkit-animation: size-bg 1.5s ease-in;\n    animation: size-bg 1.5s ease-in;\n  }\n\n  .contenido[_ngcontent-%COMP%] {\n    display: grid;\n    grid-template-columns: none;\n    grid-template-rows: 30% 70%;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n    grid-column: 1;\n    padding: 7rem 2rem 0;\n    grid-row: 1;\n    text-align: center;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    width: 704px;\n    font-size: 2.5rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 704px;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n    font-size: 1rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]:hover {\n    font-size: 0.9rem;\n  }\n\n  .contenido[_ngcontent-%COMP%]    > *[_ngcontent-%COMP%]:nth-child(1) {\n    -ms-grid-row: 1;\n    -ms-grid-column: 1;\n  }\n\n  .contenido[_ngcontent-%COMP%]    > *[_ngcontent-%COMP%]:nth-child(2) {\n    -ms-grid-row: 2;\n    -ms-grid-column: 1;\n  }\n\n  .contacto[_ngcontent-%COMP%] {\n    grid-column: 1;\n    grid-row: 2;\n    padding: 5rem 4rem;\n    text-align: center;\n  }\n  .contacto[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .contacto[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n    width: 40rem;\n  }\n  .contacto[_ngcontent-%COMP%]   .btn-send[_ngcontent-%COMP%] {\n    width: 40rem;\n  }\n\n  .adjuntar[_ngcontent-%COMP%] {\n    width: 40rem;\n  }\n}\n\n@media (min-width: 800px) and (max-width: 1024px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    position: absolute;\n    width: 1353px;\n    left: -329px;\n    height: auto;\n    -webkit-animation: size-bg 1.5s ease-in;\n    animation: size-bg 1.5s ease-in;\n  }\n\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n    padding: 15rem 4rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    width: 408px;\n    font-size: 3rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 408px;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n    font-size: 1rem;\n  }\n  .contenido[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]:hover {\n    font-size: 0.9rem;\n  }\n\n  .contacto[_ngcontent-%COMP%] {\n    padding: 8rem 0 0 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdW5ldGUvRDpcXHByb3llY3Rvc1xcd2Vic2l0ZS1jb3Mvc3JjXFxhcHBcXHBhZ2VzXFx1bmV0ZVxcdW5ldGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3VuZXRlL3VuZXRlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0ksb0NBQUE7QUNESjs7QURJQTtFQUNJLGtDQUFBO0FDREo7O0FES0E7RUFDSSxvQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGtDQUFBO0FDRko7O0FER0k7RUFDSSxvQ0FBQTtBQ0RSOztBRElBO0VBQ0ksNkNBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxvQ0FBQTtBQ0RKOztBREVJO0VBQ0ksb0NBQUE7QUNBUjs7QURLSTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSx1Q0FBQTtFQUNBLCtCQUFBO0FDRlI7O0FES0E7RUFDSTtJQUFPLFdBQUE7RUNEVDtFREVFO0lBQUsscUJBQUE7RUNDUDtBQUNGOztBREFBO0VBQ0k7SUFBTyxxQkFBQTtFQ0dUO0VERkU7SUFBSyxxQkFBQTtFQ0tQO0FBQ0Y7O0FESEE7RUFDSSxrQkFBQTtFQUVBLGFBQUE7RUFFQSw4QkFBQTtBQ0tKOztBREpJO0VBRUksY0FBQTtFQUNBLG9CQUFBO0FDTVI7O0FETFE7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FDT1o7O0FETFE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUNPWjs7QURMUTtFQUNJLGlCQUFBO0FDT1o7O0FETlk7RUFDSSxvQ0FBQTtBQ1FoQjs7QUROWTtFQUNJLFlBQUE7QUNRaEI7O0FEUGdCO0VBQ0ksaUJBQUE7QUNTcEI7O0FEREE7RUFFSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQ0lKOztBREhJO0VBQ0ksaUJBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7QUNLUjs7QURISTtFQUNJLG9DQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7QUNLUjs7QURKUTtFQUNJLG9DQUFBO0FDTVo7O0FEQUk7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHFCQUFBO0FDRVI7O0FERFE7RUFDSSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0FDR1o7O0FERFE7RUFDSSxZQUFBO0FDR1o7O0FEQUk7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7QUNFUjs7QUREUTtFQUNJLHVDQUFBO0VBQ1EsK0JBQUE7RUFDUixtQkFBQTtFQUNBLFlBQUE7QUNHWjs7QURGWTtFQUNJO0lBQU0sZ0JBQUE7RUNLcEI7RURKYztJQUFJLG1CQUFBO0VDT2xCO0FBQ0Y7O0FETFk7RUFDSTtJQUFNLGdCQUFBO0VDUXBCO0VEUGM7SUFBSSxtQkFBQTtFQ1VsQjtBQUNGOztBREZBO0VBQ0ksWUFBQTtBQ1NKOztBREpBO0VBQ0ksWUFBQTtBQ1dKOztBRFRBO0VBQ0ksWUFBQTtBQ1lKOztBRFBBO0VBQ0ksYUFBQTtBQ1VKOztBRFBBO0VBQ0ksc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUNVSjs7QURQQTtFQUNJLGtCQUFBO0VBQ0Esb0NBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtFQUNBLHNCQUFBO0FDVUo7O0FEVEk7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQ1dSOztBRFRJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUNXUjs7QURSSTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0FDVVI7O0FEUEk7RUFDSSxrQkFBQTtFQUNBLG9DQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0FDU1I7O0FEUlE7RUFDSSxXQUFBO0FDVVo7O0FEUFE7RUFDSSxXQUFBO0FDU1o7O0FESkE7RUFFUTtJQUNJLGtCQUFBO0lBQ0EsYUFBQTtJQUNBLGFBQUE7SUFDQSxZQUFBO0lBQ0EsdUNBQUE7SUFDQSwrQkFBQTtFQ01WOztFREhFO0lBRUksYUFBQTtJQUVBLDJCQUFBO0lBRUEsMkJBQUE7RUNNTjtFRExNO0lBRUksY0FBQTtJQUNBLG9CQUFBO0lBRUEsV0FBQTtJQUNBLGtCQUFBO0VDT1Y7RUROVTtJQUNJLFlBQUE7SUFDQSxpQkFBQTtFQ1FkO0VETlU7SUFDSSxlQUFBO0lBQ0EsWUFBQTtFQ1FkO0VETlU7SUFDSSxlQUFBO0VDUWQ7RURQYztJQUNJLGlCQUFBO0VDU2xCOztFREpFO0lBQ0ksZUFBQTtJQUNBLGtCQUFBO0VDT047O0VETEU7SUFDSSxlQUFBO0lBQ0Esa0JBQUE7RUNRTjs7RURORTtJQUVJLGNBQUE7SUFFQSxXQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtFQ1NOO0VEUk07SUFDSSxZQUFBO0VDVVY7RURSTTtJQUNJLFlBQUE7RUNVVjtFRFJNO0lBQ0ksWUFBQTtFQ1VWO0VEUk07SUFDSSxZQUFBO0lBQ0EsZ0JBQUE7RUNVVjs7RURQRTtJQUNJLFlBQUE7RUNVTjtBQUNGOztBRFBBO0VBRVE7SUFDSSxrQkFBQTtJQUNBLGFBQUE7SUFDQSxhQUFBO0lBQ0EsWUFBQTtJQUNBLHVDQUFBO0lBQ0EsK0JBQUE7RUNRVjs7RURMRTtJQUVJLGFBQUE7SUFFQSwyQkFBQTtJQUVBLDJCQUFBO0VDUU47RURQTTtJQUVJLGNBQUE7SUFDQSxvQkFBQTtJQUVBLFdBQUE7SUFDQSxrQkFBQTtFQ1NWO0VEUlU7SUFDSSxZQUFBO0lBQ0EsaUJBQUE7RUNVZDtFRFJVO0lBQ0ksZUFBQTtJQUNBLFlBQUE7RUNVZDtFRFJVO0lBQ0ksZUFBQTtFQ1VkO0VEVGM7SUFDSSxpQkFBQTtFQ1dsQjs7RURORTtJQUNJLGVBQUE7SUFDQSxrQkFBQTtFQ1NOOztFRFBFO0lBQ0ksZUFBQTtJQUNBLGtCQUFBO0VDVU47O0VEUkU7SUFFSSxjQUFBO0lBRUEsV0FBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7RUNXTjtFRFZNO0lBQ0ksWUFBQTtFQ1lWO0VEVk07SUFDSSxZQUFBO0VDWVY7O0VEVEU7SUFDSSxZQUFBO0VDWU47QUFDRjs7QURUQTtFQUlRO0lBQ0ksa0JBQUE7SUFDQSxhQUFBO0lBQ0EsWUFBQTtJQUNBLFlBQUE7SUFDQSx1Q0FBQTtJQUNBLCtCQUFBO0VDUVY7O0VESk07SUFDSSxtQkFBQTtFQ09WO0VETlU7SUFDSSxZQUFBO0lBQ0EsZUFBQTtFQ1FkO0VETlU7SUFDSSxlQUFBO0lBQ0EsWUFBQTtFQ1FkO0VETlU7SUFDSSxlQUFBO0VDUWQ7RURQYztJQUNJLGlCQUFBO0VDU2xCOztFREpFO0lBQ0ksbUJBQUE7RUNPTjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdW5ldGUvdW5ldGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLy8gR2VuZXJhbHMgXHJcbip7XHJcbiAgICBmb250LWZhbWlseTogJ29wZW4tc2Fucycsc2Fucy1zZXJpZjtcclxufVxyXG5cclxuaXtcclxuICAgIGZvbnQtZmFtaWx5OiAnRm9udCBBd2Vzb21lIDUgRnJlZSc7XHJcbn1cclxuXHJcbi8vIElucHVudCBWYWxpZCBDbGFzc1xyXG4uaXMtdmFsaWR7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKCRjb2xvcjogI2ZmZiwgJGFscGhhOiAuMik7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG4gICAgd2lkdGg6IDMwcmVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OjJyZW07XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyZWVuICFpbXBvcnRhbnQ7XHJcbiAgICAmOmhvdmVye1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHJnYmEoJGNvbG9yOiAjZmZmLCAkYWxwaGE6IC41KTtcclxuICAgIH1cclxufVxyXG4uaXMtaW52YWxpZHtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoJGNvbG9yOiAjQzAxQTI0LCAkYWxwaGE6IC4yKSAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICAgIHdpZHRoOiAzMHJlbTtcclxuICAgIHBhZGRpbmctbGVmdDoycmVtO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjQzAxQTI0ICFpbXBvcnRhbnQ7XHJcbiAgICAmOmhvdmVye1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHJnYmEoJGNvbG9yOiAjZmZmLCAkYWxwaGE6IC41KTtcclxuICAgIH1cclxufVxyXG4vLyBTZWN0aW9uIDEgKiogQ29udGFjdCBsYW5kaW5nXHJcbi5iZ3tcclxuICAgIGltZ3tcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgIC13ZWJraXQtYW5pbWF0aW9uOiBzaXplLWJnIDEuNXMgZWFzZS1pbjtcclxuICAgICAgICBhbmltYXRpb246IHNpemUtYmcgMS41cyBlYXNlLWluO1xyXG4gICAgfVxyXG59XHJcbkAtd2Via2l0LWtleWZyYW1lcyBzaXplLWJne1xyXG4gICAgZnJvbSB7IHdpZHRoOiAxMTAlOyB9XHJcbiAgICB0byB7IGJhY2tncm91bmQtc2l6ZTogMTAwJTt9XHJcbn1cclxuQGtleWZyYW1lcyBzaXplLWJne1xyXG4gICAgZnJvbSB7IGJhY2tncm91bmQtc2l6ZTogMTEwJTsgfVxyXG4gICAgdG8geyBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7fVxyXG59XHJcblxyXG4uY29udGVuaWRve1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XHJcbiAgICBkaXNwbGF5OiBncmlkO1xyXG4gICAgLW1zLWdyaWQtY29sdW1uczogNTAlIDUwJTtcclxuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogNTAlIDUwJTtcclxuICAgIC50ZXh0e1xyXG4gICAgICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcclxuICAgICAgICBncmlkLWNvbHVtbjogMTtcclxuICAgICAgICBwYWRkaW5nOiAxNXJlbSAxMHJlbTtcclxuICAgICAgICBoMXtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDgwMDtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNHJlbTtcclxuICAgICAgICAgICAgd2lkdGg6IDUwMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwe1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIHdpZHRoOiA0NDVweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjJyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5mb2xsb3d7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6ICdGb250IEF3ZXNvbWUgNSBCcmFuZHMnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGF7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMS4zcmVtO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgICBcclxuXHJcbn1cclxuLmNvbnRhY3Rve1xyXG4gICAgLW1zLWdyaWQtY29sdW1uOiAyO1xyXG4gICAgZ3JpZC1jb2x1bW46IDI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgcGFkZGluZzogMTVyZW0gMCAwIDVyZW07XHJcbiAgICBoM3tcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4gICAgfVxyXG4gICAgaW5wdXR7XHJcbiAgICAgICAgYmFja2dyb3VuZDogcmdiYSgkY29sb3I6ICNmZmYsICRhbHBoYTogLjIpO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICAgICAgd2lkdGg6IDMwcmVtO1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDoycmVtO1xyXG4gICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XHJcbiAgICAgICAgJjpob3ZlcntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogcmdiYSgkY29sb3I6ICNmZmYsICRhbHBoYTogLjUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHRleHRhcmVhe1xyXG4gICAgICAgIEBleHRlbmQgaW5wdXRcclxuICAgIH1cclxuICAgIGxhYmVse1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBiYXNlbGluZTtcclxuICAgICAgICBpbnB1dHtcclxuICAgICAgICAgICAgd2lkdGg6IDFyZW07XHJcbiAgICAgICAgICAgIG1hcmdpbjogMCAuNXJlbSAwIDA7XHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgYXtcclxuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5idG4tc2VuZHtcclxuICAgICAgICB3aWR0aDogMzByZW07XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgY29sb3I6ICNBNDA1MDU7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgaGVpZ2h0OiAzcmVtO1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAuMnJlbTtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgICAgICY6aG92ZXJ7XHJcbiAgICAgICAgICAgIC13ZWJraXQtYW5pbWF0aW9uOiBjb2xvckJnIC4ycyBlYXNlLWluO1xyXG4gICAgICAgICAgICAgICAgICAgIGFuaW1hdGlvbjogY29sb3JCZyAuMnMgZWFzZS1pbjtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDojQTQwNTA1O1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIEAtd2Via2l0LWtleWZyYW1lcyBjb2xvckJnIHtcclxuICAgICAgICAgICAgICAgIGZyb20ge2JhY2tncm91bmQ6ICNmZmY7fVxyXG4gICAgICAgICAgICAgICAgdG8ge2JhY2tncm91bmQ6ICNBNDA1MDU7fVxyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBAa2V5ZnJhbWVzIGNvbG9yQmcge1xyXG4gICAgICAgICAgICAgICAgZnJvbSB7YmFja2dyb3VuZDogI2ZmZjt9XHJcbiAgICAgICAgICAgICAgICB0byB7YmFja2dyb3VuZDogI0E0MDUwNTt9XHJcblxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IFxyXG46Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXJ7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn0gXHJcbjo6LW1vei1wbGFjZWhvbGRlcntcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufSBcclxuOi1tcy1pbnB1dC1wbGFjZWhvbGRlcntcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufSBcclxuOjotbXMtaW5wdXQtcGxhY2Vob2xkZXJ7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn0gXHJcbjo6cGxhY2Vob2xkZXJ7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcblxyXG4vLyBNT0RBTCBFRElUXHJcbmlucHV0W3R5cGU9ZmlsZV06Oi13ZWJraXQtZmlsZS11cGxvYWQtYnV0dG9ue1xyXG4gICAgZGlzcGxheTogbm9uZTsgXHJcbn1cclxuXHJcbi51cGxvYWR7XHJcbiAgICBwYWRkaW5nOiAzMnB4IDAgMCAxMnB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLmFkanVudGFye1xyXG4gICAgbWFyZ2luOiAwIDAgMnJlbSAwO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpO1xyXG4gICAgaGVpZ2h0OiAzcmVtO1xyXG4gICAgd2lkdGg6IDMwcmVtO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiAuNXJlbTtcclxuICAgIHBhZGRpbmc6IC41cmVtIDFyZW0gMDtcclxuICAgIGxhYmVse1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIH1cclxuICAgIHNwYW57XHJcbiAgICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAzMnB4O1xyXG4gICAgICAgIHdpZHRoOiAyMzBweDtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgIH1cclxuXHJcbiAgICBsYWJlbCBpe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgfVxyXG5cclxuICAgICY6aG92ZXJ7XHJcbiAgICAgICAgbWFyZ2luOiAwIDAgMnJlbSAwO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41KTtcclxuICAgICAgICBoZWlnaHQ6IDNyZW07XHJcbiAgICAgICAgd2lkdGg6IDMwcmVtO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgc3BhbntcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIGxhYmVsIGl7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDQyNXB4KXtcclxuICAgIC5iZ3tcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgd2lkdGg6IDE4MTRweDtcclxuICAgICAgICAgICAgbGVmdDogLTEzODlweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICAtd2Via2l0LWFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNvbnRlbmlkb3tcclxuICAgICAgICBkaXNwbGF5OiAtbXMtZ3JpZDtcclxuICAgICAgICBkaXNwbGF5OiBncmlkO1xyXG4gICAgICAgIC1tcy1ncmlkLWNvbHVtbnM6IG5vbmU7XHJcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiBub25lO1xyXG4gICAgICAgIC1tcy1ncmlkLXJvd3M6IDMwJSA3MCU7XHJcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiAzMCUgNzAlO1xyXG4gICAgICAgIC50ZXh0e1xyXG4gICAgICAgICAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XHJcbiAgICAgICAgICAgIGdyaWQtY29sdW1uOiAxO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA3cmVtIDJyZW0gMDtcclxuICAgICAgICAgICAgLW1zLWdyaWQtcm93OiAxO1xyXG4gICAgICAgICAgICBncmlkLXJvdzogMTtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBoMXtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAzNjRweDtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMi41cmVtO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzY0cHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmZvbGxvd3tcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICAgICAgICAgICY6aG92ZXJ7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAuOXJlbTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jb250ZW5pZG8gPiAqOm50aC1jaGlsZCgxKXtcclxuICAgICAgICAtbXMtZ3JpZC1yb3c6IDE7XHJcbiAgICAgICAgLW1zLWdyaWQtY29sdW1uOiAxXHJcbiAgICB9XHJcbiAgICAuY29udGVuaWRvID4gKjpudGgtY2hpbGQoMil7XHJcbiAgICAgICAgLW1zLWdyaWQtcm93OiAyO1xyXG4gICAgICAgIC1tcy1ncmlkLWNvbHVtbjogMVxyXG4gICAgfVxyXG4gICAgLmNvbnRhY3Rve1xyXG4gICAgICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcclxuICAgICAgICBncmlkLWNvbHVtbjogMTtcclxuICAgICAgICAtbXMtZ3JpZC1yb3c6IDI7XHJcbiAgICAgICAgZ3JpZC1yb3c6IDI7XHJcbiAgICAgICAgcGFkZGluZzo1cmVtIDJyZW07XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGgze1xyXG4gICAgICAgICAgICB3aWR0aDogMzY0cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlucHV0e1xyXG4gICAgICAgICAgICB3aWR0aDogMjJyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICB3aWR0aDogMzY0cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5idG4tc2VuZHtcclxuICAgICAgICAgICAgd2lkdGg6IDIycmVtO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5hZGp1bnRhcntcclxuICAgICAgICB3aWR0aDogMjJyZW07XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOjU2MHB4KSBhbmQgKG1heC13aWR0aDogNzY4cHgpe1xyXG4gICAgLmJne1xyXG4gICAgICAgIGltZ3tcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlOyAgICAgICAgICBcclxuICAgICAgICAgICAgd2lkdGg6IDE4MjVweDtcclxuICAgICAgICAgICAgbGVmdDogLTEwNTdweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICAtd2Via2l0LWFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNvbnRlbmlkb3tcclxuICAgICAgICBkaXNwbGF5OiAtbXMtZ3JpZDtcclxuICAgICAgICBkaXNwbGF5OiBncmlkO1xyXG4gICAgICAgIC1tcy1ncmlkLWNvbHVtbnM6IG5vbmU7XHJcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiBub25lO1xyXG4gICAgICAgIC1tcy1ncmlkLXJvd3M6IDMwJSA3MCU7XHJcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiAzMCUgNzAlO1xyXG4gICAgICAgIC50ZXh0e1xyXG4gICAgICAgICAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XHJcbiAgICAgICAgICAgIGdyaWQtY29sdW1uOiAxO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA3cmVtIDJyZW0gMDtcclxuICAgICAgICAgICAgLW1zLWdyaWQtcm93OiAxO1xyXG4gICAgICAgICAgICBncmlkLXJvdzogMTtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBoMXtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA3MDRweDtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMi41cmVtO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogNzA0cHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmZvbGxvd3tcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICAgICAgICAgICY6aG92ZXJ7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAuOXJlbTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jb250ZW5pZG8gPiAqOm50aC1jaGlsZCgxKXtcclxuICAgICAgICAtbXMtZ3JpZC1yb3c6IDE7XHJcbiAgICAgICAgLW1zLWdyaWQtY29sdW1uOiAxXHJcbiAgICB9XHJcbiAgICAuY29udGVuaWRvID4gKjpudGgtY2hpbGQoMil7XHJcbiAgICAgICAgLW1zLWdyaWQtcm93OiAyO1xyXG4gICAgICAgIC1tcy1ncmlkLWNvbHVtbjogMVxyXG4gICAgfVxyXG4gICAgLmNvbnRhY3Rve1xyXG4gICAgICAgIC1tcy1ncmlkLWNvbHVtbjogMTtcclxuICAgICAgICBncmlkLWNvbHVtbjogMTtcclxuICAgICAgICAtbXMtZ3JpZC1yb3c6IDI7XHJcbiAgICAgICAgZ3JpZC1yb3c6IDI7XHJcbiAgICAgICAgcGFkZGluZzo1cmVtIDRyZW07XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGlucHV0e1xyXG4gICAgICAgICAgICB3aWR0aDogNDByZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5idG4tc2VuZHtcclxuICAgICAgICAgICAgd2lkdGg6IDQwcmVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5hZGp1bnRhcntcclxuICAgICAgICB3aWR0aDogNDByZW07XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOjgwMHB4KWFuZChtYXgtd2lkdGg6MTAyNHB4KXtcclxuXHJcblxyXG4gICAgLmJne1xyXG4gICAgICAgIGltZ3tcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB3aWR0aDogMTM1M3B4O1xyXG4gICAgICAgICAgICBsZWZ0OiAtMzI5cHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAgICAgLXdlYmtpdC1hbmltYXRpb246IHNpemUtYmcgMS41cyBlYXNlLWluO1xyXG4gICAgICAgICAgICBhbmltYXRpb246IHNpemUtYmcgMS41cyBlYXNlLWluO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jb250ZW5pZG97XHJcbiAgICAgICAgLnRleHR7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDE1cmVtIDRyZW07XHJcbiAgICAgICAgICAgIGgxe1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDQwOHB4O1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAzcmVtO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogNDA4cHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLmZvbGxvd3tcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICAgICAgICAgICY6aG92ZXJ7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAuOXJlbTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jb250YWN0b3tcclxuICAgICAgICBwYWRkaW5nOiA4cmVtIDAgMCAwO1xyXG4gICAgfVxyXG59XHJcbiIsIioge1xuICBmb250LWZhbWlseTogXCJvcGVuLXNhbnNcIiwgc2Fucy1zZXJpZjtcbn1cblxuaSB7XG4gIGZvbnQtZmFtaWx5OiBcIkZvbnQgQXdlc29tZSA1IEZyZWVcIjtcbn1cblxuLmlzLXZhbGlkIHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpO1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICB3aWR0aDogMzByZW07XG4gIHBhZGRpbmctbGVmdDogMnJlbTtcbiAgY29sb3I6ICNmZmY7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGdyZWVuICFpbXBvcnRhbnQ7XG59XG4uaXMtdmFsaWQ6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNSk7XG59XG5cbi5pcy1pbnZhbGlkIHtcbiAgYmFja2dyb3VuZDogcmdiYSgxOTIsIDI2LCAzNiwgMC4yKSAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICB3aWR0aDogMzByZW07XG4gIHBhZGRpbmctbGVmdDogMnJlbTtcbiAgY29sb3I6ICNmZmY7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNDMDFBMjQgIWltcG9ydGFudDtcbn1cbi5pcy1pbnZhbGlkOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjUpO1xufVxuXG4uYmcgaW1nIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xuICAtd2Via2l0LWFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XG4gIGFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XG59XG5cbkAtd2Via2l0LWtleWZyYW1lcyBzaXplLWJnIHtcbiAgZnJvbSB7XG4gICAgd2lkdGg6IDExMCU7XG4gIH1cbiAgdG8ge1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcbiAgfVxufVxuQGtleWZyYW1lcyBzaXplLWJnIHtcbiAgZnJvbSB7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMTAlO1xuICB9XG4gIHRvIHtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7XG4gIH1cbn1cbi5jb250ZW5pZG8ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IC1tcy1ncmlkO1xuICBkaXNwbGF5OiBncmlkO1xuICAtbXMtZ3JpZC1jb2x1bW5zOiA1MCUgNTAlO1xuICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDUwJSA1MCU7XG59XG4uY29udGVuaWRvIC50ZXh0IHtcbiAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICBncmlkLWNvbHVtbjogMTtcbiAgcGFkZGluZzogMTVyZW0gMTByZW07XG59XG4uY29udGVuaWRvIC50ZXh0IGgxIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogNHJlbTtcbiAgd2lkdGg6IDUwMHB4O1xuICBtYXJnaW4tYm90dG9tOiAycmVtO1xufVxuLmNvbnRlbmlkbyAudGV4dCBwIHtcbiAgY29sb3I6IHdoaXRlO1xuICB3aWR0aDogNDQ1cHg7XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xufVxuLmNvbnRlbmlkbyAudGV4dCAuZm9sbG93IHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG59XG4uY29udGVuaWRvIC50ZXh0IC5mb2xsb3cgaSB7XG4gIGZvbnQtZmFtaWx5OiBcIkZvbnQgQXdlc29tZSA1IEJyYW5kc1wiO1xufVxuLmNvbnRlbmlkbyAudGV4dCAuZm9sbG93IGEge1xuICBjb2xvcjogd2hpdGU7XG59XG4uY29udGVuaWRvIC50ZXh0IC5mb2xsb3cgYTpob3ZlciB7XG4gIGZvbnQtc2l6ZTogMS4zcmVtO1xufVxuXG4uY29udGFjdG8ge1xuICAtbXMtZ3JpZC1jb2x1bW46IDI7XG4gIGdyaWQtY29sdW1uOiAyO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nOiAxNXJlbSAwIDAgNXJlbTtcbn1cbi5jb250YWN0byBoMyB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogI2ZmZjtcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbn1cbi5jb250YWN0byBpbnB1dCwgLmNvbnRhY3RvIHRleHRhcmVhIHtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpO1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICB3aWR0aDogMzByZW07XG4gIHBhZGRpbmctbGVmdDogMnJlbTtcbiAgY29sb3I6ICNmZmY7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XG59XG4uY29udGFjdG8gaW5wdXQ6aG92ZXIsIC5jb250YWN0byB0ZXh0YXJlYTpob3ZlciB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41KTtcbn1cbi5jb250YWN0byBsYWJlbCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGJhc2VsaW5lO1xufVxuLmNvbnRhY3RvIGxhYmVsIGlucHV0LCAuY29udGFjdG8gbGFiZWwgdGV4dGFyZWEge1xuICB3aWR0aDogMXJlbTtcbiAgbWFyZ2luOiAwIDAuNXJlbSAwIDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5jb250YWN0byBsYWJlbCBhIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmNvbnRhY3RvIC5idG4tc2VuZCB7XG4gIHdpZHRoOiAzMHJlbTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGNvbG9yOiAjQTQwNTA1O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgaGVpZ2h0OiAzcmVtO1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDAuMnJlbTtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbn1cbi5jb250YWN0byAuYnRuLXNlbmQ6aG92ZXIge1xuICAtd2Via2l0LWFuaW1hdGlvbjogY29sb3JCZyAwLjJzIGVhc2UtaW47XG4gIGFuaW1hdGlvbjogY29sb3JCZyAwLjJzIGVhc2UtaW47XG4gIGJhY2tncm91bmQ6ICNBNDA1MDU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbkAtd2Via2l0LWtleWZyYW1lcyBjb2xvckJnIHtcbiAgZnJvbSB7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgfVxuICB0byB7XG4gICAgYmFja2dyb3VuZDogI0E0MDUwNTtcbiAgfVxufVxuQGtleWZyYW1lcyBjb2xvckJnIHtcbiAgZnJvbSB7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgfVxuICB0byB7XG4gICAgYmFja2dyb3VuZDogI0E0MDUwNTtcbiAgfVxufVxuXG46Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXIge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbjo6LW1vei1wbGFjZWhvbGRlciB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuOjotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbjo6cGxhY2Vob2xkZXIge1xuICBjb2xvcjogd2hpdGU7XG59XG5cbmlucHV0W3R5cGU9ZmlsZV06Oi13ZWJraXQtZmlsZS11cGxvYWQtYnV0dG9uLCAuY29udGFjdG8gdGV4dGFyZWFbdHlwZT1maWxlXTo6LXdlYmtpdC1maWxlLXVwbG9hZC1idXR0b24ge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4udXBsb2FkIHtcbiAgcGFkZGluZzogMzJweCAwIDAgMTJweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4uYWRqdW50YXIge1xuICBtYXJnaW46IDAgMCAycmVtIDA7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTtcbiAgaGVpZ2h0OiAzcmVtO1xuICB3aWR0aDogMzByZW07XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xuICBwYWRkaW5nOiAwLjVyZW0gMXJlbSAwO1xufVxuLmFkanVudGFyIGxhYmVsIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5hZGp1bnRhciBzcGFuIHtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBtYXJnaW4tbGVmdDogMzJweDtcbiAgd2lkdGg6IDIzMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGNvbG9yOiAjZmZmO1xufVxuLmFkanVudGFyIGxhYmVsIGkge1xuICBmb250LXNpemU6IDEuMjVyZW07XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogI2ZmZjtcbn1cbi5hZGp1bnRhcjpob3ZlciB7XG4gIG1hcmdpbjogMCAwIDJyZW0gMDtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjUpO1xuICBoZWlnaHQ6IDNyZW07XG4gIHdpZHRoOiAzMHJlbTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZGlzcGxheTogZmxleDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLmFkanVudGFyOmhvdmVyIHNwYW4ge1xuICBjb2xvcjogI2ZmZjtcbn1cbi5hZGp1bnRhcjpob3ZlciBsYWJlbCBpIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA0MjVweCkge1xuICAuYmcgaW1nIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDE4MTRweDtcbiAgICBsZWZ0OiAtMTM4OXB4O1xuICAgIGhlaWdodDogYXV0bztcbiAgICAtd2Via2l0LWFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XG4gICAgYW5pbWF0aW9uOiBzaXplLWJnIDEuNXMgZWFzZS1pbjtcbiAgfVxuXG4gIC5jb250ZW5pZG8ge1xuICAgIGRpc3BsYXk6IC1tcy1ncmlkO1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgLW1zLWdyaWQtY29sdW1uczogbm9uZTtcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IG5vbmU7XG4gICAgLW1zLWdyaWQtcm93czogMzAlIDcwJTtcbiAgICBncmlkLXRlbXBsYXRlLXJvd3M6IDMwJSA3MCU7XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCB7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICAgIGdyaWQtY29sdW1uOiAxO1xuICAgIHBhZGRpbmc6IDdyZW0gMnJlbSAwO1xuICAgIC1tcy1ncmlkLXJvdzogMTtcbiAgICBncmlkLXJvdzogMTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCBoMSB7XG4gICAgd2lkdGg6IDM2NHB4O1xuICAgIGZvbnQtc2l6ZTogMi41cmVtO1xuICB9XG4gIC5jb250ZW5pZG8gLnRleHQgcCB7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIHdpZHRoOiAzNjRweDtcbiAgfVxuICAuY29udGVuaWRvIC50ZXh0IC5mb2xsb3cge1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgfVxuICAuY29udGVuaWRvIC50ZXh0IC5mb2xsb3c6aG92ZXIge1xuICAgIGZvbnQtc2l6ZTogMC45cmVtO1xuICB9XG5cbiAgLmNvbnRlbmlkbyA+ICo6bnRoLWNoaWxkKDEpIHtcbiAgICAtbXMtZ3JpZC1yb3c6IDE7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICB9XG5cbiAgLmNvbnRlbmlkbyA+ICo6bnRoLWNoaWxkKDIpIHtcbiAgICAtbXMtZ3JpZC1yb3c6IDI7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICB9XG5cbiAgLmNvbnRhY3RvIHtcbiAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XG4gICAgZ3JpZC1jb2x1bW46IDE7XG4gICAgLW1zLWdyaWQtcm93OiAyO1xuICAgIGdyaWQtcm93OiAyO1xuICAgIHBhZGRpbmc6IDVyZW0gMnJlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmNvbnRhY3RvIGgzIHtcbiAgICB3aWR0aDogMzY0cHg7XG4gIH1cbiAgLmNvbnRhY3RvIGlucHV0LCAuY29udGFjdG8gdGV4dGFyZWEge1xuICAgIHdpZHRoOiAyMnJlbTtcbiAgfVxuICAuY29udGFjdG8gbGFiZWwge1xuICAgIHdpZHRoOiAzNjRweDtcbiAgfVxuICAuY29udGFjdG8gLmJ0bi1zZW5kIHtcbiAgICB3aWR0aDogMjJyZW07XG4gICAgbWFyZ2luLXRvcDogMnJlbTtcbiAgfVxuXG4gIC5hZGp1bnRhciB7XG4gICAgd2lkdGg6IDIycmVtO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNTYwcHgpIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAuYmcgaW1nIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDE4MjVweDtcbiAgICBsZWZ0OiAtMTA1N3B4O1xuICAgIGhlaWdodDogYXV0bztcbiAgICAtd2Via2l0LWFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XG4gICAgYW5pbWF0aW9uOiBzaXplLWJnIDEuNXMgZWFzZS1pbjtcbiAgfVxuXG4gIC5jb250ZW5pZG8ge1xuICAgIGRpc3BsYXk6IC1tcy1ncmlkO1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgLW1zLWdyaWQtY29sdW1uczogbm9uZTtcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IG5vbmU7XG4gICAgLW1zLWdyaWQtcm93czogMzAlIDcwJTtcbiAgICBncmlkLXRlbXBsYXRlLXJvd3M6IDMwJSA3MCU7XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCB7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICAgIGdyaWQtY29sdW1uOiAxO1xuICAgIHBhZGRpbmc6IDdyZW0gMnJlbSAwO1xuICAgIC1tcy1ncmlkLXJvdzogMTtcbiAgICBncmlkLXJvdzogMTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCBoMSB7XG4gICAgd2lkdGg6IDcwNHB4O1xuICAgIGZvbnQtc2l6ZTogMi41cmVtO1xuICB9XG4gIC5jb250ZW5pZG8gLnRleHQgcCB7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIHdpZHRoOiA3MDRweDtcbiAgfVxuICAuY29udGVuaWRvIC50ZXh0IC5mb2xsb3cge1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgfVxuICAuY29udGVuaWRvIC50ZXh0IC5mb2xsb3c6aG92ZXIge1xuICAgIGZvbnQtc2l6ZTogMC45cmVtO1xuICB9XG5cbiAgLmNvbnRlbmlkbyA+ICo6bnRoLWNoaWxkKDEpIHtcbiAgICAtbXMtZ3JpZC1yb3c6IDE7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICB9XG5cbiAgLmNvbnRlbmlkbyA+ICo6bnRoLWNoaWxkKDIpIHtcbiAgICAtbXMtZ3JpZC1yb3c6IDI7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICB9XG5cbiAgLmNvbnRhY3RvIHtcbiAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XG4gICAgZ3JpZC1jb2x1bW46IDE7XG4gICAgLW1zLWdyaWQtcm93OiAyO1xuICAgIGdyaWQtcm93OiAyO1xuICAgIHBhZGRpbmc6IDVyZW0gNHJlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLmNvbnRhY3RvIGlucHV0LCAuY29udGFjdG8gdGV4dGFyZWEge1xuICAgIHdpZHRoOiA0MHJlbTtcbiAgfVxuICAuY29udGFjdG8gLmJ0bi1zZW5kIHtcbiAgICB3aWR0aDogNDByZW07XG4gIH1cblxuICAuYWRqdW50YXIge1xuICAgIHdpZHRoOiA0MHJlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDgwMHB4KSBhbmQgKG1heC13aWR0aDogMTAyNHB4KSB7XG4gIC5iZyBpbWcge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB3aWR0aDogMTM1M3B4O1xuICAgIGxlZnQ6IC0zMjlweDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgLXdlYmtpdC1hbmltYXRpb246IHNpemUtYmcgMS41cyBlYXNlLWluO1xuICAgIGFuaW1hdGlvbjogc2l6ZS1iZyAxLjVzIGVhc2UtaW47XG4gIH1cblxuICAuY29udGVuaWRvIC50ZXh0IHtcbiAgICBwYWRkaW5nOiAxNXJlbSA0cmVtO1xuICB9XG4gIC5jb250ZW5pZG8gLnRleHQgaDEge1xuICAgIHdpZHRoOiA0MDhweDtcbiAgICBmb250LXNpemU6IDNyZW07XG4gIH1cbiAgLmNvbnRlbmlkbyAudGV4dCBwIHtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgd2lkdGg6IDQwOHB4O1xuICB9XG4gIC5jb250ZW5pZG8gLnRleHQgLmZvbGxvdyB7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICB9XG4gIC5jb250ZW5pZG8gLnRleHQgLmZvbGxvdzpob3ZlciB7XG4gICAgZm9udC1zaXplOiAwLjlyZW07XG4gIH1cblxuICAuY29udGFjdG8ge1xuICAgIHBhZGRpbmc6IDhyZW0gMCAwIDA7XG4gIH1cbn0iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UneteComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-unete',
          templateUrl: './unete.component.html',
          styleUrls: ['./unete.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }, {
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Meta"]
        }, {
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Title"]
        }, {
          type: src_app_servicios_correo_correo_service__WEBPACK_IMPORTED_MODULE_3__["CorreoService"]
        }, {
          type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/unete/unete.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/pages/unete/unete.module.ts ***!
    \*********************************************/

  /*! exports provided: UneteModule */

  /***/
  function srcAppPagesUneteUneteModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UneteModule", function () {
      return UneteModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _unete_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./unete-routing.module */
    "./src/app/pages/unete/unete-routing.module.ts");
    /* harmony import */


    var _unete_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./unete.component */
    "./src/app/pages/unete/unete.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

    var UneteModule = function UneteModule() {
      _classCallCheck(this, UneteModule);
    };

    UneteModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: UneteModule
    });
    UneteModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function UneteModule_Factory(t) {
        return new (t || UneteModule)();
      },
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _unete_routing_module__WEBPACK_IMPORTED_MODULE_2__["UneteRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](UneteModule, {
        declarations: [_unete_component__WEBPACK_IMPORTED_MODULE_3__["UneteComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _unete_routing_module__WEBPACK_IMPORTED_MODULE_2__["UneteRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UneteModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [_unete_component__WEBPACK_IMPORTED_MODULE_3__["UneteComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _unete_routing_module__WEBPACK_IMPORTED_MODULE_2__["UneteRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/servicios/correo/correo.service.ts":
  /*!****************************************************!*\
    !*** ./src/app/servicios/correo/correo.service.ts ***!
    \****************************************************/

  /*! exports provided: CorreoService */

  /***/
  function srcAppServiciosCorreoCorreoServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CorreoService", function () {
      return CorreoService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../environments/environment */
    "./src/environments/environment.ts");

    var endpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].endpoint;
    var httpOptions = {
      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/x-www-form-urlencoded,multipart/form-data,application/json'
      })
    };

    var CorreoService =
    /*#__PURE__*/
    function () {
      function CorreoService(http) {
        _classCallCheck(this, CorreoService);

        this.http = http;
      } // metodo para enviar correo


      _createClass(CorreoService, [{
        key: "senMail",
        value: function senMail($data, Lang) {
          var params = {
            nombre: $data.nombre,
            telefono: $data.telefono,
            email: $data.email,
            asunto: $data.asunto,
            mensaje: "mensaje",
            cbox2: "true",
            enviar: $data.enviar,
            lang: Lang
          }; // console.log('---- ',params);

          return this.http.post(endpoint + 'formcos/datos_web', params, httpOptions);
        }
      }]);

      return CorreoService;
    }();

    CorreoService.ɵfac = function CorreoService_Factory(t) {
      return new (t || CorreoService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
    };

    CorreoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: CorreoService,
      factory: CorreoService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CorreoService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=pages-unete-unete-module-es5.js.map