function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-blog-entradas-servicio-rentable-servicio-rentable-module"], {
  /***/
  "./src/app/pages/blog/entradas/servicio-rentable/servicio-rentable-routing.module.ts":
  /*!*******************************************************************************************!*\
    !*** ./src/app/pages/blog/entradas/servicio-rentable/servicio-rentable-routing.module.ts ***!
    \*******************************************************************************************/

  /*! exports provided: ServicioRentableRoutingModule */

  /***/
  function srcAppPagesBlogEntradasServicioRentableServicioRentableRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ServicioRentableRoutingModule", function () {
      return ServicioRentableRoutingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _servicio_rentable_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./servicio-rentable.component */
    "./src/app/pages/blog/entradas/servicio-rentable/servicio-rentable.component.ts");

    var routes = [{
      path: '',
      component: _servicio_rentable_component__WEBPACK_IMPORTED_MODULE_2__["ServicioRentableComponent"]
    }];

    var ServicioRentableRoutingModule = function ServicioRentableRoutingModule() {
      _classCallCheck(this, ServicioRentableRoutingModule);
    };

    ServicioRentableRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: ServicioRentableRoutingModule
    });
    ServicioRentableRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function ServicioRentableRoutingModule_Factory(t) {
        return new (t || ServicioRentableRoutingModule)();
      },
      imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](ServicioRentableRoutingModule, {
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ServicioRentableRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/blog/entradas/servicio-rentable/servicio-rentable.component.ts":
  /*!**************************************************************************************!*\
    !*** ./src/app/pages/blog/entradas/servicio-rentable/servicio-rentable.component.ts ***!
    \**************************************************************************************/

  /*! exports provided: ServicioRentableComponent */

  /***/
  function srcAppPagesBlogEntradasServicioRentableServicioRentableComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ServicioRentableComponent", function () {
      return ServicioRentableComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
    /* harmony import */


    var src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/servicios/blog.service */
    "./src/app/servicios/blog.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

    var _c0 = function _c0() {
      return ["/blog"];
    };

    var ServicioRentableComponent =
    /*#__PURE__*/
    function () {
      function ServicioRentableComponent(meta, title, _blogService) {
        _classCallCheck(this, ServicioRentableComponent);

        this.meta = meta;
        this.title = title;
        this._blogService = _blogService;
        this.entrada = [];
      }

      _createClass(ServicioRentableComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          // usar el array del servicio blog
          this.entrada = this._blogService.entrada; // Para ñadir el título de la página

          this.title.setTitle('Colombian Outsourcing Solution - COS'); // Añadir el tag de la info de la página

          this.meta.addTag({
            name: 'page.info',
            content: '¿Son rentables los servicios de Outsourcing en Colombia?'
          });
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          // Quitar el tag "page.info" antes de ir a otra página, para que rastree correctamente la información
          this.meta.removeTag('name=\'page.info\'');
        }
      }]);

      return ServicioRentableComponent;
    }();

    ServicioRentableComponent.ɵfac = function ServicioRentableComponent_Factory(t) {
      return new (t || ServicioRentableComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__["BlogService"]));
    };

    ServicioRentableComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ServicioRentableComponent,
      selectors: [["app-servicio-rentable"]],
      decls: 67,
      vars: 7,
      consts: [["id", "landing", 1, "bg"], ["alt", "contact-center-BPO-COS-blog", 3, "src"], [1, "contenido"], [1, "text"], [1, "mt-5", "follow"], ["href", "https://www.facebook.com/Outsourcingcos/", "target", "blank"], [1, "fab", "fa-facebook-square", "ml-5"], ["href", "https://www.instagram.com/cosoutsourcing/", "target", "blank"], [1, "fab", "fa-instagram-square", "ml-3"], ["href", "https://www.linkedin.com/company/colombianoutsourcingcos/about/", "target", "blank"], [1, "fab", "fa-linkedin", "ml-3"], ["href", "https://www.youtube.com/channel/UCL89AlspoSj_exe6qidqHew", "target", "blank"], [1, "fab", "fa-youtube", "ml-3"], [1, "container"], [1, "fab", "fa-facebook-square", "ml-3"], [1, "tag-blog"], [3, "routerLink"], [1, "fas", "fa-tag"], [1, "comment"], [1, "form-group"], [1, "form-group", "nombre"], ["type", "text", "placeholder", "Ejem: Carlos, Maria, etc ...", 1, "form-control"], [1, "form-group", "correo"], ["type", "text", "placeholder", "Ejem: email@email.com ...", 1, "form-control"], [1, "form-group", "web"], ["type", "text", "placeholder", "Ejem: www.mysitioweb.com ...", 1, "form-control"], [1, "form-group", "comentario"], ["name", "", "id", "", "cols", "30", "rows", "10", "placeholder", "D\xE9janos tu comentario", 1, "form-control"], [1, "btn", "btn-primary", "btn-block", "mt-10"]],
      template: function ServicioRentableComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "s\xEDguenos en ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "i", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "i", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "i", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "i", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "s\xEDguenos en ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "i", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "a", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "i", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "i", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "a", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "i", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " Noticias ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "hr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, " Deja tu comentario ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, " Tu Correo no ser\xE1 publicado. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Los campos marcados con (*) con obligatorios");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Nombre *");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "input", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "correo *");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "input", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Web");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "input", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Comentario *");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "textarea", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "button", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, " Enviar Comentario ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.entrada[0].img, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[8].title);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.entrada[8].title, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[8].fecha);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.entrada[8].text);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](6, _c0));
        }
      },
      directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"]],
      styles: ["*[_ngcontent-%COMP%] {\n  font-family: \"open-sans\", sans-serif;\n}\n\ni[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Free\";\n}\n\n[_ngcontent-%COMP%]::-moz-placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n[_ngcontent-%COMP%]::-ms-input-placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n[_ngcontent-%COMP%]::placeholder {\n  color: #c4c4c4;\n  padding: 0.5rem;\n}\n\n.bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n  position: absolute;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n  position: relative;\n  padding: 16rem 12rem 0;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n  font-weight: 800;\n  color: #fff;\n  font-size: 4rem;\n  width: 800px;\n  margin-bottom: 12rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: white;\n  width: 445px;\n  margin-bottom: 2rem;\n  font-size: 1.2rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: white;\n}\n\n.bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  font-size: 1.3rem;\n}\n\n.container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  margin-top: 5rem;\n  font-weight: bold;\n  color: #353535;\n}\n\n.container[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin-bottom: 2rem;\n  font-weight: bold;\n}\n\n.container[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-family: \"Font Awesome 5 Brands\";\n  font-size: 1.5rem;\n  color: #C01A24;\n}\n\n.container[_ngcontent-%COMP%]   .follow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]:hover {\n  color: #610d12;\n}\n\n.container[_ngcontent-%COMP%]   .tag-blog[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 0.8rem;\n}\n\n.container[_ngcontent-%COMP%]   .tag-blog[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #353535;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%] {\n  margin-bottom: 0.5rem;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .nombre[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin: 2rem 0 0 0;\n}\n\n.container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .correo[_ngcontent-%COMP%]   label[_ngcontent-%COMP%], .container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .comentario[_ngcontent-%COMP%]   label[_ngcontent-%COMP%], .container[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]   .web[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  margin: 0.5rem 0 0 0;\n}\n\n.container[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  margin-top: 3rem;\n  height: 3rem;\n}\n\n@media (min-width: 800px) and (max-width: 1024px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -200px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    margin-bottom: 7rem;\n  }\n}\n\n@media (min-width: 560px) and (max-width: 768px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 1224px;\n    left: -456px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n    padding: 10rem 11rem 0;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 3rem;\n    width: 460px;\n  }\n\n  .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n    margin-top: 6rem;\n  }\n}\n\n@media (min-width: 356px) and (max-width: 425px) {\n  .bg[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 944px;\n    height: auto;\n    left: -519px;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%] {\n    padding: 10rem 1rem 0;\n    text-align: center;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\n    font-size: 2rem;\n    width: 393px;\n    margin-bottom: 5rem;\n  }\n  .bg[_ngcontent-%COMP%]   .contenido[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    width: 393px;\n  }\n\n  .container[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n    margin-top: 10rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYmxvZy9lbnRyYWRhcy9zZXJ2aWNpby1yZW50YWJsZS9EOlxccHJveWVjdG9zXFx3ZWJzaXRlLWNvcy9zcmNcXGFwcFxccGFnZXNcXGJsb2dcXGVudHJhZGFzXFxzZXJ2aWNpby1yZW50YWJsZVxcc2VydmljaW8tcmVudGFibGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Jsb2cvZW50cmFkYXMvc2VydmljaW8tcmVudGFibGUvc2VydmljaW8tcmVudGFibGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQ0FBQTtBQ0NKOztBREVBO0VBQ0ksa0NBQUE7QUNDSjs7QURDQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FDRUo7O0FESkE7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQ0VKOztBREpBO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUNFSjs7QURFSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNDUjs7QURDSTtFQUNJLGtCQUFBO0VBQ0Esc0JBQUE7QUNDUjs7QURBUTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7QUNFWjs7QURBUTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQ0VaOztBREFRO0VBQ0ksaUJBQUE7QUNFWjs7QUREWTtFQUNJLG9DQUFBO0FDR2hCOztBRERZO0VBQ0ksWUFBQTtBQ0doQjs7QURGZ0I7RUFDSSxpQkFBQTtBQ0lwQjs7QURHSTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FDQVI7O0FERUk7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0FDQVI7O0FER1E7RUFDSSxvQ0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQ0RaOztBREVZO0VBQ0ksY0FBQTtBQ0FoQjs7QURLUTtFQUNJLGlCQUFBO0FDSFo7O0FES1E7RUFDSSxjQUFBO0FDSFo7O0FETUk7RUFDSSxxQkFBQTtBQ0pSOztBRE1ZO0VBQ0ksa0JBQUE7QUNKaEI7O0FEUVk7RUFDSSxvQkFBQTtBQ05oQjs7QURnQkk7RUFDSSxnQkFBQTtFQUNBLFlBQUE7QUNkUjs7QURrQkE7RUFFUTtJQUNJLGFBQUE7SUFDQSxZQUFBO0VDaEJWO0VEb0JVO0lBQ0ksbUJBQUE7RUNsQmQ7QUFDRjs7QUR3QkE7RUFFUTtJQUNJLGFBQUE7SUFDQSxZQUFBO0VDdkJWO0VEeUJNO0lBQ0ksc0JBQUE7RUN2QlY7RUR3QlU7SUFDSSxlQUFBO0lBQ0EsWUFBQTtFQ3RCZDs7RUQyQk07SUFDSSxnQkFBQTtFQ3hCVjtBQUNGOztBRDRCQTtFQUVRO0lBQ0ksWUFBQTtJQUNBLFlBQUE7SUFDQSxZQUFBO0VDM0JWO0VENkJNO0lBQ0kscUJBQUE7SUFDQSxrQkFBQTtFQzNCVjtFRDRCVTtJQUNJLGVBQUE7SUFDQSxZQUFBO0lBQ0EsbUJBQUE7RUMxQmQ7RUQ0QlU7SUFDSSxlQUFBO0lBQ0EsWUFBQTtFQzFCZDs7RURnQ007SUFDSSxpQkFBQTtFQzdCVjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYmxvZy9lbnRyYWRhcy9zZXJ2aWNpby1yZW50YWJsZS9zZXJ2aWNpby1yZW50YWJsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIip7XHJcbiAgICBmb250LWZhbWlseTogJ29wZW4tc2FucycsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbml7XHJcbiAgICBmb250LWZhbWlseTogJ0ZvbnQgQXdlc29tZSA1IEZyZWUnO1xyXG59XHJcbjo6cGxhY2Vob2xkZXJ7XHJcbiAgICBjb2xvcjogI2M0YzRjNDtcclxuICAgIHBhZGRpbmc6IC41cmVtO1xyXG59XHJcbi8vIEJhY2tncm91bmcgY2xhc3NcclxuLmJne1xyXG4gICAgaW1ne1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7ICAgXHJcbiAgICB9XHJcbiAgICAuY29udGVuaWRve1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBwYWRkaW5nOiAxNnJlbSAxMnJlbSAwO1xyXG4gICAgICAgIGgxe1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogODAwO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgZm9udC1zaXplOiA0cmVtO1xyXG4gICAgICAgICAgICB3aWR0aDogODAwcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEycmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwe1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIHdpZHRoOiA0NDVweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjJyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5mb2xsb3d7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6ICdGb250IEF3ZXNvbWUgNSBCcmFuZHMnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGF7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMS4zcmVtO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbi5jb250YWluZXJ7XHJcbiAgICBoMntcclxuICAgICAgICBtYXJnaW4tdG9wOiA1cmVtO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjMzUzNTM1O1xyXG4gICAgfVxyXG4gICAgbGFiZWx7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuICAgIC5mb2xsb3d7XHJcbiAgICAgICAgaXtcclxuICAgICAgICAgICAgZm9udC1mYW1pbHk6ICdGb250IEF3ZXNvbWUgNSBCcmFuZHMnO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgICAgICAgICAgY29sb3I6ICNDMDFBMjQ7XHJcbiAgICAgICAgICAgICY6aG92ZXJ7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjpyZ2IoOTcsIDEzLCAxOCkgO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLnRhZy1ibG9ne1xyXG4gICAgICAgIGl7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogLjhyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGF7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjMzUzNTM1O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5mb3JtLWdyb3Vwe1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IC41cmVtO1xyXG4gICAgICAgIC5ub21icmV7XHJcbiAgICAgICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOjJyZW0gMCAwIDA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmNvcnJlb3tcclxuICAgICAgICAgICAgbGFiZWx7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46LjVyZW0gMCAwIDA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLndlYntcclxuICAgICAgICAgICAgQGV4dGVuZCAuY29ycmVvO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY29tZW50YXJpb3tcclxuICAgICAgICAgICAgQGV4dGVuZCAuY29ycmVvO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGJ1dHRvbntcclxuICAgICAgICBtYXJnaW4tdG9wOiAzcmVtO1xyXG4gICAgICAgIGhlaWdodDogM3JlbTtcclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6ODAwcHgpYW5kKG1heC13aWR0aDoxMDI0cHgpe1xyXG4gICAgLmJne1xyXG4gICAgICAgIGltZ3tcclxuICAgICAgICAgICAgd2lkdGg6IDEyMjRweDtcclxuICAgICAgICAgICAgbGVmdDogLTIwMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY29udGVuaWRve1xyXG5cclxuICAgICAgICAgICAgaDF7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA3cmVtO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6NTYwcHgpYW5kKG1heC13aWR0aDo3NjhweCl7XHJcbiAgICAuYmd7XHJcbiAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICB3aWR0aDogMTIyNHB4O1xyXG4gICAgICAgICAgICBsZWZ0OiAtNDU2cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb250ZW5pZG97XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcmVtIDExcmVtIDA7XHJcbiAgICAgICAgICAgIGgxe1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAzcmVtO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDQ2MHB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNvbnRhaW5lcntcclxuICAgICAgICBoMntcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogNnJlbTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiAzNTZweClhbmQobWF4LXdpZHRoOjQyNXB4KXtcclxuICAgIC5iZ3tcclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIHdpZHRoOiA5NDRweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgICAgICBsZWZ0OiAtNTE5cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb250ZW5pZG97XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcmVtIDFyZW0gMDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBoMXtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMnJlbTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOjM5M3B4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTo1cmVtIDsgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcHtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAzOTNweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jb250YWluZXJ7XHJcbiAgICAgICAgXHJcbiAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcmVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSIsIioge1xuICBmb250LWZhbWlseTogXCJvcGVuLXNhbnNcIiwgc2Fucy1zZXJpZjtcbn1cblxuaSB7XG4gIGZvbnQtZmFtaWx5OiBcIkZvbnQgQXdlc29tZSA1IEZyZWVcIjtcbn1cblxuOjpwbGFjZWhvbGRlciB7XG4gIGNvbG9yOiAjYzRjNGM0O1xuICBwYWRkaW5nOiAwLjVyZW07XG59XG5cbi5iZyBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4uYmcgLmNvbnRlbmlkbyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgcGFkZGluZzogMTZyZW0gMTJyZW0gMDtcbn1cbi5iZyAuY29udGVuaWRvIGgxIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogNHJlbTtcbiAgd2lkdGg6IDgwMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxMnJlbTtcbn1cbi5iZyAuY29udGVuaWRvIHAge1xuICBjb2xvcjogd2hpdGU7XG4gIHdpZHRoOiA0NDVweDtcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbiAgZm9udC1zaXplOiAxLjJyZW07XG59XG4uYmcgLmNvbnRlbmlkbyAuZm9sbG93IHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG59XG4uYmcgLmNvbnRlbmlkbyAuZm9sbG93IGkge1xuICBmb250LWZhbWlseTogXCJGb250IEF3ZXNvbWUgNSBCcmFuZHNcIjtcbn1cbi5iZyAuY29udGVuaWRvIC5mb2xsb3cgYSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5iZyAuY29udGVuaWRvIC5mb2xsb3cgYTpob3ZlciB7XG4gIGZvbnQtc2l6ZTogMS4zcmVtO1xufVxuXG4uY29udGFpbmVyIGgyIHtcbiAgbWFyZ2luLXRvcDogNXJlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMzUzNTM1O1xufVxuLmNvbnRhaW5lciBsYWJlbCB7XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNvbnRhaW5lciAuZm9sbG93IGkge1xuICBmb250LWZhbWlseTogXCJGb250IEF3ZXNvbWUgNSBCcmFuZHNcIjtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIGNvbG9yOiAjQzAxQTI0O1xufVxuLmNvbnRhaW5lciAuZm9sbG93IGk6aG92ZXIge1xuICBjb2xvcjogIzYxMGQxMjtcbn1cbi5jb250YWluZXIgLnRhZy1ibG9nIGkge1xuICBmb250LXNpemU6IDAuOHJlbTtcbn1cbi5jb250YWluZXIgLnRhZy1ibG9nIGEge1xuICBjb2xvcjogIzM1MzUzNTtcbn1cbi5jb250YWluZXIgLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XG59XG4uY29udGFpbmVyIC5mb3JtLWdyb3VwIC5ub21icmUgbGFiZWwge1xuICBtYXJnaW46IDJyZW0gMCAwIDA7XG59XG4uY29udGFpbmVyIC5mb3JtLWdyb3VwIC5jb3JyZW8gbGFiZWwsIC5jb250YWluZXIgLmZvcm0tZ3JvdXAgLmNvbWVudGFyaW8gbGFiZWwsIC5jb250YWluZXIgLmZvcm0tZ3JvdXAgLndlYiBsYWJlbCB7XG4gIG1hcmdpbjogMC41cmVtIDAgMCAwO1xufVxuLmNvbnRhaW5lciBidXR0b24ge1xuICBtYXJnaW4tdG9wOiAzcmVtO1xuICBoZWlnaHQ6IDNyZW07XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA4MDBweCkgYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xuICAuYmcgaW1nIHtcbiAgICB3aWR0aDogMTIyNHB4O1xuICAgIGxlZnQ6IC0yMDBweDtcbiAgfVxuICAuYmcgLmNvbnRlbmlkbyBoMSB7XG4gICAgbWFyZ2luLWJvdHRvbTogN3JlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDU2MHB4KSBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgLmJnIGltZyB7XG4gICAgd2lkdGg6IDEyMjRweDtcbiAgICBsZWZ0OiAtNDU2cHg7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8ge1xuICAgIHBhZGRpbmc6IDEwcmVtIDExcmVtIDA7XG4gIH1cbiAgLmJnIC5jb250ZW5pZG8gaDEge1xuICAgIGZvbnQtc2l6ZTogM3JlbTtcbiAgICB3aWR0aDogNDYwcHg7XG4gIH1cblxuICAuY29udGFpbmVyIGgyIHtcbiAgICBtYXJnaW4tdG9wOiA2cmVtO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMzU2cHgpIGFuZCAobWF4LXdpZHRoOiA0MjVweCkge1xuICAuYmcgaW1nIHtcbiAgICB3aWR0aDogOTQ0cHg7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIGxlZnQ6IC01MTlweDtcbiAgfVxuICAuYmcgLmNvbnRlbmlkbyB7XG4gICAgcGFkZGluZzogMTByZW0gMXJlbSAwO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICAuYmcgLmNvbnRlbmlkbyBoMSB7XG4gICAgZm9udC1zaXplOiAycmVtO1xuICAgIHdpZHRoOiAzOTNweDtcbiAgICBtYXJnaW4tYm90dG9tOiA1cmVtO1xuICB9XG4gIC5iZyAuY29udGVuaWRvIHAge1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICB3aWR0aDogMzkzcHg7XG4gIH1cblxuICAuY29udGFpbmVyIGgyIHtcbiAgICBtYXJnaW4tdG9wOiAxMHJlbTtcbiAgfVxufSJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ServicioRentableComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-servicio-rentable',
          templateUrl: './servicio-rentable.component.html',
          styleUrls: ['./servicio-rentable.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"]
        }, {
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"]
        }, {
          type: src_app_servicios_blog_service__WEBPACK_IMPORTED_MODULE_2__["BlogService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/blog/entradas/servicio-rentable/servicio-rentable.module.ts":
  /*!***********************************************************************************!*\
    !*** ./src/app/pages/blog/entradas/servicio-rentable/servicio-rentable.module.ts ***!
    \***********************************************************************************/

  /*! exports provided: ServicioRentableModule */

  /***/
  function srcAppPagesBlogEntradasServicioRentableServicioRentableModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ServicioRentableModule", function () {
      return ServicioRentableModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _servicio_rentable_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./servicio-rentable-routing.module */
    "./src/app/pages/blog/entradas/servicio-rentable/servicio-rentable-routing.module.ts");
    /* harmony import */


    var _servicio_rentable_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./servicio-rentable.component */
    "./src/app/pages/blog/entradas/servicio-rentable/servicio-rentable.component.ts");

    var ServicioRentableModule = function ServicioRentableModule() {
      _classCallCheck(this, ServicioRentableModule);
    };

    ServicioRentableModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: ServicioRentableModule
    });
    ServicioRentableModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function ServicioRentableModule_Factory(t) {
        return new (t || ServicioRentableModule)();
      },
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _servicio_rentable_routing_module__WEBPACK_IMPORTED_MODULE_2__["ServicioRentableRoutingModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](ServicioRentableModule, {
        declarations: [_servicio_rentable_component__WEBPACK_IMPORTED_MODULE_3__["ServicioRentableComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _servicio_rentable_routing_module__WEBPACK_IMPORTED_MODULE_2__["ServicioRentableRoutingModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ServicioRentableModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [_servicio_rentable_component__WEBPACK_IMPORTED_MODULE_3__["ServicioRentableComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _servicio_rentable_routing_module__WEBPACK_IMPORTED_MODULE_2__["ServicioRentableRoutingModule"]]
        }]
      }], null, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=pages-blog-entradas-servicio-rentable-servicio-rentable-module-es5.js.map